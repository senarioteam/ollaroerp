package com.senarios.Ollaroerp.Utitlities;


import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

import com.activeandroid.query.Delete;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonArray;
import com.senarios.Ollaroerp.Models.CartItemModel;
import com.senarios.Ollaroerp.Models.OfflineOrders;
import com.senarios.Ollaroerp.Models.OfflineOrdersResponse;
import com.senarios.Ollaroerp.Models.Order;
import com.senarios.Ollaroerp.Utitlities.Retrofit.GetDataService;
import com.senarios.Ollaroerp.Utitlities.Retrofit.RetrofitClientInstance;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.Modifier;
import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;

public class NetworkChangeReceiver extends BroadcastReceiver {

    Context context;
    ArrayList<OfflineOrders> allOrders;

    @Override
    public void onReceive(final Context context, final Intent intent) {
        this.context = context;
        ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netInfo = cm.getActiveNetworkInfo();
        //should check null because in airplane mode it will be null
        if (netInfo != null && netInfo.isConnected()) {
            if (PrefUtils.getString(context, "is_logged_in", "false").equalsIgnoreCase("true")) {

                TinyDB tinyDB = new TinyDB(context);
                ArrayList<String> offlineOrders = tinyDB.getListString("OfflineOrders");
                GsonBuilder builder = new GsonBuilder();
                builder.excludeFieldsWithModifiers();
                builder.excludeFieldsWithModifiers(Modifier.FINAL, Modifier.TRANSIENT, Modifier.STATIC);
                Gson gson = builder.create();
                allOrders = new ArrayList<>();
                for (int i = 0; i < offlineOrders.size(); i++) {
                    OfflineOrders offlineOrder = gson.fromJson(offlineOrders.get(i), OfflineOrders.class);
                    allOrders.add(offlineOrder);
                }
                JsonArray ordersToSend = new JsonArray();
                JSONArray ordersFinalArray = new JSONArray();
                for (int i = 0; i < allOrders.size(); i++) {
                    Order order = new Gson().fromJson(allOrders.get(i).getOrder(), Order.class);
                    String items = allOrders.get(i).getItems();
                    JSONObject preparedSingleOrder = new JSONObject();
                    try {
                        preparedSingleOrder.put("condiments", order.getCondiments());
                        preparedSingleOrder.put("waiter", order.getWaiter());
                        preparedSingleOrder.put("item_description", order.getItem_description());
                        preparedSingleOrder.put("no_of_guests", order.getNo_of_guests());
                        preparedSingleOrder.put("table_no", order.getTable_no());
                        preparedSingleOrder.put("date_time", order.getDate_time());
                        preparedSingleOrder.put("section", order.getSection());
                        preparedSingleOrder.put("waiter_id", String.valueOf(PrefUtils.getUserModel(context).getId()));
                        preparedSingleOrder.put("restaurant", order.getRestaurant());
                        String amount = order.getTotal_amount();
                        amount = amount.replace("KSH ", "");
                        preparedSingleOrder.put("total_amount", amount);
                        preparedSingleOrder.put("items", items);
                        ordersFinalArray.put(preparedSingleOrder);

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }


                }
                genOrder(ordersFinalArray.toString());

            }
        }
    }

    public void genOrder(final String orders) {
        GetDataService service = RetrofitClientInstance.getRetrofitInstance().create(GetDataService.class);

        Call<OfflineOrdersResponse> call = service.callOfflineOrders(orders);
        call.enqueue(new Callback<OfflineOrdersResponse>() {

            @Override
            public void onResponse(Call<OfflineOrdersResponse> call, retrofit2.Response<OfflineOrdersResponse> response) {
                if (response.body() != null) {
                    if (response.body().getSuccess().equalsIgnoreCase("Orders has been made Successfully!") && response.body().getCode() == 200) {
                        TinyDB tinyDB = new TinyDB(context);
                        tinyDB.clear();
                        new Delete().from(CartItemModel.class).execute();
                    } else {
                        genOrder(orders);
                    }

                } else {
                    genOrder(orders);
                }

            }

            @Override
            public void onFailure(Call<OfflineOrdersResponse> call, Throwable t) {
                genOrder(orders);
            }
        });


    }
}
