package com.senarios.Ollaroerp.Utitlities;

import android.content.Context;
import android.content.SharedPreferences;

import com.senarios.Ollaroerp.MainActivity;
import com.senarios.Ollaroerp.Models.UserModel;

public class PrefUtils {
    static public final class Prefs {
        public static SharedPreferences get(Context context) {
            if(context==null){
                context = MainActivity.activity;
            }
            return context.getSharedPreferences("mSharePrefs", 0);
        }
    }

    public static SharedPreferences getsharedpreference(Context context){
        return context.getSharedPreferences("mSharePrefs", 0);
    }

    static public String getString(Context context, String key) {
        SharedPreferences settings = Prefs.get(context);
        return settings.getString(key, "");
    }

    static public String getString(Context context, String key, String defaultString) {
        SharedPreferences settings = Prefs.get(context);
        return settings.getString(key, defaultString);
    }

    static public synchronized void putString(Context context, String key, String value) {
        SharedPreferences settings = Prefs.get(context);
        SharedPreferences.Editor editor = settings.edit();
        editor.putString(key, value);
        editor.apply();
    }

    static public void putUserModel(Context context, UserModel mobileUsersModel,String password){
        SharedPreferences settings = Prefs.get(context);
        SharedPreferences.Editor editor = settings.edit();
        editor.putString("name", mobileUsersModel.getName());
        editor.putString("email",mobileUsersModel.getEmail());
        editor.putString("password",password);
        editor.putString("id",Integer.toString(mobileUsersModel.getId()));
        editor.putString("restaurant",mobileUsersModel.getRestaurant());
        editor.commit();
    }
    static public UserModel getUserModel(Context context){
        UserModel mobileUsersModel = new UserModel();
        SharedPreferences settings = Prefs.get(context);
        SharedPreferences.Editor editor = settings.edit();
        mobileUsersModel.setName(settings.getString("name", ""));;
        mobileUsersModel.setEmail(settings.getString("email",""));
        mobileUsersModel.setRestaurant(settings.getString("restaurant",""));
        mobileUsersModel.setId(Integer.valueOf(settings.getString("id","-1")));
        return  mobileUsersModel;
    }

    static public void clearPreferences(Context context){
        SharedPreferences settings = Prefs.get(context);
        settings.edit().clear().apply();
    }

}
