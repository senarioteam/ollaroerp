package com.senarios.Ollaroerp.Utitlities.Retrofit;

import com.google.gson.JsonObject;
import com.senarios.Ollaroerp.Models.BaseAlchoholicGroup;
import com.senarios.Ollaroerp.Models.BaseAllItemsResponse;
import com.senarios.Ollaroerp.Models.BaseFamilyGroup;
import com.senarios.Ollaroerp.Models.BaseInProgressOrders;
import com.senarios.Ollaroerp.Models.BaseItemModel;
import com.senarios.Ollaroerp.Models.BaseMajorGroup;
import com.senarios.Ollaroerp.Models.BasePaidOrder;
import com.senarios.Ollaroerp.Models.BasePostPaidOrder;
import com.senarios.Ollaroerp.Models.BaseSubAlcoholicGroup;
import com.senarios.Ollaroerp.Models.BaseSubMajorGroup;
import com.senarios.Ollaroerp.Models.BaseTableModel;
import com.senarios.Ollaroerp.Models.BaseUnpaidBill;
import com.senarios.Ollaroerp.Models.OfflineOrdersResponse;
import com.senarios.Ollaroerp.Models.OrderResponse;
import com.senarios.Ollaroerp.Models.UserBaseModel;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Headers;
import retrofit2.http.POST;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface GetDataService {

   /* String userid=PrefUtils.getUserModel(GetDataService.this).getId();*/

    @POST("login")
    Call<UserBaseModel> callLogin(@Query("email") String email, @Query("password") String password,@Query("fcm_id") String fcmId,@Query("version")String version);

    @POST("change_password")
    Call<JsonObject> callChangePassword(@Query("email") String email, @Query("password") String password);

    @GET("major_groups")
    Call<BaseMajorGroup> getMajorGroups();

    @POST("sub_major_groups")
    Call<BaseSubMajorGroup> getSubMajorGroups(@Query("major_group_name") String subGroupName);

    @POST("alcoholic_groups")
    Call<BaseAlchoholicGroup> getAlcoholicGroups(@Query("menu_items_group_id") String subGroupId);

    @POST("waiterTables")
    Call<BaseTableModel> getTables(@Query("waiter_id") String waiterId);

    @POST("family_groups")
    Call<BaseFamilyGroup> getFamilyGroups(@Query("menu_items_group_id") String subGroupId);

    @POST("sub_alcoholic_groups")
    Call<BaseSubAlcoholicGroup> getAlcoholicSubGroups(@Query("family_groups_id") String subGroupId);

    @POST("general_items")
    Call<BaseItemModel> getGeneralItems(@Query("family_groups_id") String subGroupId, @Query("is_alchoholic") String isalco);

    @POST("updateStatus")
    Call<JsonObject> updateOrderStatus(@Query("order_id") String subGroupId);

    @POST("billTransfer")
    Call<JsonObject> transferBill(@Query("bill_id") String billId,@Query("waiter") String waiterId);

    @GET("bills/{waiter_id}")
    Call<BaseUnpaidBill>getUnpaidBills(@Path("waiter_id") String waiterId);

    @GET("waiters")
    Call<JsonObject> getWaiters();

    @GET("allGroups/{waiter_id}")
    Call<BaseAllItemsResponse> getEverything(@Path("waiter_id")String waiterid);

    @GET("InProgressOrders/{waiter_id}")
    Call<BaseInProgressOrders> getInprogressOrders(@Path("waiter_id") String waiterid);

    @GET("paidOrders")
    Call<BasePaidOrder> getPaidOrders();

    @GET("postpaidfinal")
    Call<BasePostPaidOrder> getPostPaidOrders();

    @GET("newOrders/{waiter_id}")
    Call<BasePostPaidOrder> getUnPaidOrders(@Path("waiter_id") String waiterid);

    @POST("search")
    Call<BaseItemModel> search(@Query("name") String searchable);

    @POST("generate_bill")
    Call<JsonObject> genBill(@Query("order_id") String order_id);

    @POST("bill_status")
    Call<JsonObject> updateBillStatus(@Query("bill_id") String billId);

    @POST("check_payment")
    Call<JsonObject> checkPayment(@Query("number") String number);

    @POST("pay_bill")
    Call<JsonObject> payBill(@Query("phone") String number, @Query("bill_no") String billNumber, @Query("amount") String amount, @Query("fcm_id") String fcmId, @Query("waiter_id") String waiterId);


    @POST("prepaid_billing")
    Call<JsonObject> prepaidOrder(@Query("phone") String phone, @Query("fcm_id") String fcmId, @Query("waiter_id") String waiterId, @Query("amount") String amount);


    @POST("postpaidOrders")
    Call<OrderResponse> generatePostpaidOrder(@Query("waiter_id") String waiter_id, @Query("date_time") String time, @Query("table_no") String table, @Query("no_of_guests") String guests, @Query("item_description") String description, @Query("condiments") String condiments, @Query("waiter") String waiter, @Query("restaurant") String restaurant, @Query("total_amount") String totalAmount, @Query("items") String items, @Query("section") String section);

    @POST("prepaidOrders")
    Call<OrderResponse> generatePrePaidOrders(@Query("waiter_id") String waiter_id, @Query("date_time") String time, @Query("table_no") String table, @Query("no_of_guests") String guests, @Query("item_description") String description, @Query("condiments") String condiments, @Query("waiter") String waiter, @Query("restaurant") String restaurant, @Query("total_amount") String totalAmount, @Query("items") String items, @Query("section") String section);

    @POST("add_to_bill")
    Call<JsonObject> addToBill(@Query("waiter_id") String waiter_id, @Query("bill_id") String bill_id, @Query("date_time") String time, @Query("table_no") String table, @Query("no_of_guests") String guests, @Query("item_description") String description, @Query("condiments") String condiments, @Query("waiter") String waiter, @Query("restaurant") String restaurant, @Query("total_amount") String totalAmount, @Query("items") String items, @Query("section") String section);


    @Headers("Content-Type: application/json")
    @POST("offlinePostpaidOrders")
    Call<OfflineOrdersResponse> callOfflineOrders(@Query("orders") String orders);

    @Headers("Content-Type: application/json")
    @POST("offlineAddToBill")
    Call<JsonObject> callOfflineBillAdd(@Query("orders") String orders);

    @POST("void_items")
    Call<JsonObject> voidItem(@Query("waiter_id") String waiterId, @Query("item_id") String itemId, @Query("item_name") String itemName, @Query("waiter_name") String waiter, @Query("order_id") String orderId,@Query("amount") String amount,@Query("comments") String comment,@Query("quantity") Integer quantity,@Query("general_item_id") String general_item_id);


}
