package com.senarios.Ollaroerp.Utitlities;

public class FragmentTags {
    public static final String HOME="Home";
    public static final String PLACEORDER="Place Order";
    public static final String ORDERSINPROGRESS="InprogressOrdersFragment in Progress";
    public static final String GENERATEBILL="Generate Bills";
    public static final String CREATEVOUCHER="Create Voucher";
    public static final String CHANGEPASSWORD="Change Password";
    public static final String LOGIN="Login";
    public static final String FORGOTPASSWORD="Forgot Password";
    public static final String MENU="Menu";
    public static final String MENUCAT="Categories";
    public static final String SUBCATMENU="SubcategoryMenu";
    public static final String SUBCATMENUITEM="SubcategoryMenuItem";
    public static final String MPESADIALOGUE="mpesaDialog";
    public static final String COMPLIMENTRYDIALOGUE="ComplimentryDialogue";
    public static final String CARTFRAGMENT="Items Cart";
    public static final String CHECKOUT="Checkout";
    public static final String PAYMENTMETHOD="PaymentMethod";
    public static final String ORDERACCEPTED="OrderAccepted";
    public static final String COMBINEDIALOGUE="CombinePayment";
    public static final String CHOOSETABLE="ChooseTable";
    public static final String COMBINEBILL="Combine Bill";
    public static final String CHOOSEBILL="Choose Bill";
    public static final String SELECTEDSUBCAT="SelectedSubcat";
    public static final String CONFIRMCOMBINE="CONFIRMCOMBINE";
    public static final String GENBILLDIALOGUE="GENBILLDIALOGUE";
    public static final String MANAGERHOME = "ManagerHomeFragment";
    public static final String VOIDITEM = "Void Item";
    public static final String BILLTRANSFER = "BillTransfer";
    public static String SUBCATALCOSUBMENU="SubcatAlcoSub";
    public static String FAMILYGROUPFRAGMENT="FamilyGroupFragment";
    public static String SELECTORDERDIALOGUE="SelectOrderDialoge";
    public static String SEARCH="Search";
}
