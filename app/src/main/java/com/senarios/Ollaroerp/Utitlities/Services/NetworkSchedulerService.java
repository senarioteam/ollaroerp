package com.senarios.Ollaroerp.Utitlities.Services;

import android.app.AlertDialog;
import android.app.job.JobParameters;
import android.app.job.JobService;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Build;
import android.support.annotation.RequiresApi;
import android.util.Log;

import com.activeandroid.query.Delete;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.senarios.Ollaroerp.MainActivity;
import com.senarios.Ollaroerp.Models.BaseUnpaidBill;
import com.senarios.Ollaroerp.Models.OfflineAddToBills;
import com.senarios.Ollaroerp.Models.OfflineBillModel;
import com.senarios.Ollaroerp.Models.OfflineOrders;
import com.senarios.Ollaroerp.Models.OfflineOrdersResponse;
import com.senarios.Ollaroerp.Models.Order;
import com.senarios.Ollaroerp.Utitlities.ConnectivityReceiver;
import com.senarios.Ollaroerp.Utitlities.Constants;
import com.senarios.Ollaroerp.Utitlities.PrefUtils;
import com.senarios.Ollaroerp.Utitlities.Retrofit.GetDataService;
import com.senarios.Ollaroerp.Utitlities.Retrofit.RetrofitClientInstance;
import com.senarios.Ollaroerp.Utitlities.TinyDB;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.Modifier;
import java.util.ArrayList;
import java.util.Arrays;

import retrofit2.Call;
import retrofit2.Callback;

@RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
public class NetworkSchedulerService extends JobService implements
        ConnectivityReceiver.ConnectivityReceiverListener {

    ArrayList<OfflineOrders> allOrders;
    ArrayList<OfflineAddToBills> offlineAddToBills;
    private static final String TAG = NetworkSchedulerService.class.getSimpleName();

    private ConnectivityReceiver mConnectivityReceiver;

    @Override
    public void onCreate() {
        super.onCreate();
        Log.i(TAG, "Service created");
        mConnectivityReceiver = new ConnectivityReceiver(this);
    }


    /**
     * When the app's NetworkConnectionActivity is created, it starts this service. This is so that the
     * activity and this service can communicate back and forth. See "setUiCallback()"
     */
    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        Log.i(TAG, "onStartCommand");
        return START_NOT_STICKY;
    }


    @Override
    public boolean onStartJob(JobParameters params) {
        Log.i(TAG, "onStartJob" + mConnectivityReceiver);
        registerReceiver(mConnectivityReceiver, new IntentFilter(Constants.CONNECTIVITY_ACTION));
        return true;
    }

    @Override
    public boolean onStopJob(JobParameters params) {
        Log.i(TAG, "onStopJob");
        unregisterReceiver(mConnectivityReceiver);
        return true;
    }

    @Override
    public void onNetworkConnectionChanged(boolean isConnected) {
        if (isConnected) {
            if (PrefUtils.getString(getApplicationContext(), "is_logged_in", "false").equalsIgnoreCase("true")) {

                TinyDB tinyDB = new TinyDB(getApplicationContext());
                ArrayList<String> offlineOrders = tinyDB.getListString("OfflineOrders");
                allOrders = new ArrayList<>();
                for (int i = 0; i < offlineOrders.size(); i++) {
                    GsonBuilder builder = new GsonBuilder();
                    builder.excludeFieldsWithModifiers();
                    builder.excludeFieldsWithModifiers(Modifier.FINAL, Modifier.TRANSIENT, Modifier.STATIC);
                    Gson gson = builder.create();
                    OfflineOrders offlineOrder = gson.fromJson(offlineOrders.get(i), OfflineOrders.class);
                    allOrders.add(offlineOrder);
                }
                JSONArray ordersFinalArray = new JSONArray();
                for (int i = 0; i < allOrders.size(); i++) {
                    Order order = new Gson().fromJson(allOrders.get(i).getOrder(), Order.class);
                    String items = allOrders.get(i).getItems();
                    JSONObject preparedSingleOrder = new JSONObject();
                    try {
                        preparedSingleOrder.put("condiments", order.getCondiments());
                        preparedSingleOrder.put("waiter", order.getWaiter());
                        preparedSingleOrder.put("item_description", order.getItem_description());
                        preparedSingleOrder.put("no_of_guests", order.getNo_of_guests());
                        preparedSingleOrder.put("table_no", order.getTable_no());
                        preparedSingleOrder.put("date_time", order.getDate_time());
                        preparedSingleOrder.put("section", order.getSection());
                        preparedSingleOrder.put("waiter_id", String.valueOf(PrefUtils.getUserModel(getApplicationContext()).getId()));
                        preparedSingleOrder.put("restaurant", order.getRestaurant());
                        String amount = order.getTotal_amount();
                        amount = amount.replace("KSH ", "");
                        preparedSingleOrder.put("total_amount", amount);
                        preparedSingleOrder.put("items", items);
                        ordersFinalArray.put(preparedSingleOrder);

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }


                }
                genOrder(ordersFinalArray.toString());


                ArrayList<String> offlineBillsOrders = tinyDB.getListString("OfflineAddToBills");
                offlineAddToBills = new ArrayList<>();
                for (int i = 0; i < offlineBillsOrders.size(); i++) {
                    OfflineAddToBills offlineAddToBillsClass = new Gson().fromJson(offlineBillsOrders.get(i), OfflineAddToBills.class);
                    offlineAddToBills.add(offlineAddToBillsClass);
                }
                JSONArray billsFinalArray = new JSONArray();
                for (int i = 0; i < offlineAddToBills.size(); i++) {
                    Order order = new Gson().fromJson(offlineAddToBills.get(i).getOrder(), Order.class);
                    String items = offlineAddToBills.get(i).getItems();
                    String id = offlineAddToBills.get(i).getBilld();
                    ;
                    JSONObject preparedSingleBill = new JSONObject();
                    try {
                        preparedSingleBill.put("condiments", order.getCondiments());
                        preparedSingleBill.put("bill_id", id);
                        preparedSingleBill.put("waiter", order.getWaiter());
                        preparedSingleBill.put("item_description", order.getItem_description());
                        preparedSingleBill.put("no_of_guests", order.getNo_of_guests());
                        preparedSingleBill.put("table_no", order.getTable_no());
                        preparedSingleBill.put("date_time", order.getDate_time());
                        preparedSingleBill.put("section", order.getSection());
                        preparedSingleBill.put("waiter_id", String.valueOf(PrefUtils.getUserModel(getApplicationContext()).getId()));
                        preparedSingleBill.put("restaurant", order.getRestaurant());
                      /*  String amount = order.getTotal_amount();
                        amount = amount.replace("KSH ", "");*/
                        preparedSingleBill.put("total_amount", order.getTotal_amount());
                        preparedSingleBill.put("items", items);
                        billsFinalArray.put(preparedSingleBill);

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }


                }
                genBillsRequests(billsFinalArray.toString());
            }

        }

    }

    public void genOrder(final String orders) {
        GetDataService service = RetrofitClientInstance.getRetrofitInstance().create(GetDataService.class);

        Call<OfflineOrdersResponse> call = service.callOfflineOrders(orders);
        call.enqueue(new Callback<OfflineOrdersResponse>() {

            @Override
            public void onResponse(Call<OfflineOrdersResponse> call, retrofit2.Response<OfflineOrdersResponse> response) {
                if (response.body() != null) {
                    if (response.body().getSuccess().equalsIgnoreCase("Orders has been made Successfully!") && response.body().getCode() == 200) {

                        TinyDB tinyDB = new TinyDB(getApplicationContext());
                        PrefUtils.putString(getApplicationContext(), "hasOrders", "false");
                        if (PrefUtils.getString(getApplicationContext(), "hasBills", "false").equalsIgnoreCase("false")) {
                            tinyDB.clear();
                        }
                    }
                }

            }

            @Override
            public void onFailure(Call<OfflineOrdersResponse> call, Throwable t) {

            }
        });


    }

    public void genBillsRequests(final String requests) {
        GetDataService service = RetrofitClientInstance.getRetrofitInstance().create(GetDataService.class);

        Call<JsonObject> call = service.callOfflineBillAdd(requests);
        call.enqueue(new Callback<JsonObject>() {

            @Override
            public void onResponse(Call<JsonObject> call, retrofit2.Response<JsonObject> response) {
                if (response.body() != null) {
                    JsonObject jsonObject = response.body();
                    if (jsonObject.get("success").getAsString().equalsIgnoreCase("Orders has been made Successfully!") && response.code() == 200) {
                        if (response.body().has("bill_id")){
                            JsonArray array =response.body().get("bill_id").getAsJsonArray();
                            if (array.size()>0){
                                String[] arr = array.toString().replace("},{", " ,").split(" ");
                                String bills= Arrays.toString(arr).replace("[","").replace("]","");
                                new AlertDialog.Builder(MainActivity.activity, android.R.style.Theme_Material_Dialog_Alert)
                                        .setTitle("Message")
                                        .setMessage("You added some orders in bills "+bills+" which have been paid. Please Order again!")
                                        .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                                            @Override
                                            public void onClick(DialogInterface dialog, int which) {
                                                dialog.dismiss();
                                            }
                                        })
                                        .show();
                            }
                        }

                        TinyDB tinyDB = new TinyDB(getApplicationContext());
                        PrefUtils.putString(getApplicationContext(), "hasBills", "false");
                        if (PrefUtils.getString(getApplicationContext(), "hasOrders", "false").equalsIgnoreCase("false")) {
                            tinyDB.clear();
                        }



                    } else {

                    }
                    getUnpaidBills();
                    /*getEveryThing();*/
                } else {

                }

            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {

            }
        });


    }

    public void getUnpaidBills() {
        GetDataService service = RetrofitClientInstance.getRetrofitInstance().create(GetDataService.class);
        Call<BaseUnpaidBill> call = service.getUnpaidBills(String.valueOf(PrefUtils.getUserModel(getApplicationContext()).getId()));
        call.enqueue(new Callback<BaseUnpaidBill>() {

            @Override
            public void onResponse(Call<BaseUnpaidBill> call, retrofit2.Response<BaseUnpaidBill> response) {
                if (response.body() != null) {
                    new Delete().from(OfflineBillModel.class).execute();
                    if (response.body().getBills().size()!=0){
                        for (int k = 0; k < response.body().getBills().size(); k++) {
                            OfflineBillModel offlineBillModel = new OfflineBillModel(response.body().getBills().get(k));
                            if (MainActivity.getBillRow(response.body().getBills().get(k)) == null) {
                                offlineBillModel.save();
                            }}
                    }

                } else {
                    getUnpaidBills();
                }

            }

            @Override
            public void onFailure(Call<BaseUnpaidBill> call, Throwable t) {
                getUnpaidBills();
            }
        });


    }

    /*public void getEveryThing() {
        GetDataService service = RetrofitClientInstance.getRetrofitInstance().create(GetDataService.class);

        Call<BaseAllItemsResponse> call = service.getEverything(String.valueOf(PrefUtils.getUserModel(getApplicationContext()).getId()));
        call.enqueue(new Callback<BaseAllItemsResponse>() {

            @Override
            public void onResponse(Call<BaseAllItemsResponse> call, final retrofit2.Response<BaseAllItemsResponse> response) {
                if (response.body() != null) {

                    new getEverything().execute(response.body());

                  *//*  new Delete().from(OfflineMajorGroup.class).execute();
                    new Delete().from(OfflineSubMajorGroup.class).execute();
                    new Delete().from(OfflineAlchoholicGroup.class).execute();
                    new Delete().from(OfflineFamilyGroup.class).execute();
                    new Delete().from(OfflineSubAlchoholicGroup.class).execute();
                    new Delete().from(OfflineItemModel.class).execute();
                    new Delete().from(OfflineTableModel.class).execute();
                    new Delete().from(OfflineMajorGroup.class).execute();
                    new Delete().from(OfflineBillModel.class).execute();
                    new Delete().from(OfflinePostOrderItems.class).execute();
                    new Delete().from(OfflineOrderItems.class).execute();
                    new Delete().from(OfflineMenuItem.class).execute();*//*




                   *//* for (int i = 0; i < response.body().getMajorGroup().size(); i++) {
                        if (response.body().getMajorGroup().size()!=0) {
                            OfflineMajorGroup offlineMajorGroup = new OfflineMajorGroup();
                            offlineMajorGroup.setMajor_id(response.body().getMajorGroup().get(i).getId());
                            offlineMajorGroup.setA_image(response.body().getMajorGroup().get(i).getAImage());
                            offlineMajorGroup.setName(response.body().getMajorGroup().get(i).getName());
                            offlineMajorGroup.setCreated_at(response.body().getMajorGroup().get(i).getCreatedAt());
                            offlineMajorGroup.setUpdated_at(response.body().getMajorGroup().get(i).getUpdatedAt());
                            offlineMajorGroup.setDisplay_order(response.body().getMajorGroup().get(i).getDisplayOrder());
                            if (MainActivity.getMajorGroupRow(response.body().getMajorGroup().get(i)) == null) {
                                offlineMajorGroup.save();
                            }
                        }
                        if (response.body().getSubGroups().size()!=0){
                            for (int a = 0; a < response.body().getSubGroups().size(); a++) {
                                OfflineSubMajorGroup offlineSubMajorGroup = new OfflineSubMajorGroup(response.body().getSubGroups().get(a));
                                offlineSubMajorGroup.setMajor_id(String.valueOf(response.body().getMajorGroup().get(i).getId()));
                                if (MainActivity.getSubMajorGroupRow(response.body().getSubGroups().get(a)) == null) {
                                    offlineSubMajorGroup.save();
                                }
                            }

                        }
                        if (response.body().getAlcoholGroups().size()!=0){

                            for (int b = 0; b < response.body().getAlcoholGroups().size(); b++) {
                                OfflineAlchoholicGroup offlineAlchoholicGroup = new OfflineAlchoholicGroup(response.body().getAlcoholGroups().get(b));
                                if (MainActivity.getAlcoGroupRow(response.body().getAlcoholGroups().get(b)) == null) {
                                    offlineAlchoholicGroup.save();
                                }
                            }

                        }
                        if (response.body().getFamilyGroups().size()!=0){
                            for (int c = 0; c < response.body().getFamilyGroups().size(); c++) {
                                OfflineFamilyGroup offlineFamilyGroup = new OfflineFamilyGroup(response.body().getFamilyGroups().get(c));
                                if (MainActivity.getFamGroupRow(response.body().getFamilyGroups().get(c)) == null) {
                                    offlineFamilyGroup.save();
                                }
                            }
                        }
                        if (response.body().getSubAlcoholicGroups().size()!=0){
                            for (int d = 0; d < response.body().getSubAlcoholicGroups().size(); d++) {
                                OfflineSubAlchoholicGroup offlineAlchoholicGroup = new OfflineSubAlchoholicGroup(response.body().getSubAlcoholicGroups().get(d));
                                if (MainActivity.getSubAlcoGroupRow(response.body().getSubAlcoholicGroups().get(d)) == null) {
                                    offlineAlchoholicGroup.save();
                                }}
                        }
                        if (response.body().getItems().size()!=0){
                            for (int e = 0; e < response.body().getItems().size(); e++) {
                                OfflineItemModel offlineItemModel = new OfflineItemModel(response.body().getItems().get(e));
                                if (MainActivity.getItemRow(response.body().getItems().get(e)) == null) {
                                    offlineItemModel.save();
                                }
                            }

                        }
                        if (response.body().getTables().size()!=0) {
                            for (int f = 0; f < response.body().getTables().size(); f++) {
                                if (response.body().getTables().get(f).getwaiter_id().equalsIgnoreCase(String.valueOf(PrefUtils.getUserModel(getApplicationContext()).getId()))) {
                                    OfflineTableModel offlineTableModel = new OfflineTableModel(response.body().getTables().get(f));
                                    if (MainActivity.getTableRow(response.body().getTables().get(f)) == null) {
                                        offlineTableModel.save();
                                    }
                                }
                            }
                        }

                        if (response.body().getBillModels().size()!=0){
                            for (int k = 0; k < response.body().getBillModels().size(); k++) {
                                OfflineBillModel offlineBillModel = new OfflineBillModel(response.body().getBillModels().get(k));
                                if (MainActivity.getBillRow(response.body().getBillModels().get(k)) == null) {
                                    offlineBillModel.save();
                                }}
                        }



                    }
                    if (response.body().getPaidOrders().size()!=0) {
                        for (int n = 0; n < response.body().getPaidOrders().size(); n++) {
                            for (int dum=0;dum<response.body().getPaidOrders().get(n).getOrder_items().size();dum++) {
                                OfflineOrderItems offlineOrderItems = new OfflineOrderItems(response.body().getPaidOrders().get(n).getOrder_items().get(dum));
                                if (MainActivity.getOrderItemRow(offlineOrderItems) == null) {
                                    offlineOrderItems.save();
                                }
                            }
                        }
                    }
                    if (response.body().getPaidOrders().size()!=0){
                        for(int m=0;m<response.body().getPaidOrders().size();m++) {
                            OfflinePostOrderItems offlinePostOrderItems = new OfflinePostOrderItems(response.body().getPaidOrders().get(m));
                            if (MainActivity.getPostOrderRow(offlinePostOrderItems) == null) {
                                offlinePostOrderItems.save();
                            }
                        }*//*
                    *//*}*//*



                } else {
                    getEveryThing();
                  }
                *//*pd.dismiss();*//*

            }

            @Override
            public void onFailure(Call<BaseAllItemsResponse> call, Throwable t) {
                getEveryThing();
            }
        });


    }

    public class getEverything extends AsyncTask<BaseAllItemsResponse,Void,Void> {
        @Override
        protected Void doInBackground(BaseAllItemsResponse... baseAllItemsResponses) {
          *//*  new Delete().from(OfflineMajorGroup.class).execute();
            new Delete().from(OfflineSubMajorGroup.class).execute();
            new Delete().from(OfflineAlchoholicGroup.class).execute();
            new Delete().from(OfflineFamilyGroup.class).execute();
            new Delete().from(OfflineSubAlchoholicGroup.class).execute();
            new Delete().from(OfflineItemModel.class).execute();
            new Delete().from(OfflineTableModel.class).execute();
            new Delete().from(OfflineMajorGroup.class).execute();
            new Delete().from(OfflineBillModel.class).execute();
            new Delete().from(OfflinePostOrderItems.class).execute();
            new Delete().from(OfflineOrderItems.class).execute();
            new Delete().from(OfflineMenuItem.class).execute();
*//*



            for (int i = 0; i < baseAllItemsResponses[0].getMajorGroup().size(); i++) {
                if (baseAllItemsResponses[0].getMajorGroup().size()!=0) {
                    OfflineMajorGroup offlineMajorGroup = new OfflineMajorGroup();
                    offlineMajorGroup.setMajor_id(baseAllItemsResponses[0].getMajorGroup().get(i).getId());
                    offlineMajorGroup.setA_image(baseAllItemsResponses[0].getMajorGroup().get(i).getAImage());
                    offlineMajorGroup.setName(baseAllItemsResponses[0].getMajorGroup().get(i).getName());
                    offlineMajorGroup.setCreated_at(baseAllItemsResponses[0].getMajorGroup().get(i).getCreatedAt());
                    offlineMajorGroup.setUpdated_at(baseAllItemsResponses[0].getMajorGroup().get(i).getUpdatedAt());
                    offlineMajorGroup.setDisplay_order(baseAllItemsResponses[0].getMajorGroup().get(i).getDisplayOrder());
                    if (MainActivity.getMajorGroupRow(baseAllItemsResponses[0].getMajorGroup().get(i)) == null) {
                        offlineMajorGroup.save();
                    }
                }
                if (baseAllItemsResponses[0].getSubGroups().size()!=0){
                    for (int a = 0; a <baseAllItemsResponses[0].getSubGroups().size(); a++) {
                        OfflineSubMajorGroup offlineSubMajorGroup = new OfflineSubMajorGroup(baseAllItemsResponses[0].getSubGroups().get(a));
                        offlineSubMajorGroup.setMajor_id(String.valueOf(baseAllItemsResponses[0].getMajorGroup().get(i).getId()));
                        if (MainActivity.getSubMajorGroupRow(baseAllItemsResponses[0].getSubGroups().get(a)) == null) {
                            offlineSubMajorGroup.save();
                        }
                    }

                }
                if (baseAllItemsResponses[0].getAlcoholGroups().size()!=0){

                    for (int b = 0; b < baseAllItemsResponses[0].getAlcoholGroups().size(); b++) {
                        OfflineAlchoholicGroup offlineAlchoholicGroup = new OfflineAlchoholicGroup(baseAllItemsResponses[0].getAlcoholGroups().get(b));
                        if (MainActivity.getAlcoGroupRow(baseAllItemsResponses[0].getAlcoholGroups().get(b)) == null) {
                            offlineAlchoholicGroup.save();
                        }
                    }

                }
                if (baseAllItemsResponses[0].getFamilyGroups().size()!=0){
                    for (int c = 0; c < baseAllItemsResponses[0].getFamilyGroups().size(); c++) {
                        OfflineFamilyGroup offlineFamilyGroup = new OfflineFamilyGroup(baseAllItemsResponses[0].getFamilyGroups().get(c));
                        if (MainActivity.getFamGroupRow(baseAllItemsResponses[0].getFamilyGroups().get(c)) == null) {
                            offlineFamilyGroup.save();
                        }
                    }
                }
                if (baseAllItemsResponses[0].getSubAlcoholicGroups().size()!=0){
                    for (int d = 0; d < baseAllItemsResponses[0].getSubAlcoholicGroups().size(); d++) {
                        OfflineSubAlchoholicGroup offlineAlchoholicGroup = new OfflineSubAlchoholicGroup(baseAllItemsResponses[0].getSubAlcoholicGroups().get(d));
                        if (MainActivity.getSubAlcoGroupRow(baseAllItemsResponses[0].getSubAlcoholicGroups().get(d)) == null) {
                            offlineAlchoholicGroup.save();
                        }}
                }
                if (baseAllItemsResponses[0].getItems().size()!=0){
                    for (int e = 0; e < baseAllItemsResponses[0].getItems().size(); e++) {
                        OfflineItemModel offlineItemModel = new OfflineItemModel(baseAllItemsResponses[0].getItems().get(e));
                        if (MainActivity.getItemRow(baseAllItemsResponses[0].getItems().get(e)) == null) {
                            offlineItemModel.save();
                        }
                    }

                }
                if (baseAllItemsResponses[0].getTables().size()!=0) {
                    for (int f = 0; f < baseAllItemsResponses[0].getTables().size(); f++) {
                        if (baseAllItemsResponses[0].getTables().get(f).getwaiter_id().equalsIgnoreCase(String.valueOf(PrefUtils.getUserModel(getApplicationContext()).getId()))) {
                            OfflineTableModel offlineTableModel = new OfflineTableModel(baseAllItemsResponses[0].getTables().get(f));
                            if (MainActivity.getTableRow(baseAllItemsResponses[0].getTables().get(f)) == null) {
                                offlineTableModel.save();
                            }
                        }
                    }
                }

                if (baseAllItemsResponses[0].getBillModels().size()!=0){
                    for (int k = 0; k < baseAllItemsResponses[0].getBillModels().size(); k++) {
                        OfflineBillModel offlineBillModel = new OfflineBillModel(baseAllItemsResponses[0].getBillModels().get(k));
                        if (MainActivity.getBillRow(baseAllItemsResponses[0].getBillModels().get(k)) == null) {
                            offlineBillModel.save();
                        }}
                }



            }
            if (baseAllItemsResponses[0].getPaidOrders().size()!=0) {
                for (int n = 0; n < baseAllItemsResponses[0].getPaidOrders().size(); n++) {
                    for (int dum=0;dum<baseAllItemsResponses[0].getPaidOrders().get(n).getOrder_items().size();dum++) {
                        OfflineOrderItems offlineOrderItems = new OfflineOrderItems(baseAllItemsResponses[0].getPaidOrders().get(n).getOrder_items().get(dum));
                        if (MainActivity.getOrderItemRow(offlineOrderItems) == null) {
                            offlineOrderItems.save();
                        }
                    }
                }
            }
            if (baseAllItemsResponses[0].getPaidOrders().size()!=0){
                for(int m=0;m<baseAllItemsResponses[0].getPaidOrders().size();m++) {
                    OfflinePostOrderItems offlinePostOrderItems = new OfflinePostOrderItems(baseAllItemsResponses[0].getPaidOrders().get(m));
                    if (MainActivity.getPostOrderRow(offlinePostOrderItems) == null) {
                        offlinePostOrderItems.save();
                    }
                }
            }

            return null;
        }

        @Override
        protected void onPreExecute() {

            super.onPreExecute();
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
        }




    }*/
}
