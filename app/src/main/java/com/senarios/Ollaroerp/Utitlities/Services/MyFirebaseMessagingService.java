package com.senarios.Ollaroerp.Utitlities.Services;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.media.RingtoneManager;
import android.net.Uri;
import android.support.v4.app.NotificationCompat;

import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;
import com.google.gson.Gson;
import com.senarios.Ollaroerp.Fragments.DialogeFragments.MpesaDialogueFragment;
import com.senarios.Ollaroerp.MainActivity;
import com.senarios.Ollaroerp.Models.NotificationModel;
import com.senarios.Ollaroerp.R;
import com.senarios.Ollaroerp.Utitlities.TinyDB;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Map;


public class MyFirebaseMessagingService extends FirebaseMessagingService {

    ArrayList<String> notificationModels;

    @Override
    public void onMessageReceived(RemoteMessage message) {

        try {
            Map<String, String> params = message.getData();
            JSONObject object = new JSONObject(params);
            String jsonArray = object.getString("body");
            JSONArray jsonArray1 = new JSONArray(jsonArray);
            JSONObject jsonObject = jsonArray1.getJSONObject(0);
            String order_id = jsonObject.getString("order_id");
            String item_id = jsonObject.getString("item_id");
            String waiter_id = jsonObject.getString("waiter_id");
            String item_name = jsonObject.getString("item_name");
            String waiter_name = jsonObject.getString("waiter_name");
            NotificationModel notificationModel = new NotificationModel();
            notificationModel.setWaiterId(waiter_id);
            notificationModel.setItemId(item_id);
            notificationModel.setItemName(item_name);
            notificationModel.setWaiterName(waiter_name);
            notificationModel.setOrderId(order_id);
            TinyDB tinyDB = new TinyDB(getApplicationContext());

            if (tinyDB.getListString("notifications") == null) {
                notificationModels = new ArrayList<>();
            } else {
                notificationModels = tinyDB.getListString("notifications");
            }
            Gson gson = new Gson();
            notificationModels.add(gson.toJson(notificationModel));
            tinyDB.putListString("notifications", notificationModels);
            sendMyNotification();
        } catch (Exception e) {
            e.printStackTrace();
            try {
                Map<String, String> params = message.getData();
                JSONObject object = new JSONObject(params);
                String jsonArray = object.getString("body");
                JSONArray jsonArray1 = new JSONArray(jsonArray);
                JSONObject jsonObject = jsonArray1.getJSONObject(0);
                String message1 = jsonObject.getString("message");
                sendMyNotification(message1);
            } catch (JSONException e1) {
                if (message.getNotification().getBody().equalsIgnoreCase("1")) {
                    try {
                        MpesaDialogueFragment.response("1");
                    } catch (Exception e2) {

                    }
                } else {
                    try {
                        MpesaDialogueFragment.response("0");
                    } catch (Exception e3) {

                    }
                }
            }

        }
    }


    private void sendMyNotification() {

        Intent intent = new Intent(this, MainActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0, intent, PendingIntent.FLAG_ONE_SHOT);

        Uri soundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this)
                .setSmallIcon(R.mipmap.ic_launcher)
                .setContentTitle("Item Cancellation Request")
                .setContentText("You have a new item cancellation request")
                .setAutoCancel(true)
                .setSound(soundUri)
                .setContentIntent(pendingIntent);

        NotificationManager notificationManager =
                (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

        notificationManager.notify(0, notificationBuilder.build());
    }

    private void sendMyNotification(String message) {
        Intent intent = new Intent(this, MainActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0, intent, PendingIntent.FLAG_ONE_SHOT);

        Uri soundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this)
                .setSmallIcon(R.mipmap.ic_launcher)
                .setContentTitle("Cancel Status")
                .setContentText(message)
                .setAutoCancel(true)
                .setSound(soundUri)
                .setContentIntent(pendingIntent);

        NotificationManager notificationManager =
                (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

        notificationManager.notify(0, notificationBuilder.build());
    }


}
