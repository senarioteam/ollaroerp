package com.senarios.Ollaroerp.Models;

import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "id",
        "bill_id",
        "date_time",
        "table_no",
        "section",
        "no_of_guests",
        "item_description",
        "condiments",
        "waiter_id",
        "waiter",
        "restaurant",
        "print_class_id",
        "total_amount",
        "status",
        "cooking_status",
        "created_at",
        "updated_at",
        "auto_invoice",
        "auto_docket",
        "discount_percent",
        "discount_reason",
        "discountType",
        "discount_amount",
        "discounted_amount",
        "order_items"
})
public class PostOrderItems {

    @JsonProperty("id")
    private Integer id;
    @JsonProperty("bill_id")
    private String bill_id;
    @JsonProperty("date_time")
    private String date_time;
    @JsonProperty("table_no")
    private String table_no;
    @JsonProperty("no_of_guests")
    private String no_of_guests;
    @JsonProperty("item_description")
    private String item_description;
    @JsonProperty("condiments")
    private Object condiments;
    @JsonProperty("waiter_id")
    private String waiter_id;
    @JsonProperty("waiter")
    private String waiter;
    @JsonProperty("restaurant")
    private String restaurant;
    @JsonProperty("total_amount")
    private String total_amount;
    @JsonProperty("status")
    private String status;
    @JsonProperty("created_at")
    private String created_at;
    @JsonProperty("updated_at")
    private String updated_at;
    @JsonProperty("order_items")
    private List<OrderItem> order_items = null;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("id")
    public Integer getId() {
        return id;
    }

    @JsonProperty("id")
    public void setId(Integer id) {
        this.id = id;
    }

    @JsonProperty("bill_id")
    public String  getBillId() {
        return bill_id;
    }

    @JsonProperty("bill_id")
    public void setId(String bill_id) {
        this.bill_id = bill_id;
    }

    @JsonProperty("date_time")
    public String getdate_time() {
        return date_time;
    }

    @JsonProperty("date_time")
    public void setdate_time(String date_time) {
        this.date_time = date_time;
    }

    @JsonProperty("table_no")
    public String gettable_no() {
        return table_no;
    }

    @JsonProperty("table_no")
    public void settable_no(String table_no) {
        this.table_no = table_no;
    }

    @JsonProperty("no_of_guests")
    public String getno_of_guests() {
        return no_of_guests;
    }

    @JsonProperty("no_of_guests")
    public void setno_of_guests(String no_of_guests) {
        this.no_of_guests = no_of_guests;
    }

    @JsonProperty("item_description")
    public String getitem_description() {
        return item_description;
    }

    @JsonProperty("item_description")
    public void setitem_description(String item_description) {
        this.item_description = item_description;
    }

    @JsonProperty("condiments")
    public Object getCondiments() {
        return condiments;
    }

    @JsonProperty("condiments")
    public void setCondiments(Object condiments) {
        this.condiments = condiments;
    }

    @JsonProperty("waiter_id")
    public String getwaiter_id() {
        return waiter_id;
    }

    @JsonProperty("waiter_id")
    public void setwaiter_id(String waiter_id) {
        this.waiter_id = waiter_id;
    }

    @JsonProperty("waiter")
    public String getWaiter() {
        return waiter;
    }

    @JsonProperty("waiter")
    public void setWaiter(String waiter) {
        this.waiter = waiter;
    }

    @JsonProperty("restaurant")
    public String getRestaurant() {
        return restaurant;
    }

    @JsonProperty("restaurant")
    public void setRestaurant(String restaurant) {
        this.restaurant = restaurant;
    }

    @JsonProperty("total_amount")
    public String gettotal_amount() {
        return total_amount;
    }

    @JsonProperty("total_amount")
    public void settotal_amount(String total_amount) {
        this.total_amount = total_amount;
    }

    @JsonProperty("status")
    public String getStatus() {
        return status;
    }

    @JsonProperty("status")
    public void setStatus(String status) {
        this.status = status;
    }

    @JsonProperty("created_at")
    public String getcreated_at() {
        return created_at;
    }

    @JsonProperty("created_at")
    public void setcreated_at(String created_at) {
        this.created_at = created_at;
    }

    @JsonProperty("updated_at")
    public String getupdated_at() {
        return updated_at;
    }

    @JsonProperty("updated_at")
    public void setupdated_at(String updated_at) {
        this.updated_at = updated_at;
    }

    @JsonProperty("items")
    public List<OrderItem> getItems() {
        return order_items;
    }

    @JsonProperty("order_items")
    public void setItems(List<OrderItem> items) {
        this.order_items = items;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}