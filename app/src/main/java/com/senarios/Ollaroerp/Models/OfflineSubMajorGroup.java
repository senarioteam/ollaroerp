package com.senarios.Ollaroerp.Models;

import com.activeandroid.Model;
import com.activeandroid.annotation.Column;
import com.activeandroid.annotation.Table;
import com.senarios.Ollaroerp.MainActivity;

import java.util.ArrayList;
import java.util.List;

@Table(name = "SubMajorGroup")
public class OfflineSubMajorGroup extends Model {

    @Column(name = "major_id")
    private String major_id;

    public String getMajor_id() {
        return major_id;
    }

    public void setMajor_id(String major_id) {
        this.major_id = major_id;
    }

    @Column(name = "sub_major_id")
    private Integer sub_major_id;
    @Column(name = "name")
    private String name;
    @Column(name = "major_group_name")
    private String major_group_name;
    @Column(name = "description")
    private String description;
    @Column(name = "a_image")
    private String a_image;
    @Column(name = "created_at")
    private String created_at;
    @Column(name = "updated_at")
    private String updated_at;
    @Column(name = "menu_items")
    private List<MenuItem> menu_items = null;

    public OfflineSubMajorGroup(SubMajorGroup subMajorGroup){
        setA_image(subMajorGroup.getAImage());
        setCreated_at(subMajorGroup.getCreatedAt());
        setUpdated_at(subMajorGroup.getUpdatedAt());
        setName(subMajorGroup.getName());
        setSub_major_id(subMajorGroup.getId());
        setDescription(subMajorGroup.getDescription());
        setMajor_group_name(subMajorGroup.getMajorGroupName());
        setMenu_items(subMajorGroup.getMenuItems());
    }

    public OfflineSubMajorGroup() {
    }


    public Integer getSub_major_id() {
        return sub_major_id;
    }

    public void setSub_major_id(Integer sub_major_id) {
        this.sub_major_id = sub_major_id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getMajor_group_name() {
        return major_group_name;
    }

    public void setMajor_group_name(String major_group_name) {
        this.major_group_name = major_group_name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getA_image() {
        return a_image;
    }

    public void setA_image(String a_image) {
        this.a_image = a_image;
    }

    public String getCreated_at() {
        return created_at;
    }

    public void setCreated_at(String created_at) {
        this.created_at = created_at;
    }

    public String getUpdated_at() {
        return updated_at;
    }

    public void setUpdated_at(String updated_at) {
        this.updated_at = updated_at;
    }

    public List<MenuItem> getMenu_items() {
        List<OfflineMenuItem> items=MainActivity.getAllMenuItems(String.valueOf(getSub_major_id()));
        if(items.size()!=0) {
            menu_items=new ArrayList<>();
            for(int i=0;i<items.size();i++){
                MenuItem menuItem=new MenuItem();
                menuItem.seta_image(items.get(i).getA_image());
                menuItem.setDescription(items.get(i).getDescription());
                menuItem.setcreated_at(items.get(i).getCreated_at());
                menuItem.setsub_major_groups_id(items.get(i).getSub_major_groups_id());
                menuItem.setId(items.get(i).getMenu_id());
                menuItem.setIs_checked(items.get(i).getIs_checked());
                menuItem.setTimefrom(items.get(i).getTimefrom());
                menuItem.setTimeto(items.get(i).getTimeto());
                menuItem.setName(items.get(i).getName());
                menu_items.add(menuItem);
            }
        }
        return menu_items;
    }

    public void setMenu_items(List<MenuItem> menu_items) {

        this.menu_items = menu_items;
        for(int i=0;i<menu_items.size();i++){
            OfflineMenuItem offlineMenuItem=new OfflineMenuItem(menu_items.get(i));
            if(MainActivity.getMenuItemRow(offlineMenuItem)==null){
                offlineMenuItem.save();
            }
        }
    }
}
