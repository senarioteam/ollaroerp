package com.senarios.Ollaroerp.Models;

import java.util.ArrayList;

public class MenuCategory {

    String categoryName;
    ArrayList<MenuSubcategory> menuSubcategories;

    public String getCategoryName() {
        return categoryName;
    }

    public void setCategoryName(String categoryName) {
        this.categoryName = categoryName;
    }

    public ArrayList<MenuSubcategory> getMenuSubcategories() {
        return menuSubcategories;
    }

    public void setMenuSubcategories(ArrayList<MenuSubcategory> menuSubcategories) {
        this.menuSubcategories = menuSubcategories;
    }
}
