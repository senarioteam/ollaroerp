package com.senarios.Ollaroerp.Models;

public class MenuSubcategory {
    String subcatImage;
    String subcatName;
    String subcatOpeningTIme;
    String subcatClosingTIme;

    public String getSubcatClosingTIme() {
        return subcatClosingTIme;
    }

    public void setSubcatClosingTIme(String subcatClosingTIme) {
        this.subcatClosingTIme = subcatClosingTIme;
    }

    public String getSubcatImage() {

        return subcatImage;
    }

    public void setSubcatImage(String subcatImage) {
        this.subcatImage = subcatImage;
    }

    public String getSubcatName() {
        return subcatName;
    }

    public void setSubcatName(String subcatName) {
        this.subcatName = subcatName;
    }

    public String getSubcatOpeningTIme() {
        return subcatOpeningTIme;
    }

    public void setSubcatOpeningTIme(String subcatOpeningTIme) {
        this.subcatOpeningTIme = subcatOpeningTIme;
    }
}
