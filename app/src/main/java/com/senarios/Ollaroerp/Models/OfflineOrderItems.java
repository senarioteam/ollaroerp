package com.senarios.Ollaroerp.Models;

import com.activeandroid.Model;
import com.activeandroid.annotation.Column;
import com.activeandroid.annotation.Table;

@Table(name = "OfflineOrderItems")
public class OfflineOrderItems extends Model {
    @Column(name = "orderid")
    private Integer orderid;
    @Column(name = "post_paid_orders_id")
    private String post_paid_orders_id;
    @Column(name = "name")
    private String name;
    @Column(name = "quantity")
    private Integer quantity;
    @Column(name = "price")
    private String price;
    @Column(name = "image")
    private String image;
    @Column(name = "created_at")
    private String created_at;
    @Column(name = "updated_at")
    private String updated_at;

    private boolean isChecked=false;

    public boolean isChecked() {
        return isChecked;
    }

    public void setChecked(boolean checked) {
        isChecked = checked;
    }

    public Boolean getChecked() {
        return isChecked;
    }

    public OfflineOrderItems(OrderItem orderItem) {
        setCreated_at(orderItem.getcreated_at());
        setImage(orderItem.getImage());
        setName(orderItem.getName());
        setOrderid(orderItem.getId());
        setPost_paid_orders_id(orderItem.getpost_paid_orders_id());
        setUpdated_at(orderItem.getupdated_at());
        setPrice(orderItem.getPrice());
        setQuantity(orderItem.getQuantity());




    }

    public OfflineOrderItems() {
    }

    public Integer getOrderid() {
        return orderid;
    }

    public void setOrderid(Integer orderid) {
        this.orderid = orderid;
    }

    public String getPost_paid_orders_id() {
        return post_paid_orders_id;
    }

    public void setPost_paid_orders_id(String post_paid_orders_id) {
        this.post_paid_orders_id = post_paid_orders_id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(Integer quantity) {
        this.quantity = quantity;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getCreated_at() {
        return created_at;
    }

    public void setCreated_at(String created_at) {
        this.created_at = created_at;
    }

    public String getUpdated_at() {
        return updated_at;
    }

    public void setUpdated_at(String updated_at) {
        this.updated_at = updated_at;
    }
}
