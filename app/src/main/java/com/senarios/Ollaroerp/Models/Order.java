package com.senarios.Ollaroerp.Models;


public class Order {
    String date_time,table_no,no_of_guests,item_description,condiments,waiter,restaurant,total_amount,section;


    public String getSection() {
        return section;
    }

    public void setSection(String section) {
        this.section = section;
    }

    public Order(String date_time, String table_no, String no_of_guests, String item_description, String condiments, String waiter, String restaurant, String total_amount, String section) {
        this.date_time = date_time;
        this.table_no = table_no;
        this.no_of_guests = no_of_guests;
        this.item_description = item_description;
        this.condiments = condiments;
        this.waiter = waiter;
        this.restaurant = restaurant;
        this.total_amount = total_amount;
        this.section=section;


    }

    public Order() {
    }

    public String getDate_time() {
        return date_time;
    }

    public void setDate_time(String date_time) {
        this.date_time = date_time;
    }

    public String getTable_no() {
        return table_no;
    }

    public void setTable_no(String table_no) {
        this.table_no = table_no;
    }

    public String getNo_of_guests() {
        return no_of_guests;
    }

    public void setNo_of_guests(String no_of_guests) {
        this.no_of_guests = no_of_guests;
    }

    public String getItem_description() {
        return item_description;
    }

    public void setItem_description(String item_description) {
        this.item_description = item_description;
    }

    public String getCondiments() {
        return condiments;
    }

    public void setCondiments(String condiments) {
        this.condiments = condiments;
    }

    public String getWaiter() {
        return waiter;
    }

    public void setWaiter(String waiter) {
        this.waiter = waiter;
    }

    public String getRestaurant() {
        return restaurant;
    }

    public void setRestaurant(String restaurant) {
        this.restaurant = restaurant;
    }

    public String getTotal_amount() {
        return total_amount;
    }

    public void setTotal_amount(String total_amount) {
        this.total_amount = total_amount;
    }
}
