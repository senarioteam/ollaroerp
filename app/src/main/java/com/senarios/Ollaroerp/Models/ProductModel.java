package com.senarios.Ollaroerp.Models;



public class ProductModel  {

    String productName;
    String productFranchise;
    String price;
    String productImage;

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public String getProductFranchise() {
        return productFranchise;
    }

    public void setProductFranchise(String productFranchise) {
        this.productFranchise = productFranchise;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getProductImage() {
        return productImage;
    }

    public void setProductImage(String productImage) {
        this.productImage = productImage;
    }
}
