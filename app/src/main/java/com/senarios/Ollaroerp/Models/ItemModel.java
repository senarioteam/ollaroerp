package com.senarios.Ollaroerp.Models;

import java.util.HashMap;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "id",
        "name",
        "price",
        "description",
        "a_image",
        "family_groups_id",
        "print_class_id",
        "condiments_groups_id",
        "plu_number",
        "tax",
        "is_show_menu",
        "created_at",
        "updated_at"
})
public class ItemModel {

    @JsonProperty("id")
    private Integer id;
    @JsonProperty("name")
    private String name;
    @JsonProperty("price")
    private String price;
    @JsonProperty("description")
    private String description;
    @JsonProperty("a_image")
    private String a_image;
    @JsonProperty("family_groups_id")
    private String family_groups_id;
    @JsonProperty("print_class_id")
    private String print_class_id;
    @JsonProperty("condiments_groups_id")
    private String condiments_groups_id;
    @JsonProperty("plu_number")
    private String plu_number;
    @JsonProperty("tax")
    private String tax;
    @JsonProperty("is_show_menu")
    private String is_show_menu;
    @JsonProperty("created_at")
    private String created_at;
    @JsonProperty("updated_at")
    private String updated_at;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("id")
    public Integer getId() {
        return id;
    }

    @JsonProperty("id")
    public void setId(Integer id) {
        this.id = id;
    }

    @JsonProperty("name")
    public String getName() {
        return name;
    }

    @JsonProperty("name")
    public void setName(String name) {
        this.name = name;
    }

    @JsonProperty("price")
    public String getPrice() {
        return price;
    }

    @JsonProperty("price")
    public void setPrice(String price) {
        this.price = price;
    }

    @JsonProperty("description")
    public String getDescription() {
        return description;
    }

    @JsonProperty("description")
    public void setDescription(String description) {
        this.description = description;
    }

    @JsonProperty("a_image")
    public String getAImage() {
        return a_image;
    }

    @JsonProperty("a_image")
    public void setAImage(String aImage) {
        this.a_image = aImage;
    }

    @JsonProperty("family_groups_id")
    public String getfamily_groups_id() {
        return family_groups_id;
    }

    @JsonProperty("family_groups_id")
    public void setfamily_groups_id(String family_groups_id) {
        this.family_groups_id = family_groups_id;
    }

    @JsonProperty("print_class_id")
    public String getprint_class_id() {
        return print_class_id;
    }

    @JsonProperty("print_class_id")
    public void setprint_class_id(String print_class_id) {
        this.print_class_id = print_class_id;
    }

    @JsonProperty("condiments_groups_id")
    public String getcondiments_groups_id() {
        return condiments_groups_id;
    }

    @JsonProperty("condiments_groups_id")
    public void setcondiments_groups_id(String condiments_groups_id) {
        this.condiments_groups_id = condiments_groups_id;
    }

    @JsonProperty("plu_number")
    public String getplu_number() {
        return plu_number;
    }

    @JsonProperty("plu_number")
    public void setplu_number(String plu_number) {
        this.plu_number = plu_number;
    }

    @JsonProperty("tax")
    public String getTax() {
        return tax;
    }

    @JsonProperty("tax")
    public void setTax(String tax) {
        this.tax = tax;
    }

    @JsonProperty("is_show_menu")
    public String getis_show_menu() {
        return is_show_menu;
    }

    @JsonProperty("is_show_menu")
    public void setis_show_menu(String is_show_menu) {
        this.is_show_menu = is_show_menu;
    }

    @JsonProperty("created_at")
    public String getcreated_at() {
        return created_at;
    }

    @JsonProperty("created_at")
    public void setcreated_at(String created_at) {
        this.created_at = created_at;
    }

    @JsonProperty("updated_at")
    public String getupdated_at() {
        return updated_at;
    }

    @JsonProperty("updated_at")
    public void setupdated_at(String updated_at) {
        this.updated_at = updated_at;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
