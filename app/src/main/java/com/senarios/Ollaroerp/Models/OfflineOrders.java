package com.senarios.Ollaroerp.Models;

import com.activeandroid.Model;

public class OfflineOrders extends Model {
   public String order;
   public String items;

    public OfflineOrders()
    {

    }

    public String getOrder() {
        return order;
    }

    public void setOrder(String order) {
        this.order = order;
    }

    public String getItems() {
        return items;
    }

    public void setItems(String items) {
        this.items = items;
    }


}
