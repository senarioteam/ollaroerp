package com.senarios.Ollaroerp.Models;

import java.util.ArrayList;

public class CombineBillsTableModel {
    String tableId;
    ArrayList<CombineBillOrderModel> combineBillOrderModels;

    public String getTableId() {
        return tableId;
    }

    public void setTableId(String tableId) {
        this.tableId = tableId;
    }

    public ArrayList<CombineBillOrderModel> getCombineBillOrderModels() {
        return combineBillOrderModels;
    }

    public void setCombineBillOrderModels(ArrayList<CombineBillOrderModel> combineBillOrderModels) {
        this.combineBillOrderModels = combineBillOrderModels;
    }
}
