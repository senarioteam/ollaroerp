package com.senarios.Ollaroerp.Models;

import com.activeandroid.Model;
import com.activeandroid.annotation.Column;
import com.activeandroid.annotation.Table;


@Table(name = "AlchoholicGroup")
public class OfflineAlchoholicGroup extends Model {
    @Column(name = "alco_group_id")
    private Integer alco_group_id;
    @Column(name = "name")
    private String name;
    @Column(name = "menu_items_group_id")
    private String menuItemsGroupId;
    @Column(name = "a_image")
    private String a_image;
    @Column(name = "created_at")
    private String created_at;
    @Column(name = "updated_at")
    private String updated_at;

    public OfflineAlchoholicGroup() {
    }

    public OfflineAlchoholicGroup(AlchoholicGroup alchoholicGroup) {
        setA_image(alchoholicGroup.geta_image());
        setMenuItemsGroupId(alchoholicGroup.getMenuItemsGroupId());
        setAlco_group_id(alchoholicGroup.getId());
        setCreated_at(alchoholicGroup.getcreated_at());
        setUpdated_at(alchoholicGroup.getupdated_at());
        setName(alchoholicGroup.getName());
    }

    public Integer getAlco_group_id() {
        return alco_group_id;
    }

    public void setAlco_group_id(Integer alco_group_id) {
        this.alco_group_id = alco_group_id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getMenuItemsGroupId() {
        return menuItemsGroupId;
    }

    public void setMenuItemsGroupId(String menuItemsGroupId) {
        this.menuItemsGroupId = menuItemsGroupId;
    }

    public String getA_image() {
        return a_image;
    }

    public void setA_image(String a_image) {
        this.a_image = a_image;
    }

    public String getCreated_at() {
        return created_at;
    }

    public void setCreated_at(String created_at) {
        this.created_at = created_at;
    }

    public String getUpdated_at() {
        return updated_at;
    }

    public void setUpdated_at(String updated_at) {
        this.updated_at = updated_at;
    }
}
