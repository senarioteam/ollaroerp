package com.senarios.Ollaroerp.Models;

import com.activeandroid.Model;
import com.activeandroid.annotation.Column;
import com.activeandroid.annotation.Table;

@Table(name = "OfflineBills")
public class OfflineBillModel extends Model {
    @Column(name = "bill_id")
    private String billId;
    @Column(name = "no_of_order")
    private String noOfOrders;
    @Column(name = "comments")
    private String comments;
    @Column(name = "total_payment")
    private String totalPayment;

    public String getBillId() {
        return billId;
    }

    public void setBillId(String billId) {
        this.billId = billId;
    }

    public String getNoOfOrders() {
        return noOfOrders;
    }

    public void setNoOfOrders(String noOfOrders) {
        this.noOfOrders = noOfOrders;
    }

    public String getComments() {
        return comments;
    }

    public void setComments(String comments) {
        this.comments = comments;
    }

    public String getTotalPayment() {
        return totalPayment;
    }

    public void setTotalPayment(String totalPayment) {
        this.totalPayment = totalPayment;
    }

    public OfflineBillModel() {
    }

    public OfflineBillModel(UnpaidBillModel unpaidBillModel) {
        setBillId(Integer.toString(unpaidBillModel.getId()));
        setNoOfOrders(unpaidBillModel.getno_orders());
        setTotalPayment(unpaidBillModel.gettotal_amount());
    }

}
