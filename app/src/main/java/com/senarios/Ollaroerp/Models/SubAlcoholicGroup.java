package com.senarios.Ollaroerp.Models;

import java.util.HashMap;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "id",
        "name",
        "family_groups_id",
        "description",
        "gl_accounts",
        "a_image",
        "created_at",
        "updated_at"
})
public class SubAlcoholicGroup {

    @JsonProperty("id")
    private Integer id;
    @JsonProperty("name")
    private String name;
    @JsonProperty("family_groups_id")
    private String family_groups_id;
    @JsonProperty("description")
    private String description;
    @JsonProperty("gl_accounts")
    private String gl_accounts;
    @JsonProperty("a_image")
    private String a_image;
    @JsonProperty("created_at")
    private String created_at;
    @JsonProperty("updated_at")
    private String updated_at;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("id")
    public Integer getId() {
        return id;
    }

    @JsonProperty("id")
    public void setId(Integer id) {
        this.id = id;
    }

    @JsonProperty("name")
    public String getName() {
        return name;
    }

    @JsonProperty("name")
    public void setName(String name) {
        this.name = name;
    }

    @JsonProperty("family_groups_id")
    public String getFamilyGroupsId() {
        return family_groups_id;
    }

    @JsonProperty("family_groups_id")
    public void setFamilyGroupsId(String family_groups_id) {
        this.family_groups_id = family_groups_id;
    }

    @JsonProperty("description")
    public String getDescription() {
        return description;
    }

    @JsonProperty("description")
    public void setDescription(String description) {
        this.description = description;
    }

    @JsonProperty("gl_accounts")
    public String getGlAccounts() {
        return gl_accounts;
    }

    @JsonProperty("gl_accounts")
    public void setGlAccounts(String glAccounts) {
        this.gl_accounts = glAccounts;
    }

    @JsonProperty("a_image")
    public String getAImage() {
        return a_image;
    }

    @JsonProperty("a_image")
    public void setAImage(String aImage) {
        this.a_image = aImage;
    }

    @JsonProperty("created_at")
    public String getCreatedAt() {
        return created_at;
    }

    @JsonProperty("created_at")
    public void setCreatedAt(String createdAt) {
        this.created_at = createdAt;
    }

    @JsonProperty("updated_at")
    public String getUpdatedAt() {
        return updated_at;
    }

    @JsonProperty("updated_at")
    public void setUpdatedAt(String updatedAt) {
        this.updated_at = updatedAt;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
