package com.senarios.Ollaroerp.Models;

import com.activeandroid.Model;
import com.activeandroid.annotation.Column;
import com.activeandroid.annotation.Table;

@Table(name = "TableModel")
public class OfflineTableModel extends Model {
   @Column(name = "table_id")
    private Integer table_id;
   @Column(name = "name")
    private String name;
   @Column(name = "restaurant")
    private String restaurant;
   @Column(name = "section")
    private String section;
   @Column(name = "capacity")
    private String capacity;
   @Column(name = "booking_status")
    private String booking_status;

   public OfflineTableModel(TableModel tableModel){
       setTable_id(tableModel.getId());
       setBooking_status(tableModel.getbooking_status());
       setCapacity(tableModel.getCapacity());
       setName(tableModel.getName());
       setRestaurant(tableModel.getRestaurant());
       setSection(tableModel.getSection());
       setWaiter(tableModel.getWaiter());
       setWaiter_id(tableModel.getwaiter_id());
       setCreated_at(tableModel.getcreated_at());
       setUpdated_at(tableModel.getupdated_at());
   }

   public OfflineTableModel(){

   }

    public Integer getTable_id() {
        return table_id;
    }

    public void setTable_id(Integer table_id) {
        this.table_id = table_id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getRestaurant() {
        return restaurant;
    }

    public void setRestaurant(String restaurant) {
        this.restaurant = restaurant;
    }

    public String getSection() {
        return section;
    }

    public void setSection(String section) {
        this.section = section;
    }

    public String getCapacity() {
        return capacity;
    }

    public void setCapacity(String capacity) {
        this.capacity = capacity;
    }

    public String getBooking_status() {
        return booking_status;
    }

    public void setBooking_status(String booking_status) {
        this.booking_status = booking_status;
    }

    public String getWaiter_id() {
        return waiter_id;
    }

    public void setWaiter_id(String waiter_id) {
        this.waiter_id = waiter_id;
    }

    public String getWaiter() {
        return waiter;
    }

    public void setWaiter(String waiter) {
        this.waiter = waiter;
    }

    public String getCreated_at() {
        return created_at;
    }

    public void setCreated_at(String created_at) {
        this.created_at = created_at;
    }

    public String getUpdated_at() {
        return updated_at;
    }

    public void setUpdated_at(String updated_at) {
        this.updated_at = updated_at;
    }

    @Column(name = "waiter_id")

    private String waiter_id;
   @Column(name = "waiter")
    private String waiter;
   @Column(name = "created_at")
    private String created_at;
   @Column(name = "updated_at")
    private String updated_at;
}
