package com.senarios.Ollaroerp.Models;

import java.util.HashMap;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "id",
        "name",
        "sub_major_groups_id",
        "timefrom",
        "timeto",
        "description",
        "a_image",
        "is_checked",
        "created_at",
        "updated_at"
})
public class AllMenuItems {

    @JsonProperty("id")
    private Integer id;
    @JsonProperty("name")
    private String name;
    @JsonProperty("sub_major_groups_id")
    private String subMajorGroupsId;
    @JsonProperty("timefrom")
    private String timefrom;
    @JsonProperty("timeto")
    private String timeto;
    @JsonProperty("description")
    private String description;
    @JsonProperty("a_image")
    private String aImage;
    @JsonProperty("is_checked")
    private String isChecked;
    @JsonProperty("created_at")
    private String createdAt;
    @JsonProperty("updated_at")
    private String updatedAt;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("id")
    public Integer getId() {
        return id;
    }

    @JsonProperty("id")
    public void setId(Integer id) {
        this.id = id;
    }

    @JsonProperty("name")
    public String getName() {
        return name;
    }

    @JsonProperty("name")
    public void setName(String name) {
        this.name = name;
    }

    @JsonProperty("sub_major_groups_id")
    public String getSubMajorGroupsId() {
        return subMajorGroupsId;
    }

    @JsonProperty("sub_major_groups_id")
    public void setSubMajorGroupsId(String subMajorGroupsId) {
        this.subMajorGroupsId = subMajorGroupsId;
    }

    @JsonProperty("timefrom")
    public String getTimefrom() {
        return timefrom;
    }

    @JsonProperty("timefrom")
    public void setTimefrom(String timefrom) {
        this.timefrom = timefrom;
    }

    @JsonProperty("timeto")
    public String getTimeto() {
        return timeto;
    }

    @JsonProperty("timeto")
    public void setTimeto(String timeto) {
        this.timeto = timeto;
    }

    @JsonProperty("description")
    public String getDescription() {
        return description;
    }

    @JsonProperty("description")
    public void setDescription(String description) {
        this.description = description;
    }

    @JsonProperty("a_image")
    public String getAImage() {
        return aImage;
    }

    @JsonProperty("a_image")
    public void setAImage(String aImage) {
        this.aImage = aImage;
    }

    @JsonProperty("is_checked")
    public String getIsChecked() {
        return isChecked;
    }

    @JsonProperty("is_checked")
    public void setIsChecked(String isChecked) {
        this.isChecked = isChecked;
    }

    @JsonProperty("created_at")
    public String getCreatedAt() {
        return createdAt;
    }

    @JsonProperty("created_at")
    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    @JsonProperty("updated_at")
    public String getUpdatedAt() {
        return updatedAt;
    }

    @JsonProperty("updated_at")
    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}