package com.senarios.Ollaroerp.Models;

import com.activeandroid.annotation.Table;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;

@Table(name = "PaidOrder")
public class PaidOrder {


    @JsonProperty("id")
    private Integer id;
    @JsonProperty("bill_id")
    private String bill_id;
    @JsonProperty("date_time")
    private String date_time;
    @JsonProperty("table_no")
    private String table_no;
    @JsonProperty("section")
    private String section;
    @JsonProperty("no_of_guests")
    private String no_of_guests;
    @JsonProperty("item_description")
    private String item_description;
    @JsonProperty("condiments")
    private Object condiments;
    @JsonProperty("waiter_id")
    private String waiter_id;
    @JsonProperty("waiter")
    private String waiter;
    @JsonProperty("restaurant")
    private String restaurant;
    @JsonProperty("print_class_id")
    private Object print_class_id;
    @JsonProperty("total_amount")
    private String total_amount;
    @JsonProperty("status")
    private String status;
    @JsonProperty("cooking_status")
    private String cooking_status;
    @JsonProperty("created_at")
    private String createdAt;
    @JsonProperty("updated_at")
    private String updatedAt;
    @JsonProperty("auto_invoice")
    private String auto_invoice;
    @JsonProperty("auto_docket")
    private String auto_docket;
    @JsonProperty("discount_percent")
    private String discount_percent;
    @JsonProperty("discount_reason")
    private Object discount_reason;
    @JsonProperty("discountType")
    private Object discountType;
    @JsonProperty("discount_amount")
    private String discount_amount;
    @JsonProperty("discounted_amount")
    private String discounted_amount;
    @JsonProperty("order_items")
    private List<OrderItem> order_items = null;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getBill_id() {
        return bill_id;
    }

    public void setBill_id(String bill_id) {
        this.bill_id = bill_id;
    }

    public String getDate_time() {
        return date_time;
    }

    public void setDate_time(String date_time) {
        this.date_time = date_time;
    }

    public String getTable_no() {
        return table_no;
    }

    public void setTable_no(String table_no) {
        this.table_no = table_no;
    }

    public String getSection() {
        return section;
    }

    public void setSection(String section) {
        this.section = section;
    }

    public String getNo_of_guests() {
        return no_of_guests;
    }

    public void setNo_of_guests(String no_of_guests) {
        this.no_of_guests = no_of_guests;
    }

    public String getItem_description() {
        return item_description;
    }

    public void setItem_description(String item_description) {
        this.item_description = item_description;
    }

    public Object getCondiments() {
        return condiments;
    }

    public void setCondiments(Object condiments) {
        this.condiments = condiments;
    }

    public String getWaiter_id() {
        return waiter_id;
    }

    public void setWaiter_id(String waiter_id) {
        this.waiter_id = waiter_id;
    }

    public String getWaiter() {
        return waiter;
    }

    public void setWaiter(String waiter) {
        this.waiter = waiter;
    }

    public String getRestaurant() {
        return restaurant;
    }

    public void setRestaurant(String restaurant) {
        this.restaurant = restaurant;
    }

    public Object getPrint_class_id() {
        return print_class_id;
    }

    public void setPrint_class_id(Object print_class_id) {
        this.print_class_id = print_class_id;
    }

    public String getTotal_amount() {
        return total_amount;
    }

    public void setTotal_amount(String total_amount) {
        this.total_amount = total_amount;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getCooking_status() {
        return cooking_status;
    }

    public void setCooking_status(String cooking_status) {
        this.cooking_status = cooking_status;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }

    public String getAuto_invoice() {
        return auto_invoice;
    }

    public void setAuto_invoice(String auto_invoice) {
        this.auto_invoice = auto_invoice;
    }

    public String getAuto_docket() {
        return auto_docket;
    }

    public void setAuto_docket(String auto_docket) {
        this.auto_docket = auto_docket;
    }

    public String getDiscount_percent() {
        return discount_percent;
    }

    public void setDiscount_percent(String discount_percent) {
        this.discount_percent = discount_percent;
    }

    public Object getDiscount_reason() {
        return discount_reason;
    }

    public void setDiscount_reason(Object discount_reason) {
        this.discount_reason = discount_reason;
    }

    public Object getDiscountType() {
        return discountType;
    }

    public void setDiscountType(Object discountType) {
        this.discountType = discountType;
    }

    public String getDiscount_amount() {
        return discount_amount;
    }

    public void setDiscount_amount(String discount_amount) {
        this.discount_amount = discount_amount;
    }

    public String getDiscounted_amount() {
        return discounted_amount;
    }

    public void setDiscounted_amount(String discounted_amount) {
        this.discounted_amount = discounted_amount;
    }

    public List<OrderItem> getOrder_items() {
        return order_items;
    }

    public void setOrder_items(List<OrderItem> order_items) {
        this.order_items = order_items;
    }
}
