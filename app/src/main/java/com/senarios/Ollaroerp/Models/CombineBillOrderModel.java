package com.senarios.Ollaroerp.Models;

import java.util.ArrayList;

public class CombineBillOrderModel {
    String orderId;
    String total;
    ArrayList<CombineBillItemModel> combineBillItemModels;

    public String getOrderId() {
        return orderId;
    }

    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }

    public String getTotal() {
        return total;
    }

    public void setTotal(String total) {
        this.total = total;
    }

    public ArrayList<CombineBillItemModel> getCombineBillItemModels() {
        return combineBillItemModels;
    }

    public void setCombineBillItemModels(ArrayList<CombineBillItemModel> combineBillItemModels) {
        this.combineBillItemModels = combineBillItemModels;
    }
}
