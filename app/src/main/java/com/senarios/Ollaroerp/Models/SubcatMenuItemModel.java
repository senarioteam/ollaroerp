package com.senarios.Ollaroerp.Models;

public class SubcatMenuItemModel {

    String itemName;
    String itemFranchise;
    String itemPrice;
    String itemImage;

    public String getItemName() {
        return itemName;
    }

    public void setItemName(String itemName) {
        this.itemName = itemName;
    }

    public String getItemFranchise() {
        return itemFranchise;
    }

    public void setItemFranchise(String itemFranchise) {
        this.itemFranchise = itemFranchise;
    }

    public String getItemPrice() {
        return itemPrice;
    }

    public void setItemPrice(String itemPrice) {
        this.itemPrice = itemPrice;
    }

    public String getItemImage() {
        return itemImage;
    }

    public void setItemImage(String itemImage) {
        this.itemImage = itemImage;
    }

}
