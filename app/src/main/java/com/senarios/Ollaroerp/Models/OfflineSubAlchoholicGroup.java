package com.senarios.Ollaroerp.Models;

import com.activeandroid.Model;
import com.activeandroid.annotation.Column;
import com.activeandroid.annotation.Table;

@Table(name = "SubAlchoholicGroup")
public class OfflineSubAlchoholicGroup extends Model {
    public Integer getSub_alco_id() {
        return sub_alco_id;
    }

    public void setSub_alco_id(Integer sub_alco_id) {
        this.sub_alco_id = sub_alco_id;
    }

    @Column(name = "sub_alco_id")
    private Integer sub_alco_id;
    @Column(name = "name")
    private String name;
    @Column(name = "family_groups_id")
    private String familyGroupsId;
    @Column(name = "description")
    private String description;
    @Column(name = "gl_accounts")
    private String gl_accounts;
    @Column(name = "a_image")
    private String a_image;
    @Column(name = "created_at")
    private String created_at;
    @Column(name = "updated_at")
    private String updated_at;

   public OfflineSubAlchoholicGroup(){

   }

    public OfflineSubAlchoholicGroup(SubAlcoholicGroup subAlcoholicGroup){

        setA_image(subAlcoholicGroup .getAImage());
        setGl_accounts(subAlcoholicGroup .getGlAccounts());
        setDescription(subAlcoholicGroup .getDescription());
        setFamilyGroupsId(subAlcoholicGroup.getFamilyGroupsId());
        setSub_alco_id(subAlcoholicGroup.getId());
        setCreated_at(subAlcoholicGroup .getCreatedAt());
        setUpdated_at(subAlcoholicGroup .getUpdatedAt());
        setName(subAlcoholicGroup .getName());

    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getFamilyGroupsId() {
        return familyGroupsId;
    }

    public void setFamilyGroupsId(String familyGroupsId) {
        this.familyGroupsId = familyGroupsId;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getGl_accounts() {
        return gl_accounts;
    }

    public void setGl_accounts(String gl_accounts) {
        this.gl_accounts = gl_accounts;
    }

    public String getA_image() {
        return a_image;
    }

    public void setA_image(String a_image) {
        this.a_image = a_image;
    }

    public String getCreated_at() {
        return created_at;
    }

    public void setCreated_at(String created_at) {
        this.created_at = created_at;
    }

    public String getUpdated_at() {
        return updated_at;
    }

    public void setUpdated_at(String updated_at) {
        this.updated_at = updated_at;
    }
}
