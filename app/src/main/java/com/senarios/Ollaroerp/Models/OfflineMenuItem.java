package com.senarios.Ollaroerp.Models;

import com.activeandroid.Model;
import com.activeandroid.annotation.Column;
import com.activeandroid.annotation.Table;

@Table(name = "MenuItem") 
public class OfflineMenuItem extends Model {

    @Column(name = "menu_id")
    private Integer menu_id;

    @Column(name = "name")
    private String name;
    @Column(name = "sub_major_groups_id")
    private String sub_major_groups_id;
    @Column(name = "timefrom")
    private String timefrom;
    @Column(name = "timeto")
    private String timeto;
    @Column(name = "description")
    private String description;
    @Column(name = "a_image")
    private String a_image;
    @Column(name = "is_checked")
    private String is_checked;
    @Column(name = "created_at")
    private String created_at;

    public OfflineMenuItem() {
    }

    public Integer getMenu_id() {
        return menu_id;
    }

    public void setMenu_id(Integer menu_id) {
        this.menu_id = menu_id;
    }

    public OfflineMenuItem(MenuItem menuItem) {

        setMenu_id(menuItem.getId());
        setA_image(menuItem.geta_image());
        setCreated_at(menuItem.getcreated_at());
        setUpdated_at(menuItem.getupdated_at());
        setDescription(menuItem.getDescription());
        setIs_checked(menuItem.getIs_checked());
        setName(menuItem.getName());
        setTimeto(menuItem.getTimeto());
        setTimefrom(menuItem.getTimefrom());
        setSub_major_groups_id(menuItem.getsub_major_groups_id());

    }



    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSub_major_groups_id() {
        return sub_major_groups_id;
    }

    public void setSub_major_groups_id(String sub_major_groups_id) {
        this.sub_major_groups_id = sub_major_groups_id;
    }

    public String getTimefrom() {
        return timefrom;
    }

    public void setTimefrom(String timefrom) {
        this.timefrom = timefrom;
    }

    public String getTimeto() {
        return timeto;
    }

    public void setTimeto(String timeto) {
        this.timeto = timeto;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getA_image() {
        return a_image;
    }

    public void setA_image(String a_image) {
        this.a_image = a_image;
    }

    public String getIs_checked() {
        return is_checked;
    }

    public void setIs_checked(String is_checked) {
        this.is_checked = is_checked;
    }

    public String getCreated_at() {
        return created_at;
    }

    public void setCreated_at(String created_at) {
        this.created_at = created_at;
    }

    public String getUpdated_at() {
        return updated_at;
    }

    public void setUpdated_at(String updated_at) {
        this.updated_at = updated_at;
    }

    @Column(name = "updated_at")

    private String updated_at;
}