package com.senarios.Ollaroerp.Models;

import com.activeandroid.Model;
import com.activeandroid.annotation.Column;
import com.activeandroid.annotation.Table;

@Table(name = "CartItems")
public class CartItemModel extends Model {

    @Column(name = "itemName")
    String itemName;

    @Column(name = "desc")
    String desc;

    @Column(name = "price")
    String price;

    @Column(name = "image")
    String image;


    @Column(name = "printClassId")
    String printClassId;

    @Column(name = "general_item_id")
    String general_item_id;

    @Column(name = "comments")
    String Comments;

    public String getComments() {
        return Comments;
    }

    public void setComments(String comments) {
        Comments = comments;
    }

    public String getGeneral_item_id() {
        return general_item_id;
    }

    public void setGeneral_item_id(String general_item_id) {
        this.general_item_id = general_item_id;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    @Column(name = "quantity")
    String quantity;

    public CartItemModel() {
    }


    public CartItemModel(String itemName, String price, String quantity, String desc, String printClassId) {
        this.itemName = itemName;
        this.image = price;
        this.printClassId = printClassId;
        this.desc = desc;
        this.quantity = quantity;
    }

    public String getItemName() {
        return itemName;
    }

    public void setItemName(String itemName) {
        this.itemName = itemName;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getQuantity() {
        return quantity;
    }

    public void setQuantity(String quantity) {
        this.quantity = quantity;
    }

    public String getPrintClassId() {
        return printClassId;
    }

    public void setPrintClassId(String printClassId) {
        this.printClassId = printClassId;
    }
}
