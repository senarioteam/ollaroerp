package com.senarios.Ollaroerp.Models;

import java.util.HashMap;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "id",
        "name",
        "menu_items_group_id",
        "gl_account",
        "a_image",
        "created_at",
        "updated_at"
})
public class FamilyGroup {

    @JsonProperty("id")
    private Integer id;
    @JsonProperty("name")
    private String name;
    @JsonProperty("menu_item_groups_id")
    private String menu_item_groups_id;
    @JsonProperty("gl_account")
    private String gl_account;
    @JsonProperty("a_image")
    private String a_image;
    @JsonProperty("created_at")
    private String created_at;
    @JsonProperty("updated_at")
    private String updated_a;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("id")
    public Integer getId() {
        return id;
    }

    @JsonProperty("id")
    public void setId(Integer id) {
        this.id = id;
    }

    @JsonProperty("name")
    public String getName() {
        return name;
    }

    @JsonProperty("name")
    public void setName(String name) {
        this.name = name;
    }

    @JsonProperty("menu_item_groups_id")
    public String getmenu_item_groups_id() {
        return menu_item_groups_id;
    }

    @JsonProperty("menu_item_groups_id")
    public void setmenu_items_group_id(String menu_item_groups_id) {
        this.menu_item_groups_id = menu_item_groups_id;
    }

    @JsonProperty("gl_account")
    public String getgl_account() {
        return gl_account;
    }

    @JsonProperty("gl_account")
    public void setgl_account(String gl_account) {
        this.gl_account = gl_account;
    }

    @JsonProperty("a_image")
    public String geta_image() {
        return a_image;
    }

    @JsonProperty("a_image")
    public void seta_image(String a_image) {
        this.a_image = a_image;
    }

    @JsonProperty("created_at")
    public String getcreated_at() {
        return created_at;
    }

    @JsonProperty("created_at")
    public void setcreated_at(String created_at) {
        this.created_at = created_at;
    }

    @JsonProperty("updated_at")
    public String getupdated_a() {
        return updated_a;
    }

    @JsonProperty("updated_at")
    public void setupdated_a(String updated_a) {
        this.updated_a = updated_a;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
