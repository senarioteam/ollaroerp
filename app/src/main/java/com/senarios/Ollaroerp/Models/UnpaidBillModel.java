package com.senarios.Ollaroerp.Models;

import java.util.HashMap;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "id",
        "date",
        "waiter_id",
        "waiter_name",
        "no_orders",
        "orders",
        "total_amount",
        "status",
        "created_at",
        "updated_at",
        "discount_percent",
        "discount_reason",
        "discounted_amount"
})
public class UnpaidBillModel {

    @JsonProperty("id")
    private Integer id;
    @JsonProperty("date")
    private String date;
    @JsonProperty("waiter_id")
    private String waiter_id;
    @JsonProperty("waiter_name")
    private String waiter_name;
    @JsonProperty("no_orders")
    private String no_orders;
    @JsonProperty("orders")
    private String orders;
    @JsonProperty("total_amount")
    private String total_amount;
    @JsonProperty("status")
    private String status;
    @JsonProperty("created_at")
    private String created_at;
    @JsonProperty("updated_at")
    private String updated_at;
    @JsonProperty("discount_percent")
    private String discount_percent;
    @JsonProperty("discount_reason")
    private Object discount_reason;
    @JsonProperty("discounted_amount")
    private String discounted_amount;//
    @JsonProperty("comments")
    private String comments;
    @JsonProperty("table_no")
    private String table_no;

    public String getComments() {
        return comments;
    }

    public void setComments(String comments) {
        this.comments = comments;
    }

    public String getTable_no() {
        return table_no;
    }

    public void setTable_no(String table_no) {
        this.table_no = table_no;
    }

    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("id")
    public Integer getId() {
        return id;
    }

    @JsonProperty("id")
    public void setId(Integer id) {
        this.id = id;
    }

    @JsonProperty("date")
    public String getDate() {
        return date;
    }

    @JsonProperty("date")
    public void setDate(String date) {
        this.date = date;
    }

    @JsonProperty("waiter_id")
    public String getwaiter_id() {
        return waiter_id;
    }

    @JsonProperty("waiter_id")
    public void setwaiter_id(String waiter_id) {
        this.waiter_id = waiter_id;
    }

    @JsonProperty("waiter_name")
    public String getwaiter_name() {
        return waiter_name;
    }

    @JsonProperty("waiter_name")
    public void setwaiter_name(String waiter_name) {
        this.waiter_name = waiter_name;
    }

    @JsonProperty("no_orders")
    public String getno_orders() {
        return no_orders;
    }

    @JsonProperty("no_orders")
    public void setno_orders(String no_orders) {
        this.no_orders = no_orders;
    }

    @JsonProperty("orders")
    public String getOrders() {
        return orders;
    }

    @JsonProperty("orders")
    public void setOrders(String orders) {
        this.orders = orders;
    }

    @JsonProperty("total_amount")
    public String gettotal_amount() {
        return total_amount;
    }

    @JsonProperty("total_amount")
    public void settotal_amount(String total_amount) {
        this.total_amount = total_amount;
    }

    @JsonProperty("status")
    public String getStatus() {
        return status;
    }

    @JsonProperty("status")
    public void setStatus(String status) {
        this.status = status;
    }

    @JsonProperty("created_at")
    public String getcreated_at() {
        return created_at;
    }

    @JsonProperty("created_at")
    public void setcreated_at(String created_at) {
        this.created_at = created_at;
    }

    @JsonProperty("updated_at")
    public String getupdated_at() {
        return updated_at;
    }

    @JsonProperty("updated_at")
    public void setupdated_at(String updated_at) {
        this.updated_at = updated_at;
    }

    @JsonProperty("discount_percent")
    public String getdiscount_percent() {
        return discount_percent;
    }

    @JsonProperty("discount_percent")
    public void setdiscount_percent(String discount_percent) {
        this.discount_percent = discount_percent;
    }

    @JsonProperty("discount_reason")
    public Object getdiscount_reason() {
        return discount_reason;
    }

    @JsonProperty("discount_reason")
    public void setdiscount_reason(Object discount_reason) {
        this.discount_reason = discount_reason;
    }

    @JsonProperty("discounted_amount")
    public String getdiscounted_amount() {
        return discounted_amount;
    }

    @JsonProperty("discounted_amount")
    public void setdiscounted_amount(String discounted_amount) {
        this.discounted_amount = discounted_amount;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}