package com.senarios.Ollaroerp.Models;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;

public class PaidOrderDummy {


    @JsonProperty("id")
    private Integer id;
    @JsonProperty("bill_id")
    private String billId;
    @JsonProperty("date_time")
    private String dateTime;
    @JsonProperty("table_no")
    private String tableNo;
    @JsonProperty("section")
    private String section;
    @JsonProperty("no_of_guests")
    private String noOfGuests;
    @JsonProperty("item_description")
    private Object itemDescription;
    @JsonProperty("condiments")
    private Object condiments;
    @JsonProperty("waiter_id")
    private String waiterId;
    @JsonProperty("waiter")
    private String waiter;
    @JsonProperty("restaurant")
    private String restaurant;
    @JsonProperty("print_class_id")
    private Object printClassId;
    @JsonProperty("total_amount")
    private String totalAmount;
    @JsonProperty("status")
    private String status;
    @JsonProperty("cooking_status")
    private String cookingStatus;
    @JsonProperty("created_at")
    private String createdAt;
    @JsonProperty("updated_at")
    private String updatedAt;
    @JsonProperty("auto_invoice")
    private String autoInvoice;
    @JsonProperty("auto_docket")
    private String autoDocket;
    @JsonProperty("discount_percent")
    private String discountPercent;
    @JsonProperty("discount_reason")
    private Object discountReason;
    @JsonProperty("discountType")
    private Object discountType;
    @JsonProperty("discount_amount")
    private String discountAmount;
    @JsonProperty("discounted_amount")
    private String discountedAmount;
    @JsonProperty("order_items")
    private List<OrderItem> orderItems = null;

    @JsonProperty("id")
    public Integer getId() {
        return id;
    }

    @JsonProperty("id")
    public void setId(Integer id) {
        this.id = id;
    }

    @JsonProperty("bill_id")
    public String getBillId() {
        return billId;
    }

    @JsonProperty("bill_id")
    public void setBillId(String billId) {
        this.billId = billId;
    }

    @JsonProperty("date_time")
    public String getDateTime() {
        return dateTime;
    }

    @JsonProperty("date_time")
    public void setDateTime(String dateTime) {
        this.dateTime = dateTime;
    }

    @JsonProperty("table_no")
    public String getTableNo() {
        return tableNo;
    }

    @JsonProperty("table_no")
    public void setTableNo(String tableNo) {
        this.tableNo = tableNo;
    }

    @JsonProperty("section")
    public String getSection() {
        return section;
    }

    @JsonProperty("section")
    public void setSection(String section) {
        this.section = section;
    }

    @JsonProperty("no_of_guests")
    public String getNoOfGuests() {
        return noOfGuests;
    }

    @JsonProperty("no_of_guests")
    public void setNoOfGuests(String noOfGuests) {
        this.noOfGuests = noOfGuests;
    }

    @JsonProperty("item_description")
    public Object getItemDescription() {
        return itemDescription;
    }

    @JsonProperty("item_description")
    public void setItemDescription(Object itemDescription) {
        this.itemDescription = itemDescription;
    }

    @JsonProperty("condiments")
    public Object getCondiments() {
        return condiments;
    }

    @JsonProperty("condiments")
    public void setCondiments(Object condiments) {
        this.condiments = condiments;
    }

    @JsonProperty("waiter_id")
    public String getWaiterId() {
        return waiterId;
    }

    @JsonProperty("waiter_id")
    public void setWaiterId(String waiterId) {
        this.waiterId = waiterId;
    }

    @JsonProperty("waiter")
    public String getWaiter() {
        return waiter;
    }

    @JsonProperty("waiter")
    public void setWaiter(String waiter) {
        this.waiter = waiter;
    }

    @JsonProperty("restaurant")
    public String getRestaurant() {
        return restaurant;
    }

    @JsonProperty("restaurant")
    public void setRestaurant(String restaurant) {
        this.restaurant = restaurant;
    }

    @JsonProperty("print_class_id")
    public Object getPrintClassId() {
        return printClassId;
    }

    @JsonProperty("print_class_id")
    public void setPrintClassId(Object printClassId) {
        this.printClassId = printClassId;
    }

    @JsonProperty("total_amount")
    public String getTotalAmount() {
        return totalAmount;
    }

    @JsonProperty("total_amount")
    public void setTotalAmount(String totalAmount) {
        this.totalAmount = totalAmount;
    }

    @JsonProperty("status")
    public String getStatus() {
        return status;
    }

    @JsonProperty("status")
    public void setStatus(String status) {
        this.status = status;
    }

    @JsonProperty("cooking_status")
    public String getCookingStatus() {
        return cookingStatus;
    }

    @JsonProperty("cooking_status")
    public void setCookingStatus(String cookingStatus) {
        this.cookingStatus = cookingStatus;
    }

    @JsonProperty("created_at")
    public String getCreatedAt() {
        return createdAt;
    }

    @JsonProperty("created_at")
    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    @JsonProperty("updated_at")
    public String getUpdatedAt() {
        return updatedAt;
    }

    @JsonProperty("updated_at")
    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }

    @JsonProperty("auto_invoice")
    public String getAutoInvoice() {
        return autoInvoice;
    }

    @JsonProperty("auto_invoice")
    public void setAutoInvoice(String autoInvoice) {
        this.autoInvoice = autoInvoice;
    }

    @JsonProperty("auto_docket")
    public String getAutoDocket() {
        return autoDocket;
    }

    @JsonProperty("auto_docket")
    public void setAutoDocket(String autoDocket) {
        this.autoDocket = autoDocket;
    }

    @JsonProperty("discount_percent")
    public String getDiscountPercent() {
        return discountPercent;
    }

    @JsonProperty("discount_percent")
    public void setDiscountPercent(String discountPercent) {
        this.discountPercent = discountPercent;
    }

    @JsonProperty("discount_reason")
    public Object getDiscountReason() {
        return discountReason;
    }

    @JsonProperty("discount_reason")
    public void setDiscountReason(Object discountReason) {
        this.discountReason = discountReason;
    }

    @JsonProperty("discountType")
    public Object getDiscountType() {
        return discountType;
    }

    @JsonProperty("discountType")
    public void setDiscountType(Object discountType) {
        this.discountType = discountType;
    }

    @JsonProperty("discount_amount")
    public String getDiscountAmount() {
        return discountAmount;
    }

    @JsonProperty("discount_amount")
    public void setDiscountAmount(String discountAmount) {
        this.discountAmount = discountAmount;
    }

    @JsonProperty("discounted_amount")
    public String getDiscountedAmount() {
        return discountedAmount;
    }

    @JsonProperty("discounted_amount")
    public void setDiscountedAmount(String discountedAmount) {
        this.discountedAmount = discountedAmount;
    }

    @JsonProperty("order_items")
    public List<OrderItem> getOrderItems() {
        return orderItems;
    }

    @JsonProperty("order_items")
    public void setOrderItems(List<OrderItem> orderItems) {
        this.orderItems = orderItems;
    }
}
