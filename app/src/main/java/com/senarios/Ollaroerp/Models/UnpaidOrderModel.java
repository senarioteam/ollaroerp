package com.senarios.Ollaroerp.Models;

import java.util.ArrayList;

public class UnpaidOrderModel {
    String orderId;
    String tableId;
    String total;
    ArrayList<UnpaidItemModel> unpaidItemModels;

    public String getOrderId() {
        return orderId;
    }

    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }

    public String getTableId() {
        return tableId;
    }

    public void setTableId(String tableId) {
        this.tableId = tableId;
    }

    public String getTotal() {
        return total;
    }

    public void setTotal(String total) {
        this.total = total;
    }

    public ArrayList<UnpaidItemModel> getUnpaidItemModels() {
        return unpaidItemModels;
    }

    public void setUnpaidItemModels(ArrayList<UnpaidItemModel> unpaidItemModels) {
        this.unpaidItemModels = unpaidItemModels;
    }
}
