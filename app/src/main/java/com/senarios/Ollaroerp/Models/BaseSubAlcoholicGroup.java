package com.senarios.Ollaroerp.Models;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import com.google.gson.annotations.SerializedName;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "subAlcoholicGroups"
})
public class BaseSubAlcoholicGroup {

    @SerializedName("subAlcoholicGroups")
    private List<SubAlcoholicGroup> subAlcoholicGroups = null;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("subAlcoholicGroups")
    public List<SubAlcoholicGroup> getSubAlcoholicGroups() {
        return subAlcoholicGroups;
    }

    @JsonProperty("subAlcoholicGroups")
    public void setSubAlcoholicGroups(List<SubAlcoholicGroup> subAlcoholicGroups) {
        this.subAlcoholicGroups = subAlcoholicGroups;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
