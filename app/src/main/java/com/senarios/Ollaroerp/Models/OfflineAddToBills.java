package com.senarios.Ollaroerp.Models;

public class OfflineAddToBills {
    String billd;
    String order;
    String items;

    public String getBilld() {
        return billd;
    }

    public void setBilld(String billd) {
        this.billd = billd;
    }

    public String getOrder() {
        return order;
    }

    public void setOrder(String order) {
        this.order = order;
    }

    public String getItems() {
        return items;
    }

    public void setItems(String items) {
        this.items = items;
    }

}
