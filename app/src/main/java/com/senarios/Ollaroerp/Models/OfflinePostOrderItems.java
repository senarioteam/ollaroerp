package com.senarios.Ollaroerp.Models;

import com.activeandroid.Model;
import com.activeandroid.annotation.Column;
import com.activeandroid.annotation.Table;

@Table(name = "OfflinePaidOrders")
public class OfflinePostOrderItems extends Model {
    @Column(name = "Postorderid")
            private Integer Postorderid;
    @Column(name = "bill_id")
    private String bill_id;
    @Column(name = "date_time")
    private String date_time;
    @Column(name = "table_no")
    private String table_no;
    @Column(name = "no_of_guests")
    private String no_of_guests;
    @Column(name = "item_description")
    private String item_description;
    private Object condiments;
    @Column(name = "waiter_id")
    private String waiter_id;
    @Column(name = "waiter")
    private String waiter;
    @Column(name = "restaurant")
    private String restaurant;
    @Column(name = "total_amount")
    private String total_amount;
    @Column(name = "status")
    private String status;
    @Column(name = "created_at")
    private String created_at;
    @Column(name = "updated_at")
    private String updated_at;

    @Column(name = "void_request")
    private String void_request;

    public String getVoid_request() {
        return void_request;
    }

    public void setVoid_request(String void_request) {
        this.void_request = void_request;
    }



    public Integer getPostorderid() {
        return Postorderid;
    }

    public void setPostorderid(Integer postorderid) {
        Postorderid = postorderid;
    }

    public String getBill_id() {
        return bill_id;
    }

    public void setBill_id(String bill_id) {
        this.bill_id = bill_id;
    }

    public String getDate_time() {
        return date_time;
    }

    public void setDate_time(String date_time) {
        this.date_time = date_time;
    }

    public String getTable_no() {
        return table_no;
    }

    public void setTable_no(String table_no) {
        this.table_no = table_no;
    }

    public String getNo_of_guests() {
        return no_of_guests;
    }

    public void setNo_of_guests(String no_of_guests) {
        this.no_of_guests = no_of_guests;
    }

    public String getItem_description() {
        return item_description;
    }

    public void setItem_description(String item_description) {
        this.item_description = item_description;
    }

    public Object getCondiments() {
        return condiments;
    }

    public void setCondiments(Object condiments) {
        this.condiments = condiments;
    }

    public String getWaiter_id() {
        return waiter_id;
    }

    public void setWaiter_id(String waiter_id) {
        this.waiter_id = waiter_id;
    }

    public String getWaiter() {
        return waiter;
    }

    public void setWaiter(String waiter) {
        this.waiter = waiter;
    }

    public String getRestaurant() {
        return restaurant;
    }

    public void setRestaurant(String restaurant) {
        this.restaurant = restaurant;
    }

    public String getTotal_amount() {
        return total_amount;
    }

    public void setTotal_amount(String total_amount) {
        this.total_amount = total_amount;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getCreated_at() {
        return created_at;
    }

    public void setCreated_at(String created_at) {
        this.created_at = created_at;
    }

    public String getUpdated_at() {
        return updated_at;
    }

    public void setUpdated_at(String updated_at) {
        this.updated_at = updated_at;
    }



    public OfflinePostOrderItems(PaidOrder paidOrder) {
        setPostorderid(paidOrder.getId());
        setBill_id(paidOrder.getBill_id());
        setNo_of_guests(paidOrder.getNo_of_guests());
        setTable_no(paidOrder.getTable_no());
        setStatus(paidOrder.getStatus());
        setRestaurant(paidOrder.getRestaurant());
        setCondiments(paidOrder.getCondiments());
        setTotal_amount(paidOrder.getTotal_amount());
        setDate_time(paidOrder.getDate_time());
        setWaiter(paidOrder.getWaiter());
        setWaiter_id(paidOrder.getWaiter_id());




    }

    public OfflinePostOrderItems() {
    }
}
