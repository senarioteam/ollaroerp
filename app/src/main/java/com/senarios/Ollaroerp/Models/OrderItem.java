package com.senarios.Ollaroerp.Models;

import java.util.HashMap;
import java.util.Map;

import com.activeandroid.annotation.Column;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "id",
        "post_paid_orders_id",
        "name",
        "quantity",
        "created_at",
        "updated_at",
        "void_request",
        "general_item_id"
})
public class OrderItem {

    private boolean isChecked=false;

    public boolean isChecked() {
        return isChecked;
    }

    public void setChecked(boolean checked) {
        isChecked = checked;
    }

    public Boolean getChecked() {
        return isChecked;
    }
    private boolean isVoid;

    public boolean isVoid() {
        return isVoid;
    }

    public void setVoid(boolean aVoid) {
        isVoid = aVoid;
    }

    @JsonProperty("id")
    private Integer id;
    @JsonProperty("post_paid_orders_id")
    private String post_paid_orders_id;
    @JsonProperty("name")
    private String name;
    @JsonProperty("quantity")
    private Integer quantity=0;
    @JsonProperty("price")
    private String price;
    @JsonProperty("image")
    private String image;
    @JsonProperty("created_at")
    private String created_at;
    @JsonProperty("updated_at")
    private String updated_at;
    @JsonProperty("status")
    private String status;
    @JsonProperty("general_item_id")
    private String general_item_id;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @Column(name = "void_request")
    private String void_request;

    public String getVoid_request() {
        return void_request;
    }

    public void setVoid_request(String void_request) {
        this.void_request = void_request;
    }

    @JsonProperty("status")
    public String getStatus() {
        return status;
    }

    @JsonProperty("status")
    public void setStatus(String status) {
        this.status = status;
    }
    @JsonProperty("image")
    public String getImage() {
        return image;
    }

    @JsonProperty("id")
    public void setImage(String id) {
        this.image = image;
    }

    @JsonProperty("price")
    public String getPrice() {
        return price;
    }

    @JsonProperty("id")
    public void setPrice(String price) {
        this.price = price;
    }

    @JsonProperty("id")
    public Integer getId() {
        return id;
    }

    @JsonProperty("id")
    public void setId(Integer id) {
        this.id = id;
    }

    @JsonProperty("post_paid_orders_id")
    public String getpost_paid_orders_id() {
        return post_paid_orders_id;
    }

    @JsonProperty("post_paid_orders_id")
    public void setpost_paid_orders_id(String post_paid_orders_id) {
        this.post_paid_orders_id = post_paid_orders_id;
    }

    @JsonProperty("name")
    public String getName() {
        return name;
    }

    @JsonProperty("name")
    public void setName(String name) {
        this.name = name;
    }

    @JsonProperty("quantity")
    public Integer getQuantity() {
        return quantity;
    }

    @JsonProperty("quantity")
    public void setQuantity(Integer quantity) {
        this.quantity = quantity;
    }

    @JsonProperty("general_item_id")
    public void setGeneral_item_id(String general_item_id){this.general_item_id=general_item_id;}

    @JsonProperty("general_item_id")
    public String getGeneral_item_id(){return general_item_id;}

    @JsonProperty("created_at")
    public String getcreated_at() {
        return created_at;
    }

    @JsonProperty("created_at")
    public void setcreated_at(String created_at) {
        this.created_at = created_at;
    }

    @JsonProperty("updated_at")
    public String getupdated_at() {
        return updated_at;
    }

    @JsonProperty("updated_at")
    public void setupdated_at(String updated_at) {
        this.updated_at = updated_at;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}