package com.senarios.Ollaroerp.Models;

import java.util.HashMap;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "id",
        "name",
        "price",
        "description",
        "a_image",
        "family_groups_id",
        "print_class_id",
        "condiments_groups_id",
        "plu_number",
        "tax",
        "is_show_menu",
        "created_at",
        "updated_at"
})
public class AllMenuItemsOffline {

    @JsonProperty("id")
    private Integer id;
    @JsonProperty("name")
    private String name;
    @JsonProperty("price")
    private String price;
    @JsonProperty("description")
    private String description;
    @JsonProperty("a_image")
    private String aImage;
    @JsonProperty("family_groups_id")
    private String familyGroupsId;
    @JsonProperty("print_class_id")
    private String printClassId;
    @JsonProperty("condiments_groups_id")
    private String condimentsGroupsId;
    @JsonProperty("plu_number")
    private String pluNumber;
    @JsonProperty("tax")
    private String tax;
    @JsonProperty("is_show_menu")
    private String isShowMenu;
    @JsonProperty("created_at")
    private String createdAt;
    @JsonProperty("updated_at")
    private String updatedAt;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("id")
    public Integer getId() {
        return id;
    }

    @JsonProperty("id")
    public void setId(Integer id) {
        this.id = id;
    }

    @JsonProperty("name")
    public String getName() {
        return name;
    }

    @JsonProperty("name")
    public void setName(String name) {
        this.name = name;
    }

    @JsonProperty("price")
    public String getPrice() {
        return price;
    }

    @JsonProperty("price")
    public void setPrice(String price) {
        this.price = price;
    }

    @JsonProperty("description")
    public String getDescription() {
        return description;
    }

    @JsonProperty("description")
    public void setDescription(String description) {
        this.description = description;
    }

    @JsonProperty("a_image")
    public String getAImage() {
        return aImage;
    }

    @JsonProperty("a_image")
    public void setAImage(String aImage) {
        this.aImage = aImage;
    }

    @JsonProperty("family_groups_id")
    public String getFamilyGroupsId() {
        return familyGroupsId;
    }

    @JsonProperty("family_groups_id")
    public void setFamilyGroupsId(String familyGroupsId) {
        this.familyGroupsId = familyGroupsId;
    }

    @JsonProperty("print_class_id")
    public String getPrintClassId() {
        return printClassId;
    }

    @JsonProperty("print_class_id")
    public void setPrintClassId(String printClassId) {
        this.printClassId = printClassId;
    }

    @JsonProperty("condiments_groups_id")
    public String getCondimentsGroupsId() {
        return condimentsGroupsId;
    }

    @JsonProperty("condiments_groups_id")
    public void setCondimentsGroupsId(String condimentsGroupsId) {
        this.condimentsGroupsId = condimentsGroupsId;
    }

    @JsonProperty("plu_number")
    public String getPluNumber() {
        return pluNumber;
    }

    @JsonProperty("plu_number")
    public void setPluNumber(String pluNumber) {
        this.pluNumber = pluNumber;
    }

    @JsonProperty("tax")
    public String getTax() {
        return tax;
    }

    @JsonProperty("tax")
    public void setTax(String tax) {
        this.tax = tax;
    }

    @JsonProperty("is_show_menu")
    public String getIsShowMenu() {
        return isShowMenu;
    }

    @JsonProperty("is_show_menu")
    public void setIsShowMenu(String isShowMenu) {
        this.isShowMenu = isShowMenu;
    }

    @JsonProperty("created_at")
    public String getCreatedAt() {
        return createdAt;
    }

    @JsonProperty("created_at")
    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    @JsonProperty("updated_at")
    public String getUpdatedAt() {
        return updatedAt;
    }

    @JsonProperty("updated_at")
    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
