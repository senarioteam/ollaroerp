package com.senarios.Ollaroerp.Models;

import java.util.HashMap;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "id",
        "name",
        "family_groups_id",
        "description",
        "gl_accounts",
        "a_image",
        "created_at",
        "updated_at",
        "check_flag"
})
public class AllSubAlcoholicGroups {

    @JsonProperty("id")
    private Integer id;
    @JsonProperty("name")
    private String name;
    @JsonProperty("family_groups_id")
    private String familyGroupsId;
    @JsonProperty("description")
    private String description;
    @JsonProperty("gl_accounts")
    private String glAccounts;
    @JsonProperty("a_image")
    private String aImage;
    @JsonProperty("created_at")
    private String createdAt;
    @JsonProperty("updated_at")
    private String updatedAt;
    @JsonProperty("check_flag")
    private String checkFlag;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("id")
    public Integer getId() {
        return id;
    }

    @JsonProperty("id")
    public void setId(Integer id) {
        this.id = id;
    }

    @JsonProperty("name")
    public String getName() {
        return name;
    }

    @JsonProperty("name")
    public void setName(String name) {
        this.name = name;
    }

    @JsonProperty("family_groups_id")
    public String getFamilyGroupsId() {
        return familyGroupsId;
    }

    @JsonProperty("family_groups_id")
    public void setFamilyGroupsId(String familyGroupsId) {
        this.familyGroupsId = familyGroupsId;
    }

    @JsonProperty("description")
    public String getDescription() {
        return description;
    }

    @JsonProperty("description")
    public void setDescription(String description) {
        this.description = description;
    }

    @JsonProperty("gl_accounts")
    public String getGlAccounts() {
        return glAccounts;
    }

    @JsonProperty("gl_accounts")
    public void setGlAccounts(String glAccounts) {
        this.glAccounts = glAccounts;
    }

    @JsonProperty("a_image")
    public String getAImage() {
        return aImage;
    }

    @JsonProperty("a_image")
    public void setAImage(String aImage) {
        this.aImage = aImage;
    }

    @JsonProperty("created_at")
    public String getCreatedAt() {
        return createdAt;
    }

    @JsonProperty("created_at")
    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    @JsonProperty("updated_at")
    public String getUpdatedAt() {
        return updatedAt;
    }

    @JsonProperty("updated_at")
    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }

    @JsonProperty("check_flag")
    public String getCheckFlag() {
        return checkFlag;
    }

    @JsonProperty("check_flag")
    public void setCheckFlag(String checkFlag) {
        this.checkFlag = checkFlag;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
