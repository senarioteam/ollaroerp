package com.senarios.Ollaroerp.Models;

import com.activeandroid.Model;
import com.activeandroid.annotation.Column;
import com.activeandroid.annotation.Table;

@Table(name = "FamilyGroup")
public class OfflineFamilyGroup extends Model {
    @Column(name = "fam_group_id")
    private Integer fam_group_id;
    @Column(name = "name")
    private String name;
    @Column(name = "menu_item_groups_id")
    private String menu_item_groups_id;
    @Column(name = "a_image")
    private String a_image;
    @Column(name = "created_at")
    private String created_at;
    @Column(name = "updated_at")
    private String updated_at;
    @Column(name = "gl_account")
    private String gl_account;

    public String getGl_account() {
        return gl_account;
    }

    public void setGl_account(String gl_account) {
        this.gl_account = gl_account;
    }

    public OfflineFamilyGroup() {
    }

    public OfflineFamilyGroup(FamilyGroup familyGroup) {
        setA_image(familyGroup.geta_image());
        setMenuItemsGroupId(familyGroup.getmenu_item_groups_id());
        setFam_group_id(familyGroup.getId());
        setCreated_at(familyGroup.getcreated_at());
        setUpdated_at(familyGroup.getupdated_a());
        setName(familyGroup.getName());
        setGl_account(familyGroup.getgl_account());
    }

    public Integer getFam_group_id() {
        return fam_group_id;
    }

    public void setFam_group_id(Integer alco_group_id) {
        this.fam_group_id = alco_group_id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getMenuItemsGroupId() {
        return menu_item_groups_id;
    }

    public void setMenuItemsGroupId(String menu_item_groups_id) {
        this.menu_item_groups_id = menu_item_groups_id;
    }

    public String getA_image() {
        return a_image;
    }

    public void setA_image(String a_image) {
        this.a_image = a_image;
    }

    public String getCreated_at() {
        return created_at;
    }

    public void setCreated_at(String created_at) {
        this.created_at = created_at;
    }

    public String getUpdated_at() {
        return updated_at;
    }

    public void setUpdated_at(String updated_at) {
        this.updated_at = updated_at;
    }
}
