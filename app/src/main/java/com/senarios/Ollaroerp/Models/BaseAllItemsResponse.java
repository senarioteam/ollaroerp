package com.senarios.Ollaroerp.Models;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "major_group",
        "subGroups",
        "family_groups",
        "alcoholGroups",
        "subAlcoholicGroups",
        "items",
        "tables",
        "bills",
        "paidOrders"
})
public class BaseAllItemsResponse {

    @JsonProperty("major_group")
    private List<MajorGroup> major_group = null;
    @JsonProperty("subGroups")
    private List<SubMajorGroup> subGroups = null;
    @JsonProperty("family_groups")
    private List<FamilyGroup> family_groups = null;
    @JsonProperty("alcoholGroups")
    private List<AlchoholicGroup> alcoholGroups = null;
    @JsonProperty("subAlcoholicGroups")
    private List<SubAlcoholicGroup> subAlcoholicGroups = null;
    @JsonProperty("items")
    private List<ItemModel> items = null;
    @JsonProperty("tables")
    private List<TableModel> tables = null;
    @JsonProperty("bills")
    private List<UnpaidBillModel> bills = null;
    @JsonProperty("paidOrders")
    private List<PaidOrder> paidOrders = null;



    @JsonProperty("bills")
    public List<UnpaidBillModel> getBillModels() {
        return bills;
    }
    @JsonProperty("bills")
    public void setBillModels(List<UnpaidBillModel> billModels) {
        this.bills = billModels;
    }

    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("major_group")
    public List<MajorGroup> getMajorGroup() {
        return major_group;
    }

    @JsonProperty("major_group")
    public void setMajorGroup(List<MajorGroup> major_group) {
        this.major_group = major_group;
    }

    @JsonProperty("tables")
    public List<TableModel> getTables() {
        return tables;
    }

    @JsonProperty("tables")
    public void setTables(List<TableModel> tables) {
        this.tables = tables;
    }

    @JsonProperty("subGroups")
    public List<SubMajorGroup> getSubGroups() {
        return subGroups;
    }

    @JsonProperty("subGroups")
    public void setSubGroups(List<SubMajorGroup> subGroups) {
        this.subGroups = subGroups;
    }

    @JsonProperty("family_groups")
    public List<FamilyGroup> getFamilyGroups() {
        return family_groups;
    }

    @JsonProperty("family_groups")
    public void setFamilyGroups(List<FamilyGroup> family_groups) {
        this.family_groups = family_groups;
    }

    @JsonProperty("alcoholGroups")
    public List<AlchoholicGroup> getAlcoholGroups() {
        return alcoholGroups;
    }

    @JsonProperty("alcoholGroups")
    public void setAlcoholGroups(List<AlchoholicGroup> alcoholGroups) {
        this.alcoholGroups = alcoholGroups;
    }

    @JsonProperty("subAlcoholicGroups")
    public List<SubAlcoholicGroup> getSubAlcoholicGroups() {
        return subAlcoholicGroups;
    }

    @JsonProperty("subAlcoholicGroups")
    public void setSubAlcoholicGroups(List<SubAlcoholicGroup> subAlcoholicGroups) {
        this.subAlcoholicGroups = subAlcoholicGroups;
    }

    @JsonProperty("items")
    public List<ItemModel> getItems() {
        return items;
    }

    @JsonProperty("items")
    public void setItems(List<ItemModel> items) {
        this.items = items;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }
    @JsonProperty("paidOrders")
    public List<PaidOrder> getPaidOrders() {
        return paidOrders;
    }

    @JsonProperty("paidOrders")
    public void setPaidOrders(List<PaidOrder> postOrderItems) {
        this.paidOrders = postOrderItems;
    }


}
