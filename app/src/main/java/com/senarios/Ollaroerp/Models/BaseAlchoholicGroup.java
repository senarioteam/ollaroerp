package com.senarios.Ollaroerp.Models;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "alcoholGroups"
})
public class BaseAlchoholicGroup {

    @JsonProperty("alcoholGroups")
    private List<AlchoholicGroup> alcoholGroups = null;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("alcoholGroups")
    public List<AlchoholicGroup> getAlcoholGroups() {
        return alcoholGroups;
    }

    @JsonProperty("alcoholGroups")
    public void setAlcoholGroups(List<AlchoholicGroup> alcoholGroups) {
        this.alcoholGroups = alcoholGroups;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
