package com.senarios.Ollaroerp.Models;

import com.activeandroid.Model;
import com.activeandroid.annotation.Column;
import com.activeandroid.annotation.Table;

@Table(name = "ItemModel")
public class OfflineItemModel extends Model {
    @Column(name = "item_id")
    private Integer item_id;
    @Column(name = "name")
    private String name;
    @Column(name = "price")
    private String price;
    @Column(name = "description")
    private String description;
    @Column(name = "a_image")
    private String a_image;
    @Column(name = "family_groups_id")
    private String family_groups_id;
    @Column(name = "print_class_id")
    private String print_class_id;
    @Column(name = "condiments_groups_id")
    private String condiments_groups_id;
    @Column(name = "plu_number")
    private String plu_number;
    @Column(name = "tax")
    private String tax;


    public OfflineItemModel() {
    }

    public OfflineItemModel(ItemModel itemModel) {
        setItemId(itemModel.getId());
        setA_image(itemModel.getAImage());
        setCondiments_groups_id(itemModel.getcondiments_groups_id());
        setCreated_at(itemModel.getcreated_at());
        setUpdated_at(itemModel.getupdated_at());
        setDescription(itemModel.getDescription());
        setFamily_groups_id(itemModel.getfamily_groups_id());
        setIs_show_menu(itemModel.getis_show_menu());
        setPlu_number(itemModel.getplu_number());
        setName(itemModel.getName());
        setPrice(itemModel.getPrice());
        setPrint_class_id(itemModel.getprint_class_id());
        setTax(itemModel.getTax());
    }

    public Integer getItemId() {
        return item_id;
    }

    public void setItemId(Integer item_id) {
        this.item_id = item_id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getA_image() {
        return a_image;
    }

    public void setA_image(String a_image) {
        this.a_image = a_image;
    }

    public String getFamily_groups_id() {
        return family_groups_id;
    }

    public void setFamily_groups_id(String family_groups_id) {
        this.family_groups_id = family_groups_id;
    }

    public String getPrint_class_id() {
        return print_class_id;
    }

    public void setPrint_class_id(String print_class_id) {
        this.print_class_id = print_class_id;
    }

    public String getCondiments_groups_id() {
        return condiments_groups_id;
    }

    public void setCondiments_groups_id(String condiments_groups_id) {
        this.condiments_groups_id = condiments_groups_id;
    }

    public String getPlu_number() {
        return plu_number;
    }

    public void setPlu_number(String plu_number) {
        this.plu_number = plu_number;
    }

    public String getTax() {
        return tax;
    }

    public void setTax(String tax) {
        this.tax = tax;
    }

    public String getIs_show_menu() {
        return is_show_menu;
    }

    public void setIs_show_menu(String is_show_menu) {
        this.is_show_menu = is_show_menu;
    }

    public String getCreated_at() {
        return created_at;
    }

    public void setCreated_at(String created_at) {
        this.created_at = created_at;
    }

    public String getUpdated_at() {
        return updated_at;
    }

    public void setUpdated_at(String updated_at) {
        this.updated_at = updated_at;
    }

    @Column(name = "is_show_menu")

    private String is_show_menu;
    @Column(name = "created_at")
    private String created_at;
    @Column(name = "updated_at")
    private String updated_at;
}
