package com.senarios.Ollaroerp.Models;

import java.util.HashMap;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "id",
        "name",
        "restaurant",
        "section",
        "capacity",
        "booking_status",
        "waiter_id",
        "waiter",
        "created_at",
        "updated_at"
})
public class TableModel {

    @JsonProperty("id")
    private Integer id;
    @JsonProperty("name")
    private String name;
    @JsonProperty("restaurant")
    private String restaurant;
    @JsonProperty("section")
    private String section;
    @JsonProperty("capacity")
    private String capacity;
    @JsonProperty("booking_status")
    private String booking_status;
    @JsonProperty("waiter_id")
    private String waiter_id;
    @JsonProperty("waiter")
    private String waiter;
    @JsonProperty("created_at")
    private String created_at;
    @JsonProperty("updated_at")
    private String updated_at;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("id")
    public Integer getId() {
        return id;
    }

    @JsonProperty("id")
    public void setId(Integer id) {
        this.id = id;
    }

    @JsonProperty("name")
    public String getName() {
        return name;
    }

    @JsonProperty("name")
    public void setName(String name) {
        this.name = name;
    }

    @JsonProperty("restaurant")
    public String getRestaurant() {
        return restaurant;
    }

    @JsonProperty("restaurant")
    public void setRestaurant(String restaurant) {
        this.restaurant = restaurant;
    }

    @JsonProperty("section")
    public String getSection() {
        return section;
    }

    @JsonProperty("section")
    public void setSection(String section) {
        this.section = section;
    }

    @JsonProperty("capacity")
    public String getCapacity() {
        return capacity;
    }

    @JsonProperty("capacity")
    public void setCapacity(String capacity) {
        this.capacity = capacity;
    }

    @JsonProperty("booking_status")
    public String getbooking_status() {
        return booking_status;
    }

    @JsonProperty("booking_status")
    public void setbooking_status(String booking_status) {
        this.booking_status = booking_status;
    }

    @JsonProperty("waiter_id")
    public String getwaiter_id() {
        return waiter_id;
    }

    @JsonProperty("waiter_id")
    public void setwaiter_id(String waiter_id) {
        this.waiter_id = waiter_id;
    }

    @JsonProperty("waiter")
    public String getWaiter() {
        return waiter;
    }

    @JsonProperty("waiter")
    public void setWaiter(String waiter) {
        this.waiter = waiter;
    }

    @JsonProperty("created_at")
    public String getcreated_at() {
        return created_at;
    }

    @JsonProperty("created_at")
    public void setcreated_at(String created_at) {
        this.created_at = created_at;
    }

    @JsonProperty("updated_at")
    public String getupdated_at() {
        return updated_at;
    }

    @JsonProperty("updated_at")
    public void setupdated_at(String updated_at) {
        this.updated_at = updated_at;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
