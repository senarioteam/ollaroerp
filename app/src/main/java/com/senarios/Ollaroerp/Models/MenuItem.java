package com.senarios.Ollaroerp.Models;

import java.util.HashMap;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import com.google.gson.annotations.SerializedName;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "id",
        "name",
        "sub_major_groups_id",
        "timefrom",
        "timeto",
        "description",
        "a_image",
        "is_checked",
        "created_at",
        "updated_at"
})
public class MenuItem {

    @SerializedName("id")
    private Integer id;

    @SerializedName("name")
    private String name;
    @SerializedName("sub_major_groups_id")
    private String sub_major_groups_id;
    @SerializedName("timefrom")
    private String timefrom;
    @SerializedName("timeto")
    private String timeto;
    @SerializedName("description")
    private String description;
    @SerializedName("a_image")
    private String a_image;
    @SerializedName("is_checked")
    private String is_checked;
    @SerializedName("created_at")
    private String created_at;
    @SerializedName("updated_at")

    public String getIs_checked() {
        return is_checked;
    }

    public void setIs_checked(String is_checked) {
        this.is_checked = is_checked;
    }



    private String updated_at;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @SerializedName("id")
    public Integer getId() {
        return id;
    }

    @SerializedName("id")
    public void setId(Integer id) {
        this.id = id;
    }

    @SerializedName("name")
    public String getName() {
        return name;
    }

    @SerializedName("name")
    public void setName(String name) {
        this.name = name;
    }

    @SerializedName("sub_major_groups_id")
    public String getsub_major_groups_id() {
        return sub_major_groups_id;
    }

    @SerializedName("sub_major_groups_id")
    public void setsub_major_groups_id(String sub_major_groups_id) {
        this.sub_major_groups_id = sub_major_groups_id;
    }

    @SerializedName("timefrom")
    public String getTimefrom() {
        return timefrom;
    }

    @SerializedName("timefrom")
    public void setTimefrom(String timefrom) {
        this.timefrom = timefrom;
    }

    @SerializedName("timeto")
    public String getTimeto() {
        return timeto;
    }

    @SerializedName("timeto")
    public void setTimeto(String timeto) {
        this.timeto = timeto;
    }

    @SerializedName("description")
    public String getDescription() {
        return description;
    }

    @SerializedName("description")
    public void setDescription(String description) {
        this.description = description;
    }

    @SerializedName("a_image")
    public String geta_image() {
        return a_image;
    }

    @SerializedName("a_image")
    public void seta_image(String a_image) {
        this.a_image = a_image;
    }

    @SerializedName("created_at")
    public String getcreated_at() {
        return created_at;
    }

    @SerializedName("created_at")
    public void setcreated_at(String created_at) {
        this.created_at = created_at;
    }

    @SerializedName("updated_at")
    public String getupdated_at() {
        return updated_at;
    }

    @SerializedName("updated_at")
    public void setupdated_at(String updated_at) {
        this.updated_at = updated_at;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
