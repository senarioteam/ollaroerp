package com.senarios.Ollaroerp;

import java.util.ArrayList;

public interface CheckingOrdersInterface {
    public Boolean check(ArrayList<Boolean> checks);
}
