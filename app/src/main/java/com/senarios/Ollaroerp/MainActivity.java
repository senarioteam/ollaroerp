package com.senarios.Ollaroerp;

import android.app.job.JobInfo;
import android.app.job.JobScheduler;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.RequiresApi;
import android.support.v4.app.Fragment;
import android.support.v4.view.MenuItemCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.activeandroid.query.Delete;
import com.activeandroid.query.Select;
import com.kaopiz.kprogresshud.KProgressHUD;
import com.senarios.Ollaroerp.Fragments.AddToCartFragment;
import com.senarios.Ollaroerp.Fragments.BillTransferFragment;
import com.senarios.Ollaroerp.Fragments.ChangePasswordFragment;
import com.senarios.Ollaroerp.Fragments.CheckoutFragment;
import com.senarios.Ollaroerp.Fragments.ChooseBillFragment;
import com.senarios.Ollaroerp.Fragments.CombineBillFragment;
import com.senarios.Ollaroerp.Fragments.ConfirmCombineFragment;
import com.senarios.Ollaroerp.Fragments.CreateVoucherFragment;
import com.senarios.Ollaroerp.Fragments.DialogeFragments.ChooseTableDialogueFragment;
import com.senarios.Ollaroerp.Fragments.DialogeFragments.CombinePyamentDialogueFragment;
import com.senarios.Ollaroerp.Fragments.DialogeFragments.ComplimentryDialogueFragment;
import com.senarios.Ollaroerp.Fragments.DialogeFragments.GenerateBillDialogueFragment;
import com.senarios.Ollaroerp.Fragments.DialogeFragments.MpesaDialogueFragment;
import com.senarios.Ollaroerp.Fragments.DialogeFragments.OrderSelectionDialogFragment;
import com.senarios.Ollaroerp.Fragments.FamilyGroupFragment;
import com.senarios.Ollaroerp.Fragments.ForgotPasswordFragment;
import com.senarios.Ollaroerp.Fragments.GenerateBillFragment;
import com.senarios.Ollaroerp.Fragments.HomeFragment;
import com.senarios.Ollaroerp.Fragments.InprogressOrdersFragment;
import com.senarios.Ollaroerp.Fragments.LoginFragment;
import com.senarios.Ollaroerp.Fragments.ManagerHomeFragment;
import com.senarios.Ollaroerp.Fragments.SearchFragment;
import com.senarios.Ollaroerp.Fragments.SubMajorGroupFragment;
import com.senarios.Ollaroerp.Fragments.MenuFragment;
import com.senarios.Ollaroerp.Fragments.NavigationFragment;
import com.senarios.Ollaroerp.Fragments.PaymentMethodFragment;
import com.senarios.Ollaroerp.Fragments.SelectedSubcatItemFragment;
import com.senarios.Ollaroerp.Fragments.AlcoholicGroupFragment;
import com.senarios.Ollaroerp.Fragments.SubcatMenuItemFragment;
import com.senarios.Ollaroerp.Models.AlchoholicGroup;
import com.senarios.Ollaroerp.Models.BaseAllItemsResponse;
import com.senarios.Ollaroerp.Models.CartItemModel;
import com.senarios.Ollaroerp.Models.FamilyGroup;
import com.senarios.Ollaroerp.Models.ItemModel;
import com.senarios.Ollaroerp.Models.MajorGroup;
import com.senarios.Ollaroerp.Models.OfflineAlchoholicGroup;
import com.senarios.Ollaroerp.Models.OfflineBillModel;
import com.senarios.Ollaroerp.Models.OfflineFamilyGroup;
import com.senarios.Ollaroerp.Models.OfflineItemModel;
import com.senarios.Ollaroerp.Models.OfflineMajorGroup;
import com.senarios.Ollaroerp.Models.OfflineMenuItem;
import com.senarios.Ollaroerp.Models.OfflineOrderItems;
import com.senarios.Ollaroerp.Models.OfflinePostOrderItems;
import com.senarios.Ollaroerp.Models.OfflineSubAlchoholicGroup;
import com.senarios.Ollaroerp.Models.OfflineSubMajorGroup;
import com.senarios.Ollaroerp.Models.OfflineTableModel;
import com.senarios.Ollaroerp.Models.SubAlcoholicGroup;
import com.senarios.Ollaroerp.Models.SubMajorGroup;
import com.senarios.Ollaroerp.Models.TableModel;
import com.senarios.Ollaroerp.Models.UnpaidBillModel;
import com.senarios.Ollaroerp.Utitlities.FragmentTags;
import com.senarios.Ollaroerp.Utitlities.PrefUtils;
import com.senarios.Ollaroerp.Utitlities.Retrofit.GetDataService;
import com.senarios.Ollaroerp.Utitlities.Retrofit.RetrofitClientInstance;
import com.senarios.Ollaroerp.Utitlities.Services.NetworkSchedulerService;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;


public class MainActivity extends AppCompatActivity {

    public TextView itemMessagesBadgeTextView;
    private View badgeLayout;
    private ImageView iconButtonMessages;
    public static int count = 0;
    public static String sub_major_id;
    private MenuItem itemMessages;
    public static MainActivity activity;
    public DrawerLayout mDrawerLayout;
    public Toolbar toolbar;
    KProgressHUD pd;
    public static ArrayList<String> offlineOrders = new ArrayList<>();
    public static ArrayList<String>offlineAddToBills = new ArrayList<>();
    int containerId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        activity = this;
        mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout_id);
        pd = pd.create(this)
                .setStyle(KProgressHUD.Style.SPIN_INDETERMINATE)
                .setLabel("Synchronizing Data")
                .setCancellable(false)
                .setAnimationSpeed(2)
                .setDimAmount(0.5f);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            scheduleJob();
        }
        containerId = R.id.fragment_container;
        NavigationFragment navigationFragment = new NavigationFragment();
        toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        navigationFragment.setup(mDrawerLayout, toolbar);
        if (PrefUtils.getString(this, "is_logged_in", "false").equalsIgnoreCase("true")) {
           /*getEveryThing();*/

            changeFragment(R.id.fragment_container, new HomeFragment(), FragmentTags.HOME);

        } else if (PrefUtils.getString(this, "is_logged_in", "false").equalsIgnoreCase("trueManager")) {
            changeFragment(R.id.fragment_container, new ManagerHomeFragment(), FragmentTags.MANAGERHOME);
        } else {
            openLoginFragment();
        }

    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main_menu, menu);

        itemMessages = menu.findItem(R.id.menu_cart);

        itemMessages.setActionView(R.layout.counter_menuitem_layout);
        badgeLayout = (RelativeLayout) MenuItemCompat.getActionView(itemMessages);
        itemMessagesBadgeTextView = (TextView) badgeLayout.findViewById(R.id.badge_textView);
        try {
            List<CartItemModel> items = getAll();
            itemMessagesBadgeTextView.setText(Integer.toString(items.size()));
        } catch (Exception e) {
            itemMessagesBadgeTextView.setText("0");
        }
        iconButtonMessages = badgeLayout.findViewById(R.id.badge_icon_button);
        iconButtonMessages.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                changeFragment(R.id.fragment_container, new AddToCartFragment(), FragmentTags.CARTFRAGMENT);
            }
        });

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.menu_notification) {
            changeFragment(R.id.fragment_container, new SearchFragment(), FragmentTags.SEARCH);
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public void changeFragment(int container, Fragment fragment, String tag) {
        toolbar.setTitle(tag);
        getSupportFragmentManager().beginTransaction().replace(container, fragment, tag).commitAllowingStateLoss();
    }

    public void openLoginFragment() {
        toolbar.setVisibility(View.GONE);
        mDrawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED);
        changeFragment(containerId, new LoginFragment(), FragmentTags.LOGIN);
    }

    public void showToolbar() {
        toolbar.setVisibility(View.VISIBLE);
    }

    @Override
    public void onBackPressed() {
        BillTransferFragment billTransferFragment = (BillTransferFragment) getSupportFragmentManager().findFragmentByTag(FragmentTags.BILLTRANSFER);
        SearchFragment searchFragment = (SearchFragment) getSupportFragmentManager().findFragmentByTag(FragmentTags.SEARCH);
        OrderSelectionDialogFragment orderSelectionDialogueFragment = (OrderSelectionDialogFragment) getSupportFragmentManager().findFragmentByTag(FragmentTags.SELECTORDERDIALOGUE);
        FamilyGroupFragment familyGroupFragment = (FamilyGroupFragment) getSupportFragmentManager().findFragmentByTag(FragmentTags.FAMILYGROUPFRAGMENT);
        LoginFragment loginFragment = (LoginFragment) getSupportFragmentManager().findFragmentByTag(FragmentTags.LOGIN);
        ForgotPasswordFragment forgotPasswordFragment = (ForgotPasswordFragment) getSupportFragmentManager().findFragmentByTag(FragmentTags.FORGOTPASSWORD);
        HomeFragment homeFragment = (HomeFragment) getSupportFragmentManager().findFragmentByTag(FragmentTags.HOME);
        CheckoutFragment checkoutFragment = (CheckoutFragment) getSupportFragmentManager().findFragmentByTag(FragmentTags.CHECKOUT);
        MenuFragment menuFragment = (MenuFragment) getSupportFragmentManager().findFragmentByTag(FragmentTags.MENU);
        SubcatMenuItemFragment subcatMenuItemFragment = (SubcatMenuItemFragment) getSupportFragmentManager().findFragmentByTag(FragmentTags.SUBCATMENUITEM);
        AlcoholicGroupFragment alcoholicGroupFragment = (AlcoholicGroupFragment) getSupportFragmentManager().findFragmentByTag(FragmentTags.SUBCATMENU);
        SubMajorGroupFragment subMajorGroupFragment = (SubMajorGroupFragment) getSupportFragmentManager().findFragmentByTag(FragmentTags.MENUCAT);
        SelectedSubcatItemFragment selectedSubcatItemFragment = (SelectedSubcatItemFragment) getSupportFragmentManager().findFragmentByTag(FragmentTags.SELECTEDSUBCAT);
        InprogressOrdersFragment inprogressOrdersFragment = (InprogressOrdersFragment) getSupportFragmentManager().findFragmentByTag(FragmentTags.ORDERSINPROGRESS);
        GenerateBillFragment generateBillFragment = (GenerateBillFragment) getSupportFragmentManager().findFragmentByTag(FragmentTags.GENERATEBILL);
        CreateVoucherFragment createVoucherFragment = (CreateVoucherFragment) getSupportFragmentManager().findFragmentByTag(FragmentTags.CREATEVOUCHER);
        ChangePasswordFragment changePasswordFragment = (ChangePasswordFragment) getSupportFragmentManager().findFragmentByTag(FragmentTags.CHANGEPASSWORD);
        AddToCartFragment addToCartFragment = (AddToCartFragment) getSupportFragmentManager().findFragmentByTag(FragmentTags.CARTFRAGMENT);
        CombinePyamentDialogueFragment combinePyamentDialogueFragment = (CombinePyamentDialogueFragment) getSupportFragmentManager().findFragmentByTag(FragmentTags.COMBINEDIALOGUE);
        PaymentMethodFragment paymentMethodFragment = (PaymentMethodFragment) getSupportFragmentManager().findFragmentByTag(FragmentTags.PAYMENTMETHOD);
        CombineBillFragment combineBillFragment = (CombineBillFragment) getSupportFragmentManager().findFragmentByTag(FragmentTags.COMBINEBILL);
        MpesaDialogueFragment mpesaDialogueFragment = (MpesaDialogueFragment) getSupportFragmentManager().findFragmentByTag(FragmentTags.MPESADIALOGUE);
        ComplimentryDialogueFragment complimentryDialogueFragment = (ComplimentryDialogueFragment) getSupportFragmentManager().findFragmentByTag(FragmentTags.COMPLIMENTRYDIALOGUE);
        ChooseTableDialogueFragment chooseTableDialogueFragment = (ChooseTableDialogueFragment) getSupportFragmentManager().findFragmentByTag(FragmentTags.CHOOSETABLE);
        ConfirmCombineFragment confirmCombineFragment = (ConfirmCombineFragment) getSupportFragmentManager().findFragmentByTag(FragmentTags.CONFIRMCOMBINE);
        GenerateBillDialogueFragment generateBillDialogueFragment = (GenerateBillDialogueFragment) getSupportFragmentManager().findFragmentByTag(FragmentTags.GENBILLDIALOGUE);
        ChooseBillFragment chooseBillFragment = (ChooseBillFragment) getSupportFragmentManager().findFragmentByTag(FragmentTags.CHOOSEBILL);
        ManagerHomeFragment managerHomeFragment = (ManagerHomeFragment) getSupportFragmentManager().findFragmentByTag(FragmentTags.MANAGERHOME);
        if (homeFragment != null && homeFragment.isVisible()) {
            super.onBackPressed();
        } else if (inprogressOrdersFragment != null && inprogressOrdersFragment.isVisible()) {
            MainActivity.activity.changeFragment(R.id.fragment_container, new HomeFragment(), FragmentTags.HOME);
        } else if (generateBillFragment != null && generateBillFragment.isVisible()) {
            if (paymentMethodFragment != null && paymentMethodFragment.isAdded()) {
                getSupportFragmentManager().popBackStack();
            }else if(billTransferFragment!=null && billTransferFragment.isAdded() && billTransferFragment.isVisible()){
                getSupportFragmentManager().popBackStack();
            }
            else {
                MainActivity.activity.changeFragment(R.id.fragment_container, new HomeFragment(), FragmentTags.HOME);
            }
        } else if (menuFragment != null && menuFragment.isVisible()) {
            if (subMajorGroupFragment != null && subMajorGroupFragment.isAdded() && subMajorGroupFragment.isVisible()) {
                getSupportFragmentManager().popBackStack();
            } else if (alcoholicGroupFragment != null && subMajorGroupFragment.isAdded() && alcoholicGroupFragment.isVisible()) {
                getSupportFragmentManager().popBackStack();
            } else if (familyGroupFragment != null && familyGroupFragment.isVisible()) {
                getSupportFragmentManager().popBackStack();
            } else if (selectedSubcatItemFragment != null && selectedSubcatItemFragment.isVisible() && selectedSubcatItemFragment.isAdded()) {
                getSupportFragmentManager().popBackStack();
            } else if (subcatMenuItemFragment != null && subcatMenuItemFragment.isAdded() && subcatMenuItemFragment.isVisible()) {
                getSupportFragmentManager().popBackStack();
            } else if (addToCartFragment != null && addToCartFragment.isAdded() && addToCartFragment.isVisible()) {
                getSupportFragmentManager().popBackStack();
            } else {
                MainActivity.activity.changeFragment(R.id.fragment_container, new HomeFragment(), FragmentTags.HOME);
            }
        } else if (chooseTableDialogueFragment != null && chooseTableDialogueFragment.isVisible()) {
            chooseTableDialogueFragment.onBackPressed();
        } else if (orderSelectionDialogueFragment != null && orderSelectionDialogueFragment.isVisible()) {
            orderSelectionDialogueFragment.onBackPressed();
        } else if (chooseBillFragment != null && chooseBillFragment.isVisible()) {
            MainActivity.activity.changeFragment(R.id.fragment_container, new AddToCartFragment(), FragmentTags.CARTFRAGMENT);
        } else if (mpesaDialogueFragment != null && mpesaDialogueFragment.isVisible()) {
            mpesaDialogueFragment.onBackPressed();
        } else if (complimentryDialogueFragment != null && complimentryDialogueFragment.isVisible()) {
            complimentryDialogueFragment.onBackPressed();
        } else if (combinePyamentDialogueFragment != null && combinePyamentDialogueFragment.isVisible()) {
            combinePyamentDialogueFragment.onBackPressed();
        } else if (generateBillDialogueFragment != null && generateBillDialogueFragment.isVisible()) {
            generateBillDialogueFragment.onBackPressed();
        } else if (paymentMethodFragment != null && paymentMethodFragment.isVisible()) {
            getSupportFragmentManager().popBackStack();
        } else if (addToCartFragment != null && addToCartFragment.isVisible()) {
            MainActivity.activity.changeFragment(R.id.fragment_container, new HomeFragment(), FragmentTags.HOME);
        } else if (checkoutFragment != null && checkoutFragment.isVisible()) {
            MainActivity.activity.changeFragment(R.id.fragment_container, new AddToCartFragment(), FragmentTags.CARTFRAGMENT);
        } else if (changePasswordFragment != null && changePasswordFragment.isVisible()) {
            MainActivity.activity.changeFragment(R.id.fragment_container, new HomeFragment(), FragmentTags.HOME);
        } else if (createVoucherFragment != null && createVoucherFragment.isVisible()) {
            MainActivity.activity.changeFragment(R.id.fragment_container, new HomeFragment(), FragmentTags.HOME);
        } else if (forgotPasswordFragment != null && forgotPasswordFragment.isVisible()) {
            MainActivity.activity.changeFragment(R.id.fragment_container, new LoginFragment(), FragmentTags.LOGIN);
        } else if (loginFragment != null && loginFragment.isVisible()) {
            super.onBackPressed();
        } else if (combineBillFragment != null && combineBillFragment.isVisible()) {
            MainActivity.activity.changeFragment(R.id.fragment_container, new GenerateBillFragment(), FragmentTags.GENERATEBILL);
        } else if (confirmCombineFragment != null && confirmCombineFragment.isVisible()) {
            MainActivity.activity.changeFragment(R.id.fragment_container, new ChooseBillFragment(), FragmentTags.COMBINEBILL);
        } else if (searchFragment != null && searchFragment.isVisible()) {
            if (selectedSubcatItemFragment != null && selectedSubcatItemFragment.isAdded()) {
                getSupportFragmentManager().popBackStack();
            } else {
                MainActivity.activity.changeFragment(R.id.fragment_container, new HomeFragment(), FragmentTags.HOME);
            }
        } else if (managerHomeFragment != null && managerHomeFragment.isVisible()) {
            super.onBackPressed();
        }
    }

    public static List<CartItemModel> getAll() {
        return new Select()
                .from(CartItemModel.class)
                .execute();
    }

    public static List<OfflineMajorGroup> getAllMajorGroups() {
        return new Select()
                .from(OfflineMajorGroup.class)
                .execute();
    }

    public static List<OfflineSubMajorGroup> getAllSubMajorGroups(String major_id) {
        return new Select()
                .from(OfflineSubMajorGroup.class)
                .where("major_id =?", major_id)
                .execute();
    }

    public static List<OfflineMenuItem> getAllMenuItems(String sub_major_id) {
        return new Select()
                .from(OfflineMenuItem.class)
                .where("sub_major_groups_id =?", sub_major_id)
                .execute();
    }

    public static List<OfflineItemModel> getAllItems(String id) {
        return new Select()
                .from(OfflineItemModel.class)
                .where("family_groups_id =?", id)
                .execute();
    }

    public static List<OfflineAlchoholicGroup> getAllAlcoGroups(String group_id) {
        return new Select()
                .from(OfflineAlchoholicGroup.class)
                .where("menu_items_group_id =?", group_id)
                .execute();
    }

    public static List<OfflineSubAlchoholicGroup> getAllSubAlcoGroups(String sub_group_id) {
        return new Select()
                .from(OfflineSubAlchoholicGroup.class)
                .where("family_groups_id =?", sub_group_id)
                .execute();
    }

    public static List<OfflineFamilyGroup> getAllFamGroups(String group_id) {
        return new Select()
                .from(OfflineFamilyGroup.class)
                .where("menu_item_groups_id =?", group_id)
                .execute();
    }

    public static List<OfflineTableModel> getAllTables() {
        return new Select()
                .from(OfflineTableModel.class)
                .execute();
    }

    public static List<OfflineBillModel> getAllBills() {
        return new Select()
                .from(OfflineBillModel.class)
                .execute();
    }

    public static List<OfflinePostOrderItems> getAllPostOrderItems() {
        return new Select()
                .from(OfflinePostOrderItems.class)
                .execute();
    }

    public static  List<OfflineOrderItems> getOrderitemsbyID(String id){
        return new Select()
                .from(OfflineOrderItems.class)
                .where("post_paid_orders_id =?",id)
                .execute();
    }


    public void updateItemCount() {
        List<CartItemModel> items = getAll();
        itemMessagesBadgeTextView.setText(Integer.toString(items.size()));
    }

    public static CartItemModel getRow(CartItemModel category) {
        return new Select()
                .from(CartItemModel.class)
                .where("general_item_id = ?", category.getGeneral_item_id())
                .executeSingle();
    }

    public static OfflinePostOrderItems getPostOrderRow(OfflinePostOrderItems offlinePostOrderItems) {
        return new Select()
                .from(OfflinePostOrderItems.class)
                .where("Postorderid = ?", offlinePostOrderItems.getPostorderid())
                .executeSingle();
    }

    public static OfflineOrderItems getOrderItemRow(OfflineOrderItems OrderItems) {
        return new Select()
                .from(OfflineOrderItems.class)
                .where("orderid = ?", OrderItems.getOrderid())
                .executeSingle();
    }




    public static OfflineMenuItem getMenuItemRow(OfflineMenuItem category) {
        return new Select()
                .from(OfflineMenuItem.class)
                .where("menu_id = ?", category.getMenu_id())
                .executeSingle();
    }

    public static OfflineBillModel getBillRow(UnpaidBillModel category) {
        return new Select()
                .from(OfflineBillModel.class)
                .where("bill_id = ?", String.valueOf(category.getId()))
                .executeSingle();
    }

    public static OfflineMajorGroup getMajorGroupRow(MajorGroup category) {
        return new Select()
                .from(OfflineMajorGroup.class)
                .where("major_id = ?", String.valueOf(category.getId()))
                .executeSingle();
    }


    public static OfflineSubMajorGroup getSubMajorGroupRow(SubMajorGroup category) {
        return new Select()
                .from(OfflineSubMajorGroup.class)
                .where("sub_major_id = ?", String.valueOf(category.getId()))
                .executeSingle();
    }

    public static OfflineAlchoholicGroup getAlcoGroupRow(AlchoholicGroup category) {
        return new Select()
                .from(OfflineAlchoholicGroup.class)
                .where("alco_group_id = ?", String.valueOf(category.getId()))
                .executeSingle();
    }

    public static OfflineFamilyGroup getFamGroupRow(FamilyGroup category) {
        return new Select()
                .from(OfflineFamilyGroup.class)
                .where("fam_group_id = ?", String.valueOf(category.getId()))
                .executeSingle();
    }

    public static OfflineSubAlchoholicGroup getSubAlcoGroupRow(SubAlcoholicGroup category) {
        return new Select()
                .from(OfflineSubAlchoholicGroup.class)
                .where("sub_alco_id = ?", String.valueOf(category.getId()))
                .executeSingle();
    }

    public static OfflineItemModel getItemRow(ItemModel category) {
        return new Select()
                .from(OfflineItemModel.class)
                .where("item_id = ?", String.valueOf(category.getId()))
                .executeSingle();
    }

    public static OfflineTableModel getTableRow(TableModel category) {
        return new Select()
                .from(OfflineTableModel.class)
                .where("table_id = ?", String.valueOf(category.getId()))
                .executeSingle();
    }


    public void getEveryThing() {
        pd.show();
        GetDataService service = RetrofitClientInstance.getRetrofitInstance().create(GetDataService.class);

        Call<BaseAllItemsResponse> call = service.getEverything(String.valueOf(PrefUtils.getUserModel(MainActivity.this).getId()));
        call.enqueue(new Callback<BaseAllItemsResponse>() {

            @Override
            public void onResponse(Call<BaseAllItemsResponse> call, final retrofit2.Response<BaseAllItemsResponse> response) {
                if (response.code() == 200) {


                    new getEverything().execute(response.body());


                   /* new Delete().from(OfflineMajorGroup.class).execute();
                    new Delete().from(OfflineSubMajorGroup.class).execute();
                    new Delete().from(OfflineAlchoholicGroup.class).execute();
                    new Delete().from(OfflineFamilyGroup.class).execute();
                    new Delete().from(OfflineSubAlchoholicGroup.class).execute();
                    new Delete().from(OfflineItemModel.class).execute();
                    new Delete().from(OfflineTableModel.class).execute();
                    new Delete().from(OfflineMajorGroup.class).execute();
                    new Delete().from(OfflineBillModel.class).execute();
                    new Delete().from(OfflinePostOrderItems.class).execute();
                    new Delete().from(OfflineOrderItems.class).execute();
                    new Delete().from(OfflineMenuItem.class).execute();




                    for (int i = 0; i < response.body().getMajorGroup().size(); i++) {
                        if (response.body().getMajorGroup().size()!=0) {
                            OfflineMajorGroup offlineMajorGroup = new OfflineMajorGroup();
                            offlineMajorGroup.setMajor_id(response.body().getMajorGroup().get(i).getId());
                            offlineMajorGroup.setA_image(response.body().getMajorGroup().get(i).getAImage());
                            offlineMajorGroup.setName(response.body().getMajorGroup().get(i).getName());
                            offlineMajorGroup.setCreated_at(response.body().getMajorGroup().get(i).getCreatedAt());
                            offlineMajorGroup.setUpdated_at(response.body().getMajorGroup().get(i).getUpdatedAt());
                            offlineMajorGroup.setDisplay_order(response.body().getMajorGroup().get(i).getDisplayOrder());
                            if (MainActivity.getMajorGroupRow(response.body().getMajorGroup().get(i)) == null) {
                                offlineMajorGroup.save();
                            }
                        }
                        if (response.body().getSubGroups().size()!=0){
                            for (int a = 0; a < response.body().getSubGroups().size(); a++) {
                                OfflineSubMajorGroup offlineSubMajorGroup = new OfflineSubMajorGroup(response.body().getSubGroups().get(a));
                                offlineSubMajorGroup.setMajor_id(String.valueOf(response.body().getMajorGroup().get(i).getId()));
                                if (MainActivity.getSubMajorGroupRow(response.body().getSubGroups().get(a)) == null) {
                                    offlineSubMajorGroup.save();
                                }
                            }

                        }
                        if (response.body().getAlcoholGroups().size()!=0){

                            for (int b = 0; b < response.body().getAlcoholGroups().size(); b++) {
                                OfflineAlchoholicGroup offlineAlchoholicGroup = new OfflineAlchoholicGroup(response.body().getAlcoholGroups().get(b));
                                if (MainActivity.getAlcoGroupRow(response.body().getAlcoholGroups().get(b)) == null) {
                                    offlineAlchoholicGroup.save();
                                }
                            }

                        }
                        if (response.body().getFamilyGroups().size()!=0){
                            for (int c = 0; c < response.body().getFamilyGroups().size(); c++) {
                                OfflineFamilyGroup offlineFamilyGroup = new OfflineFamilyGroup(response.body().getFamilyGroups().get(c));
                                if (MainActivity.getFamGroupRow(response.body().getFamilyGroups().get(c)) == null) {
                                    offlineFamilyGroup.save();
                                }
                            }
                        }
                        if (response.body().getSubAlcoholicGroups().size()!=0){
                            for (int d = 0; d < response.body().getSubAlcoholicGroups().size(); d++) {
                                OfflineSubAlchoholicGroup offlineAlchoholicGroup = new OfflineSubAlchoholicGroup(response.body().getSubAlcoholicGroups().get(d));
                                if (MainActivity.getSubAlcoGroupRow(response.body().getSubAlcoholicGroups().get(d)) == null) {
                                    offlineAlchoholicGroup.save();
                                }}
                        }
                        if (response.body().getItems().size()!=0){
                            for (int e = 0; e < response.body().getItems().size(); e++) {
                                OfflineItemModel offlineItemModel = new OfflineItemModel(response.body().getItems().get(e));
                                if (MainActivity.getItemRow(response.body().getItems().get(e)) == null) {
                                    offlineItemModel.save();
                                }
                            }

                        }
                        if (response.body().getTables().size()!=0) {
                            for (int f = 0; f < response.body().getTables().size(); f++) {
                                if (response.body().getTables().get(f).getwaiter_id().equalsIgnoreCase(String.valueOf(PrefUtils.getUserModel(MainActivity.this).getId()))) {
                                    OfflineTableModel offlineTableModel = new OfflineTableModel(response.body().getTables().get(f));
                                    if (MainActivity.getTableRow(response.body().getTables().get(f)) == null) {
                                        offlineTableModel.save();
                                    }
                                }
                            }
                        }

                        if (response.body().getBillModels().size()!=0){
                            for (int k = 0; k < response.body().getBillModels().size(); k++) {
                                OfflineBillModel offlineBillModel = new OfflineBillModel(response.body().getBillModels().get(k));
                                if (MainActivity.getBillRow(response.body().getBillModels().get(k)) == null) {
                                    offlineBillModel.save();
                                }}
                        }



                    }
                    if (response.body().getPaidOrders().size()!=0) {
                        for (int n = 0; n < response.body().getPaidOrders().size(); n++) {
                            for (int dum=0;dum<response.body().getPaidOrders().get(n).getOrder_items().size();dum++) {
                                OfflineOrderItems offlineOrderItems = new OfflineOrderItems(response.body().getPaidOrders().get(n).getOrder_items().get(dum));
                                if (MainActivity.getOrderItemRow(offlineOrderItems) == null) {
                                    offlineOrderItems.save();
                                }
                            }
                        }
                    }
                    if (response.body().getPaidOrders().size()!=0){
                        for(int m=0;m<response.body().getPaidOrders().size();m++) {
                            OfflinePostOrderItems offlinePostOrderItems = new OfflinePostOrderItems(response.body().getPaidOrders().get(m));
                            if (MainActivity.getPostOrderRow(offlinePostOrderItems) == null) {
                                offlinePostOrderItems.save();
                            }
                        }
                    }
*/



                } else {
                    pd.dismiss();
                    changeFragment(R.id.fragment_container, new HomeFragment(), FragmentTags.HOME);
                    Toast.makeText(MainActivity.this, "Error fetching list! Please try again later", Toast.LENGTH_SHORT).show();
                }
                /*pd.dismiss();*/

            }

            @Override
            public void onFailure(Call<BaseAllItemsResponse> call, Throwable t) {
                pd.dismiss();
                changeFragment(R.id.fragment_container, new HomeFragment(), FragmentTags.HOME);
                if (!(getAllMajorGroups().size() > 0)) {
                    Toast.makeText(MainActivity.activity, "Something went wrong...Please try later!", Toast.LENGTH_SHORT).show();
                }
            }
        });


    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    private void scheduleJob() {
        JobInfo myJob = new JobInfo.Builder(0, new ComponentName(this, NetworkSchedulerService.class))
                .setMinimumLatency(1000)
                .setOverrideDeadline(2000)
                .setRequiredNetworkType(JobInfo.NETWORK_TYPE_ANY)
                .setPersisted(true)
                .build();

        JobScheduler jobScheduler = (JobScheduler) getSystemService(Context.JOB_SCHEDULER_SERVICE);
        jobScheduler.schedule(myJob);
    }

    @Override
    protected void onStop() {
        // A service can be "started" and/or "bound". In this case, it's "started" by this Activity
        // and "bound" to the JobScheduler (also called "Scheduled" by the JobScheduler). This call
        // to stopService() won't prevent scheduled jobs to be processed. However, failing
        // to call stopService() would keep it alive indefinitely.
        stopService(new Intent(this, NetworkSchedulerService.class));
        super.onStop();
    }

    @Override
    protected void onStart() {
        super.onStart();
        // Start service and provide it a way to communicate with this class.
        Intent startServiceIntent = new Intent(this, NetworkSchedulerService.class);
        startService(startServiceIntent);
    }
    private boolean isNetworkAvailable() {
        ConnectivityManager connectivityManager
                = (ConnectivityManager) getApplicationContext().getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }

    public class getEverything extends AsyncTask<BaseAllItemsResponse,Void,Void>{
        @Override
        protected Void doInBackground(BaseAllItemsResponse... baseAllItemsResponses) {
            new Delete().from(OfflineMajorGroup.class).execute();
            new Delete().from(OfflineSubMajorGroup.class).execute();
            new Delete().from(OfflineAlchoholicGroup.class).execute();
            new Delete().from(OfflineFamilyGroup.class).execute();
            new Delete().from(OfflineSubAlchoholicGroup.class).execute();
            new Delete().from(OfflineItemModel.class).execute();
            new Delete().from(OfflineTableModel.class).execute();
            new Delete().from(OfflineMajorGroup.class).execute();
            new Delete().from(OfflineBillModel.class).execute();
            new Delete().from(OfflinePostOrderItems.class).execute();
            new Delete().from(OfflineOrderItems.class).execute();
            new Delete().from(OfflineMenuItem.class).execute();



            for (int i = 0; i < baseAllItemsResponses[0].getMajorGroup().size(); i++) {
                if (baseAllItemsResponses[0].getMajorGroup().size()!=0) {
                    OfflineMajorGroup offlineMajorGroup = new OfflineMajorGroup();
                    offlineMajorGroup.setMajor_id(baseAllItemsResponses[0].getMajorGroup().get(i).getId());
                    offlineMajorGroup.setA_image(baseAllItemsResponses[0].getMajorGroup().get(i).getAImage());
                    offlineMajorGroup.setName(baseAllItemsResponses[0].getMajorGroup().get(i).getName());
                    offlineMajorGroup.setCreated_at(baseAllItemsResponses[0].getMajorGroup().get(i).getCreatedAt());
                    offlineMajorGroup.setUpdated_at(baseAllItemsResponses[0].getMajorGroup().get(i).getUpdatedAt());
                    offlineMajorGroup.setDisplay_order(baseAllItemsResponses[0].getMajorGroup().get(i).getDisplayOrder());
                    offlineMajorGroup.save();
                }
                if (baseAllItemsResponses[0].getSubGroups().size()!=0){
                    for (int a = 0; a <baseAllItemsResponses[0].getSubGroups().size(); a++) {
                        OfflineSubMajorGroup offlineSubMajorGroup = new OfflineSubMajorGroup(baseAllItemsResponses[0].getSubGroups().get(a));
                        offlineSubMajorGroup.setMajor_id(String.valueOf(baseAllItemsResponses[0].getMajorGroup().get(i).getId()));
                        offlineSubMajorGroup.save();
                    }

                }
                if (baseAllItemsResponses[0].getAlcoholGroups().size()!=0){

                    for (int b = 0; b < baseAllItemsResponses[0].getAlcoholGroups().size(); b++) {
                        OfflineAlchoholicGroup offlineAlchoholicGroup = new OfflineAlchoholicGroup(baseAllItemsResponses[0].getAlcoholGroups().get(b));
                        offlineAlchoholicGroup.save();
                    }

                }
                if (baseAllItemsResponses[0].getFamilyGroups().size()!=0){
                    for (int c = 0; c < baseAllItemsResponses[0].getFamilyGroups().size(); c++) {
                        OfflineFamilyGroup offlineFamilyGroup = new OfflineFamilyGroup(baseAllItemsResponses[0].getFamilyGroups().get(c));
                        offlineFamilyGroup.save();
                    }
                }
                if (baseAllItemsResponses[0].getSubAlcoholicGroups().size()!=0) {
                    for (int d = 0; d < baseAllItemsResponses[0].getSubAlcoholicGroups().size(); d++) {
                        OfflineSubAlchoholicGroup offlineAlchoholicGroup = new OfflineSubAlchoholicGroup(baseAllItemsResponses[0].getSubAlcoholicGroups().get(d));
                        offlineAlchoholicGroup.save();
                    }
                }
                if (baseAllItemsResponses[0].getItems().size()!=0){
                  /*  int id=0;
                    if (PrefUtils.getsharedpreference(MainActivity.this).contains("itemid")){
                       id= Integer.valueOf(PrefUtils.getString(MainActivity.this,"itemid"));
                    }*/

                    for (int e = 0; e < baseAllItemsResponses[0].getItems().size(); e++) {
                        /*if (baseAllItemsResponses[0].getItems().get(i).getId()>id){
                            OfflineItemModel offlineItemModel = new OfflineItemModel(baseAllItemsResponses[0].getItems().get(e));
                            offlineItemModel.save();
                        }*/
                        OfflineItemModel offlineItemModel = new OfflineItemModel(baseAllItemsResponses[0].getItems().get(e));
                        offlineItemModel.save();
                    }
                 /*   PrefUtils.putString(MainActivity.this,"itemid",String.valueOf(baseAllItemsResponses[0].getItems().get(baseAllItemsResponses[0].getItems().size()-1).getId()));
*/
                }
                if (baseAllItemsResponses[0].getTables().size()!=0) {
                    for (int f = 0; f < baseAllItemsResponses[0].getTables().size(); f++) {
                        if (baseAllItemsResponses[0].getTables().get(f).getwaiter_id().equalsIgnoreCase(String.valueOf(PrefUtils.getUserModel(MainActivity.this).getId()))) {
                            OfflineTableModel offlineTableModel = new OfflineTableModel(baseAllItemsResponses[0].getTables().get(f));
                            offlineTableModel.save();
                        }
                    }
                }

                if (baseAllItemsResponses[0].getBillModels().size()!=0){
                    for (int k = 0; k < baseAllItemsResponses[0].getBillModels().size(); k++) {
                        OfflineBillModel offlineBillModel = new OfflineBillModel(baseAllItemsResponses[0].getBillModels().get(k));
                        offlineBillModel.save();
                    }
                }



            }
            if (baseAllItemsResponses[0].getPaidOrders().size()!=0) {
                for (int n = 0; n < baseAllItemsResponses[0].getPaidOrders().size(); n++) {
                    for (int dum=0;dum<baseAllItemsResponses[0].getPaidOrders().get(n).getOrder_items().size();dum++) {
                        OfflineOrderItems offlineOrderItems = new OfflineOrderItems(baseAllItemsResponses[0].getPaidOrders().get(n).getOrder_items().get(dum));
                        offlineOrderItems.save();
                    }
                }
            }
            if (baseAllItemsResponses[0].getPaidOrders().size()!=0){
                for(int m=0;m<baseAllItemsResponses[0].getPaidOrders().size();m++) {
                    OfflinePostOrderItems offlinePostOrderItems = new OfflinePostOrderItems(baseAllItemsResponses[0].getPaidOrders().get(m));
                    offlinePostOrderItems.save();
                }
            }

            return null;
        }

        @Override
        protected void onPreExecute() {
            pd.show();
            super.onPreExecute();
        }

        @Override
        protected void onPostExecute(Void aVoid) {

            Date c = Calendar.getInstance().getTime();
            System.out.println("Current time => " + c);

            SimpleDateFormat df = new SimpleDateFormat("dd-MMM-yyyy");
            String formattedDate = df.format(c);
            PrefUtils.putString(MainActivity.this, "date", formattedDate);
            changeFragment(R.id.fragment_container, new HomeFragment(), FragmentTags.HOME);
            pd.dismiss();
            super.onPostExecute(aVoid
            );
        }




    }

}
