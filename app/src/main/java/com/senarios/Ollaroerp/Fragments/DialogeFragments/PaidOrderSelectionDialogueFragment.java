package com.senarios.Ollaroerp.Fragments.DialogeFragments;

import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;

import com.senarios.Ollaroerp.R;
import com.senarios.Ollaroerp.Utitlities.ChangeOrderStatus;


public class PaidOrderSelectionDialogueFragment extends DialogFragment {
    ChangeOrderStatus changeOrderStatus;
    RadioButton inprogRadio,compRadio;
    Boolean check;

    public PaidOrderSelectionDialogueFragment(ChangeOrderStatus listSelecter, Boolean check) {
        changeOrderStatus = listSelecter;
        this.check=check;

    }

    public PaidOrderSelectionDialogueFragment(){

    }

    ImageView cross;
    LinearLayout inprogress, completed;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.paid_order_dialog_selection, container, false);
        cross = v.findViewById(R.id.cross);
        cross.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
            }
        });
        completed = v.findViewById(R.id.completed_selection);
        inprogress = v.findViewById(R.id.inprogress_selection);
        inprogRadio=v.findViewById(R.id.inprog_radio);
        inprogRadio.setEnabled(false);
        compRadio=v.findViewById(R.id.comp_radio);
        compRadio.setEnabled(false);
        if(check){
            compRadio.setChecked(true);
        }else if(!check){
            inprogRadio.setChecked(true);
        }
        completed.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                changeOrderStatus.listType(1);
                compRadio.setChecked(true);
                inprogRadio.setChecked(false);
                dismiss();
            }
        });
        inprogress.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                changeOrderStatus.listType(0);
                compRadio.setChecked(false);
                inprogRadio.setChecked(true);
                dismiss();
            }
        });
        return v;
    }

    public void onBackPressed() {
        dismiss();
    }


}
