package com.senarios.Ollaroerp.Fragments;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.kaopiz.kprogresshud.KProgressHUD;
import com.senarios.Ollaroerp.Adapters.SubcatMenuItemAdapter;
import com.senarios.Ollaroerp.MainActivity;
import com.senarios.Ollaroerp.Models.BaseItemModel;
import com.senarios.Ollaroerp.Models.ItemModel;
import com.senarios.Ollaroerp.Models.OfflineItemModel;
import com.senarios.Ollaroerp.R;
import com.senarios.Ollaroerp.Utitlities.Retrofit.GetDataService;
import com.senarios.Ollaroerp.Utitlities.Retrofit.RetrofitClientInstance;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;

public class SubcatMenuItemFragment extends Fragment {

    private RecyclerView subMenuItemRecyclerView;
    private SubcatMenuItemAdapter subcatMenuItemAdapter;
    private ArrayList<ItemModel> menuItemModels;
    private String sub_major_id;
    KProgressHUD pd;
    private String sub_family_major_id;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        View rootView = inflater.inflate(R.layout.fragment_subcategory_menu_item, container, false);
        initView(rootView);
        return rootView;
    }

    private void initView(View rootView) {
        menuItemModels = new ArrayList<>();
        subMenuItemRecyclerView = rootView.findViewById(R.id.rv_subcat_menu_item);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        pd = pd.create(getActivity())
                .setStyle(KProgressHUD.Style.SPIN_INDETERMINATE)
                .setLabel("Please Wait")
                .setCancellable(false)
                .setAnimationSpeed(2)
                .setDimAmount(0.5f);

    }

    @Override
    public void onStart() {
        super.onStart();
        Bundle args = getArguments();
        if (args != null) {
            sub_major_id = args.getString("sub_alco_id");
            sub_family_major_id = args.getString("sub_family_id");
            menuItemModels = new ArrayList<>();
          /*  if(isNetworkAvailable()) {
                pd.show();
                callGeneralItems();
            }else{*/
            if (sub_family_major_id != null) {
                String completeId = sub_family_major_id + "_family";
                if (MainActivity.getAllItems(completeId).size() != 0) {
                    menuItemModels.clear();
                    List<OfflineItemModel> familyGroups;
                    familyGroups = MainActivity.getAllItems(completeId);
                    for (int i = 0; i < familyGroups.size(); i++) {
                        ItemModel itemModel = new ItemModel();
                        itemModel.setId(familyGroups.get(i).getItemId());
                        itemModel.setAImage(familyGroups.get(i).getA_image());
                        itemModel.setName(familyGroups.get(i).getName());
                        itemModel.setcreated_at(familyGroups.get(i).getCreated_at());
                        itemModel.setupdated_at(familyGroups.get(i).getUpdated_at());
                        itemModel.setPrice(familyGroups.get(i).getPrice());
                        itemModel.setTax(familyGroups.get(i).getTax());
                        itemModel.setcondiments_groups_id(familyGroups.get(i).getCondiments_groups_id());
                        itemModel.setDescription(familyGroups.get(i).getDescription());
                        itemModel.setis_show_menu(familyGroups.get(i).getIs_show_menu());
                        itemModel.setplu_number(familyGroups.get(i).getPlu_number());
                        itemModel.setprint_class_id(familyGroups.get(i).getPrint_class_id());
                        itemModel.setfamily_groups_id(familyGroups.get(i).getFamily_groups_id());
                        menuItemModels.add(itemModel);
                    }

                    subcatMenuItemAdapter = new SubcatMenuItemAdapter(getActivity(), menuItemModels);
                    subMenuItemRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
                    subMenuItemRecyclerView.setAdapter(subcatMenuItemAdapter);
                } else {
                    if (isNetworkAvailable()) {
                        pd.show();
                        callGeneralItems();
                    } else {
                        Toast.makeText(getActivity(), "No items in the list! Try synchronizing again for updating items", Toast.LENGTH_LONG).show();
                    }
                }
            } else if (sub_major_id != null) {
                String completeId = sub_major_id + "_alcohol_family";
                if (MainActivity.getAllItems(completeId).size() != 0) {
                    menuItemModels.clear();
                    List<OfflineItemModel> familyGroups;
                    familyGroups = MainActivity.getAllItems(completeId);
                    for (int i = 0; i < familyGroups.size(); i++) {
                        ItemModel itemModel = new ItemModel();
                        itemModel.setId(familyGroups.get(i).getItemId());
                        itemModel.setAImage(familyGroups.get(i).getA_image());
                        itemModel.setName(familyGroups.get(i).getName());
                        itemModel.setcreated_at(familyGroups.get(i).getCreated_at());
                        itemModel.setupdated_at(familyGroups.get(i).getUpdated_at());
                        itemModel.setPrice(familyGroups.get(i).getPrice());
                        itemModel.setTax(familyGroups.get(i).getTax());
                        itemModel.setcondiments_groups_id(familyGroups.get(i).getCondiments_groups_id());
                        itemModel.setDescription(familyGroups.get(i).getDescription());
                        itemModel.setis_show_menu(familyGroups.get(i).getIs_show_menu());
                        itemModel.setplu_number(familyGroups.get(i).getPlu_number());
                        itemModel.setprint_class_id(familyGroups.get(i).getPrint_class_id());
                        itemModel.setfamily_groups_id(familyGroups.get(i).getFamily_groups_id());
                        menuItemModels.add(itemModel);
                    }

                    subcatMenuItemAdapter = new SubcatMenuItemAdapter(getActivity(), menuItemModels);
                    subMenuItemRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
                    subMenuItemRecyclerView.setAdapter(subcatMenuItemAdapter);
                } else {
                    if (isNetworkAvailable()) {
                        pd.show();
                        callGeneralItems();
                    } else {
                        Toast.makeText(getActivity(), "No items in the list! Try synchronizing again for updating items", Toast.LENGTH_LONG).show();
                    }
                }


            }

        }
    }

    public void callGeneralItems() {
        GetDataService service = RetrofitClientInstance.getRetrofitInstance().create(GetDataService.class);
        Call<BaseItemModel> call = null;
        if (sub_major_id != null) {
            call = service.getGeneralItems(sub_major_id, "true");
        } else if (sub_family_major_id != null) {
            call = service.getGeneralItems(sub_family_major_id, "false");
        }
        if (call != null) {
            call.enqueue(new Callback<BaseItemModel>() {

                @Override
                public void onResponse(Call<BaseItemModel> call, retrofit2.Response<BaseItemModel> response) {
                    if (response.body() != null) {
                        for (int i = 0; i < response.body().getItems().size(); i++) {
                            menuItemModels.add(response.body().getItems().get(i));
                            OfflineItemModel offlineItemModel = new OfflineItemModel(response.body().getItems().get(i));
                            if (MainActivity.getItemRow(response.body().getItems().get(i)) == null) {
                                offlineItemModel.save();
                            }
                        }
                        subcatMenuItemAdapter = new SubcatMenuItemAdapter(getActivity(), menuItemModels);
                        subMenuItemRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
                        subMenuItemRecyclerView.setAdapter(subcatMenuItemAdapter);
                    } else {
                        Toast.makeText(getActivity(), "Error fetching list! Please try again later", Toast.LENGTH_SHORT).show();
                    }
                    pd.dismiss();
                }

                @Override
                public void onFailure(Call<BaseItemModel> call, Throwable t) {
                    pd.dismiss();
                    Toast.makeText(MainActivity.activity, "Something went wrong...Please try later!", Toast.LENGTH_SHORT).show();
                }
            });


        }
    }

    private boolean isNetworkAvailable() {
        ConnectivityManager connectivityManager
                = (ConnectivityManager) getActivity().getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }


}
