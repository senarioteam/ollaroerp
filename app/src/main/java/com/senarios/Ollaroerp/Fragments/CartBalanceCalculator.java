package com.senarios.Ollaroerp.Fragments;

public interface CartBalanceCalculator {

    public void deductPrice(String price);
    public void inductPrice(String price);
    public void del(int pos,String price);

}
