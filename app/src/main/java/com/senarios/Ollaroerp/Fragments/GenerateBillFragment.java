package com.senarios.Ollaroerp.Fragments;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.senarios.Ollaroerp.R;


public class GenerateBillFragment extends Fragment {

    private TabLayout tabLayout;
    private ViewPager defaultViewpager;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View rootView = inflater.inflate(R.layout.fragment_generate_bill, container, false);
        initView(rootView);
        return rootView;
    }

    private void initView(View rootView){
        tabLayout = rootView.findViewById(R.id.tab_layout);
        defaultViewpager =  rootView.findViewById(R.id.viewpager_default);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        tabLayout.addTab(tabLayout.newTab().setText("UNPAID ORDERS"));
        tabLayout.addTab(tabLayout.newTab().setText("UNPAID BILLS"));
        tabLayout.addTab(tabLayout.newTab().setText("PAID ORDERS"));


        tabLayout.setTabGravity(TabLayout.GRAVITY_FILL);

        MyPagerAdapter adapter;

        adapter = new MyPagerAdapter(getChildFragmentManager(), 3);
        defaultViewpager.setAdapter(adapter);

        defaultViewpager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabLayout));
        defaultViewpager.setOffscreenPageLimit(3);
        tabLayout.setOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {


            @Override
            public void onTabSelected(TabLayout.Tab tab) {

                defaultViewpager.setCurrentItem(tab.getPosition());
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });
    }

    public class MyPagerAdapter extends FragmentPagerAdapter {
        int mNumOfTabs;

        public MyPagerAdapter(FragmentManager fm, int NumOfTabs) {
            super(fm);
            this.mNumOfTabs = NumOfTabs;
        }

        @Override
        public Fragment getItem(int position) {

            switch (position) {
                case 0:
                    UnpaidOrdersFragment tab1 = new UnpaidOrdersFragment();
                    return tab1;

                case 1:
                    UnpaidBillsFragment tab2 = new UnpaidBillsFragment();
                    return tab2;

                case 2:
                    PaidOrdersFragment tab3 = new PaidOrdersFragment();
                    return tab3;

                default:
                    return null;
            }
        }

        @Override
        public int getCount() {
            return mNumOfTabs;
        }
    }

}

