package com.senarios.Ollaroerp.Fragments;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.gson.Gson;
import com.senarios.Ollaroerp.Fragments.DialogeFragments.ComplimentryDialogueFragment;
import com.senarios.Ollaroerp.Fragments.DialogeFragments.MpesaDialogueFragment;
import com.senarios.Ollaroerp.Models.Order;
import com.senarios.Ollaroerp.Models.UnpaidBillModel;
import com.senarios.Ollaroerp.R;
import com.senarios.Ollaroerp.Utitlities.FragmentTags;

public class PaymentMethodFragment extends Fragment {

    private LinearLayout mpesa, complimentry;
    private int width, height;
    private String myObject;
    private String items;
    TextView amount;
    private Order order;
    private UnpaidBillModel unpaidBillModel;
    private Order unpaidOrder;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_payment_method, container, false);
        initView(rootView);
        return rootView;
    }

    private void initView(View rootView) {
        mpesa = rootView.findViewById(R.id.layout_mpesa);
        complimentry = rootView.findViewById(R.id.layout_complimentry);
        amount=rootView.findViewById(R.id.tv_amount);
        setClickListeners();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        DisplayMetrics metrics = getResources().getDisplayMetrics();
        width = metrics.widthPixels;
        height = metrics.heightPixels;
    }

    private void setClickListeners() {
        mpesa.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FragmentTransaction ft = getFragmentManager().beginTransaction();
                Fragment prev = getFragmentManager().findFragmentByTag(FragmentTags.MPESADIALOGUE);
                if (prev != null) {
                    ft.remove(prev);
                }
                ft.addToBackStack(null);
                MpesaDialogueFragment mpesaDialogueFragment = new MpesaDialogueFragment();
                Bundle args = new Bundle();
                if (order != null) {
                    args.putString("myObject", myObject);
                    args.putString("list", items);
                } else if (unpaidBillModel != null) {
                    args.putString("myBill", myObject);
                } else if (unpaidOrder != null) {
                    args.putString("myOrder", myObject);
                }
                mpesaDialogueFragment.setArguments(args);
                DialogFragment dialogFragment = mpesaDialogueFragment;
                dialogFragment.show(ft, FragmentTags.MPESADIALOGUE);
                dialogFragment.setCancelable(true);

            }
        });
        complimentry.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FragmentTransaction ft = getFragmentManager().beginTransaction();
                Fragment prev = getFragmentManager().findFragmentByTag(FragmentTags.COMPLIMENTRYDIALOGUE);
                if (prev != null) {
                    ft.remove(prev);
                }
                ft.addToBackStack(null);
                DialogFragment dialogFragment = new ComplimentryDialogueFragment();
                dialogFragment.show(ft, FragmentTags.COMPLIMENTRYDIALOGUE);

                dialogFragment.setCancelable(true);
            }
        });

    }

    @Override
    public void onStart() {
        super.onStart();
        Bundle args = getArguments();
        if (args != null) {
            if(args.getString("myObject")!=null) {
                myObject = args.getString("myObject");
                items = args.getString("list");
                order = new Gson().fromJson(myObject, Order.class);
                amount.setText("Pay "+order.getTotal_amount());
            }
            else if(args.getString("myBill")!=null) {
                myObject = args.getString("myBill");
                unpaidBillModel = new Gson().fromJson(myObject, UnpaidBillModel.class);
                amount.setText("Pay KSH "+unpaidBillModel.gettotal_amount());

            }
            else if(args.getString("myOrder")!=null) {
                myObject = args.getString("myOrder");
                unpaidOrder = new Gson().fromJson(myObject, Order.class);
                amount.setText("Pay KSH "+unpaidOrder.getTotal_amount());
            }


        }
    }

}
