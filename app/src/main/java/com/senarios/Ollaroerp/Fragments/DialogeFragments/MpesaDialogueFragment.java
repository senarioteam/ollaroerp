package com.senarios.Ollaroerp.Fragments.DialogeFragments;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.activeandroid.query.Delete;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.kaopiz.kprogresshud.KProgressHUD;
import com.senarios.Ollaroerp.Fragments.HomeFragment;
import com.senarios.Ollaroerp.Fragments.OrderAcceptedFragment;
import com.senarios.Ollaroerp.MainActivity;
import com.senarios.Ollaroerp.Models.CartItemModel;
import com.senarios.Ollaroerp.Models.Order;
import com.senarios.Ollaroerp.Models.OrderResponse;
import com.senarios.Ollaroerp.Models.UnpaidBillModel;
import com.senarios.Ollaroerp.R;
import com.senarios.Ollaroerp.Utitlities.FragmentTags;
import com.senarios.Ollaroerp.Utitlities.PrefUtils;
import com.senarios.Ollaroerp.Utitlities.Retrofit.GetDataService;
import com.senarios.Ollaroerp.Utitlities.Retrofit.RetrofitClientInstance;

import retrofit2.Call;
import retrofit2.Callback;


public class MpesaDialogueFragment extends DialogFragment {


    public static MpesaDialogueFragment mpesaDialogueFragment;
    private ImageView cross;
    private EditText number;

    private Button done;
    static KProgressHUD pd, pd2;
    private String myObject;
    private String items;
    private Order order;
    private UnpaidBillModel unpaidBillModel;
    private Order unpaidOrder;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.dialogue_mpesa, container, false);
        cross = v.findViewById(R.id.cross);
        mpesaDialogueFragment = this;
        cross.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
            }
        });
        done = v.findViewById(R.id.btn_done);
        number = v.findViewById(R.id.et_number);
        done.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (number.getText().toString().equalsIgnoreCase("")) {
                    number.setError("Please enter number to proceed");
                } else {
                    pd.show();
                    if (order != null) {
                        String amount=order.getTotal_amount();
                        Float flAmount=Float.parseFloat(amount);
                        Integer intAmount=Math.round(flAmount);
                        amount=Integer.toString(intAmount);
                        checkPayment(number.getText().toString(),amount);
                    } else if (unpaidBillModel != null) {
                        String amount=unpaidBillModel.gettotal_amount();
                        Float flAmount=Float.parseFloat(amount);
                        Integer intAmount=Math.round(flAmount);
                        amount=Integer.toString(intAmount);
                        payBill(number.getText().toString(), String.valueOf(unpaidBillModel.getId()),amount);
                    } else if (unpaidOrder != null) {

                    }
                }
            }
        });
        return v;
    }

    public void onBackPressed() {
        dismiss();
    }

    public void checkPayment(String number,String amount) {
        GetDataService service = RetrofitClientInstance.getRetrofitInstance().create(GetDataService.class);
        Call<JsonObject> call = service.prepaidOrder(number, FirebaseInstanceId.getInstance().getToken(), Integer.toString(PrefUtils.getUserModel(getActivity()).getId()),amount);
        call.enqueue(new Callback<JsonObject>() {

            @Override
            public void onResponse(Call<JsonObject> call, retrofit2.Response<JsonObject> response) {
                if (response.body() != null) {
                    JsonObject jsonObject = response.body();
                    try {
                        String code = jsonObject.get("ResponseCode").getAsString();
                        if (code.equalsIgnoreCase("0")) {
                            pd.dismiss();
                            PrefUtils.putString(getActivity(), "mid", jsonObject.get("MerchantRequestID").getAsString());
                            PrefUtils.putString(getActivity(), "FCM", FirebaseInstanceId.getInstance().getToken());
                            pd2.show();
                        }
                    } catch (Exception e) {

                        pd.dismiss();
                        Toast.makeText(getActivity(), jsonObject.get("errorMessage").getAsString(), Toast.LENGTH_SHORT).show();
                    }

                } else {
                    Toast.makeText(getActivity(), "Error processing bill!", Toast.LENGTH_SHORT).show();
                }
                pd.dismiss();
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                pd.dismiss();
                Toast.makeText(MainActivity.activity, "Something went wrong...Please try later!", Toast.LENGTH_SHORT).show();
            }
        });
    }

    public void payBill(String number, String billNumber,String amount) {

        GetDataService service = RetrofitClientInstance.getRetrofitInstance().create(GetDataService.class);
        Call<JsonObject> call = service.payBill(number, billNumber,amount, FirebaseInstanceId.getInstance().getToken(), Integer.toString(PrefUtils.getUserModel(getActivity()).getId()));
        call.enqueue(new Callback<JsonObject>() {

            @Override
            public void onResponse(Call<JsonObject> call, retrofit2.Response<JsonObject> response) {
                if (response.body() != null) {
                    JsonObject jsonObject = response.body();
                    try {
                        String code = jsonObject.get("ResponseCode").getAsString();
                        if (code.equalsIgnoreCase("0")) {
                            pd.dismiss();
                            PrefUtils.putString(getActivity(), "mid", jsonObject.get("MerchantRequestID").getAsString());
                            PrefUtils.putString(getActivity(), "FCM", FirebaseInstanceId.getInstance().getToken());
                            pd2.show();
                        }
                    } catch (Exception e) {

                        pd.dismiss();
                        Toast.makeText(getActivity(), jsonObject.get("errorMessage").getAsString(), Toast.LENGTH_SHORT).show();
                    }

                } else {
                    Toast.makeText(getActivity(), "Invalid m-pesa number", Toast.LENGTH_SHORT).show();
                }
                pd.dismiss();
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                pd.dismiss();
                Toast.makeText(MainActivity.activity, "Something went wrong...Please try later!", Toast.LENGTH_SHORT).show();
            }
        });
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        pd = pd.create(getActivity())
                .setStyle(KProgressHUD.Style.SPIN_INDETERMINATE)
                .setLabel("Please Wait")
                .setCancellable(false)
                .setAnimationSpeed(2)
                .setDimAmount(0.5f);
        pd2 = pd2.create(getActivity())
                .setStyle(KProgressHUD.Style.SPIN_INDETERMINATE)
                .setLabel("Waiting for customer's response")
                .setCancellable(false)
                .setAnimationSpeed(2)
                .setDimAmount(0.5f);


    }

    @Override
    public void onPause() {
        super.onPause();
        dismiss();
    }

    @Override
    public void onStart() {
        super.onStart();
        Bundle args = getArguments();
        if (args != null) {

            if (args.getString("myObject") != null) {
                myObject = args.getString("myObject");
                items = args.getString("list");
                order = new Gson().fromJson(myObject, Order.class);
                String amount=order.getTotal_amount();
                amount=amount.replace("KSH ","");
                order.setTotal_amount(amount);
            } else if (args.getString("myBill") != null) {
                myObject = args.getString("myBill");
                unpaidBillModel = new Gson().fromJson(myObject, UnpaidBillModel.class);

            } else if (args.getString("myOrder") != null) {
                myObject = args.getString("myOrder");
                unpaidOrder = new Gson().fromJson(myObject, Order.class);
            }


        }
    }

    public static void response(String s) {
        if (s.equalsIgnoreCase("1")) {


            if (MpesaDialogueFragment.mpesaDialogueFragment.unpaidBillModel != null) {
                MainActivity.activity.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        pd.show();
                        GetDataService service = RetrofitClientInstance.getRetrofitInstance().create(GetDataService.class);
                        Call<JsonObject> call = service.updateBillStatus(String.valueOf(MpesaDialogueFragment.mpesaDialogueFragment.unpaidBillModel.getId()));
                        call.enqueue(new Callback<JsonObject>() {

                            @Override
                            public void onResponse(Call<JsonObject> call, retrofit2.Response<JsonObject> response) {
                                if (response.body() != null) {
                                    JsonObject jsonObject = response.body();
                                    String code = jsonObject.get("code").getAsString();
                                    if (code.equalsIgnoreCase("200")) {
                                        Toast.makeText(MainActivity.activity, "Bill Paid Successfully", Toast.LENGTH_SHORT).show();
                                        MainActivity.activity.changeFragment(R.id.fragment_container, new HomeFragment(), FragmentTags.HOME);
                                        MpesaDialogueFragment.mpesaDialogueFragment.dismiss();
                                    } else {
                                        Toast.makeText(MainActivity.activity, "Please try again later", Toast.LENGTH_SHORT).show();
                                    }


                                } else

                                {

                                    Toast.makeText(MainActivity.activity, "Invalid m-pesa number", Toast.LENGTH_SHORT).show();
                                }
                                pd.dismiss();
                            }

                            @Override
                            public void onFailure(Call<JsonObject> call, Throwable t) {
                                pd.dismiss();
                                Toast.makeText(MainActivity.activity, "Something went wrong...Please try later!", Toast.LENGTH_SHORT).show();
                            }
                        });
                    }
                });

            }




            else if(MpesaDialogueFragment.mpesaDialogueFragment.order!=null){
                MainActivity.activity.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        pd.show();
                        GetDataService service = RetrofitClientInstance.getRetrofitInstance().create(GetDataService.class);
                        Call<OrderResponse> call = service.generatePrePaidOrders(String.valueOf(PrefUtils.getUserModel(MainActivity.activity).getId()),MpesaDialogueFragment.mpesaDialogueFragment.order.getDate_time(), MpesaDialogueFragment.mpesaDialogueFragment.order.getTable_no(), MpesaDialogueFragment.mpesaDialogueFragment.order.getNo_of_guests(), MpesaDialogueFragment.mpesaDialogueFragment.order.getItem_description(), MpesaDialogueFragment.mpesaDialogueFragment.order.getCondiments(), MpesaDialogueFragment.mpesaDialogueFragment.order.getWaiter(), MpesaDialogueFragment.mpesaDialogueFragment.order.getRestaurant(), MpesaDialogueFragment.mpesaDialogueFragment.order.getTotal_amount(), MpesaDialogueFragment.mpesaDialogueFragment.items, MpesaDialogueFragment.mpesaDialogueFragment.order.getSection());
                        call.enqueue(new Callback<OrderResponse>() {

                            @Override
                            public void onResponse(Call<OrderResponse> call, retrofit2.Response<OrderResponse> response) {
                                if (response.body() != null) {
                                    MpesaDialogueFragment.mpesaDialogueFragment.dismiss();
                                    int id = response.body().getOrderId();
                                    OrderAcceptedFragment orderAcceptedFragment = new OrderAcceptedFragment();
                                    Bundle bundle = new Bundle();
                                    bundle.putString("order_id", Integer.toString(id));
                                    new Delete().from(CartItemModel.class).execute();
                                    MainActivity.activity.updateItemCount();
                                    orderAcceptedFragment.setArguments(bundle);
                                    MainActivity.activity.changeFragment(R.id.fragment_container, orderAcceptedFragment, FragmentTags.ORDERACCEPTED);
                                } else {
                                    Toast.makeText(MainActivity.activity, "Error generating order! Please try again later", Toast.LENGTH_SHORT).show();
                                }
                                pd.dismiss();
                            }

                            @Override
                            public void onFailure(Call<OrderResponse> call, Throwable t) {
                                pd.dismiss();
                                Toast.makeText(MainActivity.activity, "Something went wrong...Please try later!", Toast.LENGTH_SHORT).show();
                            }
                        });
                    }
                });
            }
        }


        else if (s.equalsIgnoreCase("0"))

        {
            MainActivity.activity.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    Toast.makeText(MainActivity.activity, "Error Occured While Processing Payment", Toast.LENGTH_SHORT).show();
                }
            });

        }
        pd2.dismiss();

    }
}
