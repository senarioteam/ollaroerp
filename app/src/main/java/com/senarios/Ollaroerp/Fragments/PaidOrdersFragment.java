package com.senarios.Ollaroerp.Fragments;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.kaopiz.kprogresshud.KProgressHUD;
import com.senarios.Ollaroerp.Adapters.PaidOrdersAdapter;
import com.senarios.Ollaroerp.Adapters.PaidPostOrderAdapter;
import com.senarios.Ollaroerp.Fragments.DialogeFragments.PaidOrderSelectionDialogueFragment;
import com.senarios.Ollaroerp.MainActivity;
import com.senarios.Ollaroerp.Models.BasePaidOrder;
import com.senarios.Ollaroerp.Models.BasePostPaidOrder;
import com.senarios.Ollaroerp.Models.PaidOrder;
import com.senarios.Ollaroerp.Models.PostOrderItems;
import com.senarios.Ollaroerp.R;
import com.senarios.Ollaroerp.Utitlities.ChangeOrderStatus;
import com.senarios.Ollaroerp.Utitlities.FragmentTags;
import com.senarios.Ollaroerp.Utitlities.Retrofit.GetDataService;
import com.senarios.Ollaroerp.Utitlities.Retrofit.RetrofitClientInstance;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;

public class PaidOrdersFragment extends Fragment implements ChangeOrderStatus {

    private RecyclerView paidOrdersRecyclerView;
    private List<PaidOrder> itemModels = new ArrayList<>();
    private List<PostOrderItems> itemModels2 = new ArrayList<>();
    TextView orderId, orderTime, total;
    private LinearLayout switchLayout;
    private TextView title;
    boolean check = false;
    KProgressHUD pd;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        View rootView = inflater.inflate(R.layout.paid_orders_layout, container, false);
        pd = pd.create(getActivity())
                .setStyle(KProgressHUD.Style.SPIN_INDETERMINATE)
                .setLabel("Please Wait")
                .setCancellable(false)
                .setAnimationSpeed(2)
                .setDimAmount(0.5f);
        paidOrdersRecyclerView = rootView.findViewById(R.id.rv_paid_orders);
        title=rootView.findViewById(R.id.tv_title);
        switchLayout=rootView.findViewById(R.id.switch_layout);
        switchLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FragmentTransaction ft = getFragmentManager().beginTransaction();
                Fragment prev = getFragmentManager().findFragmentByTag(FragmentTags.SELECTORDERDIALOGUE);
                if (prev != null) {
                    ft.remove(prev);
                }
                ft.addToBackStack(null);
                DialogFragment dialogFragment = new PaidOrderSelectionDialogueFragment(PaidOrdersFragment.this,check);
                dialogFragment.show(ft, FragmentTags.SELECTORDERDIALOGUE);
                dialogFragment.setCancelable(true);

            }
        });
        return rootView;
    }


    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        itemModels = new ArrayList<>();
        getOrders();
    }

    public void getOrders() {
        pd.show();
        GetDataService service = RetrofitClientInstance.getRetrofitInstance().create(GetDataService.class);
        Call<BasePaidOrder> call = service.getPaidOrders();
        call.enqueue(new Callback<BasePaidOrder>() {

            @Override
            public void onResponse(Call<BasePaidOrder> call, retrofit2.Response<BasePaidOrder> response) {
                if (response.body() != null) {
                    if (response.body().getPaidOrders().size() > 0) {
                        itemModels = response.body().getPaidOrders();
                        paidOrdersRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
                        paidOrdersRecyclerView.setAdapter(new PaidOrdersAdapter(getActivity(), itemModels));
                    }
                    getPostOrders();
                } else {
                    Toast.makeText(getActivity(), "Some Error Occured! Please Try Again", Toast.LENGTH_SHORT).show();
                }
                pd.dismiss();
            }

            @Override
            public void onFailure(Call<BasePaidOrder> call, Throwable t) {
                pd.dismiss();
                Toast.makeText(MainActivity.activity, "Something went wrong...Please try later!", Toast.LENGTH_SHORT).show();
            }
        });


    }

    public void getPostOrders() {
        pd.show();
        GetDataService service = RetrofitClientInstance.getRetrofitInstance().create(GetDataService.class);
        Call<BasePostPaidOrder> call = service.getPostPaidOrders();
        call.enqueue(new Callback<BasePostPaidOrder>() {

            @Override
            public void onResponse(Call<BasePostPaidOrder> call, retrofit2.Response<BasePostPaidOrder> response) {
                if (response.body() != null) {
                    if (response.body().getPaidOrders().size() > 0) {
                        itemModels2 = response.body().getPaidOrders();
                    }

                } else {
                    Toast.makeText(getActivity(), "Some Error Occured! Please Try Again", Toast.LENGTH_SHORT).show();
                }
                pd.dismiss();
            }

            @Override
            public void onFailure(Call<BasePostPaidOrder> call, Throwable t) {
                pd.dismiss();
           //     Toast.makeText(MainActivity.activity, "Something went wrong...Please try later!", Toast.LENGTH_SHORT).show();
            }
        });


    }


    @Override
    public void changeStatus(int id) {

    }

    @Override
    public void listType(int i) {
        if (i == 0) {
            check = false;
            title.setText("Prepaid Orders");
            paidOrdersRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
            paidOrdersRecyclerView.setAdapter(new PaidOrdersAdapter(getActivity(), itemModels));
        } else if (i == 1) {
            check = true;
            title.setText("Postpaid Orders");
            paidOrdersRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
            paidOrdersRecyclerView.setAdapter(new PaidPostOrderAdapter(getActivity(), itemModels2));

        }
    }
}
