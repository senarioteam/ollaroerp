package com.senarios.Ollaroerp.Fragments;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.kaopiz.kprogresshud.KProgressHUD;
import com.senarios.Ollaroerp.Adapters.AlcoholicGroupAdapter;
import com.senarios.Ollaroerp.MainActivity;
import com.senarios.Ollaroerp.Models.AlchoholicGroup;
import com.senarios.Ollaroerp.Models.BaseAlchoholicGroup;
import com.senarios.Ollaroerp.Models.OfflineAlchoholicGroup;
import com.senarios.Ollaroerp.R;
import com.senarios.Ollaroerp.Utitlities.Retrofit.GetDataService;
import com.senarios.Ollaroerp.Utitlities.Retrofit.RetrofitClientInstance;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;

public class AlcoholicGroupFragment extends Fragment {
    private RecyclerView subcatMenuRecyclerView;
    private AlcoholicGroupAdapter alcoholicGroupAdapter;
    private ArrayList<AlchoholicGroup> alchoholicMenus;
    KProgressHUD pd;
    String sub_major_id;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        View rootView = inflater.inflate(R.layout.fragment_subcategory_menu, container, false);
        initView(rootView);
        return rootView;
    }

    private void initView(View rootView) {
        subcatMenuRecyclerView = rootView.findViewById(R.id.rv_subcat_menu);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        pd = pd.create(getActivity())
                .setStyle(KProgressHUD.Style.SPIN_INDETERMINATE)
                .setLabel("Please Wait")
                .setCancellable(false)
                .setAnimationSpeed(2)
                .setDimAmount(0.5f);


    }

    @Override
    public void onStart() {
        super.onStart();
        Bundle args = getArguments();
        if (args != null) {
            sub_major_id = args.getString("group_id");
            alchoholicMenus = new ArrayList<>();
           /* if(isNetworkAvailable()) {
                pd.show();
                callAlchoholicGroup();
            }else{*/
            if (MainActivity.getAllAlcoGroups(sub_major_id).size() != 0) {
                alchoholicMenus.clear();
                List<OfflineAlchoholicGroup> alchoholicGroups;
                alchoholicGroups = MainActivity.getAllAlcoGroups(sub_major_id);
                for (int i = 0; i < alchoholicGroups.size(); i++) {
                    AlchoholicGroup majorGroup = new AlchoholicGroup();
                    majorGroup.setId(alchoholicGroups.get(i).getAlco_group_id());
                    majorGroup.seta_image(alchoholicGroups.get(i).getA_image());
                    majorGroup.setName(alchoholicGroups.get(i).getName());
                    majorGroup.setcreated_at(alchoholicGroups.get(i).getCreated_at());
                    majorGroup.setupdated_at(alchoholicGroups.get(i).getUpdated_at());
                    majorGroup.setMenuItemsGroupId(alchoholicGroups.get(i).getMenuItemsGroupId());
                    alchoholicMenus.add(majorGroup);
                }

                subcatMenuRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
                alcoholicGroupAdapter = new AlcoholicGroupAdapter(getActivity(), alchoholicMenus);
                subcatMenuRecyclerView.setAdapter(alcoholicGroupAdapter);
            } else {
                if (isNetworkAvailable()) {
                    pd.show();
                    callAlchoholicGroup();
                } else {
                    Toast.makeText(getActivity(), "No items in the list! Try synchronizing again for updating items", Toast.LENGTH_LONG).show();
                }
            }

        }
    }

    public void callAlchoholicGroup() {
        GetDataService service = RetrofitClientInstance.getRetrofitInstance().create(GetDataService.class);
        Call<BaseAlchoholicGroup> call = service.getAlcoholicGroups(sub_major_id);
        call.enqueue(new Callback<BaseAlchoholicGroup>() {

            @Override
            public void onResponse(Call<BaseAlchoholicGroup> call, retrofit2.Response<BaseAlchoholicGroup> response) {
                if (response.body() != null) {
                    for (int i = 0; i < response.body().getAlcoholGroups().size(); i++) {
                        alchoholicMenus.add(response.body().getAlcoholGroups().get(i));
                        OfflineAlchoholicGroup offlineAlchoholicGroup = new OfflineAlchoholicGroup(response.body().getAlcoholGroups().get(i));
                        if (MainActivity.getAlcoGroupRow(response.body().getAlcoholGroups().get(i)) == null) {
                            offlineAlchoholicGroup.save();
                        }
                    }
                    subcatMenuRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
                    alcoholicGroupAdapter = new AlcoholicGroupAdapter(getActivity(), alchoholicMenus);
                    subcatMenuRecyclerView.setAdapter(alcoholicGroupAdapter);
                } else {
                    Toast.makeText(getActivity(), "Error fetching list! Please try again later", Toast.LENGTH_SHORT).show();
                }
                pd.dismiss();
            }

            @Override
            public void onFailure(Call<BaseAlchoholicGroup> call, Throwable t) {
                pd.dismiss();
                Toast.makeText(MainActivity.activity, "Something went wrong...Please try later!", Toast.LENGTH_SHORT).show();
            }
        });


    }

    private boolean isNetworkAvailable() {
        ConnectivityManager connectivityManager
                = (ConnectivityManager) getActivity().getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }
}
