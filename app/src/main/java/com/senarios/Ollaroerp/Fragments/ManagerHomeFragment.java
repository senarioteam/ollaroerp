package com.senarios.Ollaroerp.Fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.google.gson.Gson;
import com.senarios.Ollaroerp.Adapters.NotificationRecyclerAdapter;
import com.senarios.Ollaroerp.MainActivity;
import com.senarios.Ollaroerp.Models.NotificationModel;
import com.senarios.Ollaroerp.R;
import com.senarios.Ollaroerp.Utitlities.TinyDB;

import java.util.ArrayList;


public class ManagerHomeFragment extends Fragment {

    private RecyclerView notifRecyclerView;
    private TextView noRequests;
    private SwipeRefreshLayout swipeRefreshLayout, swipeRefreshLayout2;
    private TinyDB tinyDB;
    ArrayList<NotificationModel> notificationModels = new ArrayList<>();


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        MainActivity.activity.getSupportActionBar().hide();
        View rootView = inflater.inflate(R.layout.fragment_manager_home, container, false);
        notifRecyclerView = rootView.findViewById(R.id.rv_notifications);
        notifRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        noRequests = rootView.findViewById(R.id.tv_no_requests);
        swipeRefreshLayout = rootView.findViewById(R.id.pullToRefresh);
        swipeRefreshLayout2 = rootView.findViewById(R.id.pullToRefresh2);
        swipeRefreshLayout2.setVisibility(View.VISIBLE);
        tinyDB = new TinyDB(getActivity());

        if (tinyDB.getListString("notifications").size() > 0) {
            ArrayList<String> array = tinyDB.getListString("notifications");
            Gson gson = new Gson();
            for (int i = 0; i < array.size(); i++) {
                notificationModels.add(gson.fromJson(array.get(i), NotificationModel.class));
            }
            notifRecyclerView.setAdapter(new NotificationRecyclerAdapter(getActivity(), notificationModels));
            noRequests.setVisibility(View.GONE);
            swipeRefreshLayout2.setVisibility(View.GONE);
        } else {
            noRequests.setVisibility(View.VISIBLE);
            swipeRefreshLayout.setVisibility(View.GONE);
            notifRecyclerView.setVisibility(View.GONE);
        }
        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                if (tinyDB.getListString("notifications").size() > 0) {
                    ArrayList<String> array = tinyDB.getListString("notifications");
                    Gson gson = new Gson();
                    notificationModels.clear();
                    for (int i = 0; i < array.size(); i++) {
                        notificationModels.add(gson.fromJson(array.get(i), NotificationModel.class));
                    }
                    notifRecyclerView.setAdapter(new NotificationRecyclerAdapter(getActivity(), notificationModels));
                    noRequests.setVisibility(View.GONE);
                    swipeRefreshLayout2.setVisibility(View.GONE);
                    notifRecyclerView.setVisibility(View.VISIBLE);
                    swipeRefreshLayout.setVisibility(View.VISIBLE);
                } else {
                    noRequests.setVisibility(View.VISIBLE);
                    swipeRefreshLayout2.setVisibility(View.VISIBLE);

                    notifRecyclerView.setVisibility(View.GONE);
                    swipeRefreshLayout.setVisibility(View.GONE);
                }
                swipeRefreshLayout.setRefreshing(false);
            }
        });
        swipeRefreshLayout2.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                if (tinyDB.getListObject("notifications", NotificationModel.class).size() > 0) {
                    ArrayList<String> array = tinyDB.getListString("notifications");
                    notificationModels.clear();
                    Gson gson = new Gson();
                    for (int i = 0; i < array.size(); i++) {
                        notificationModels.add(gson.fromJson(array.get(i), NotificationModel.class));
                    }
                    notifRecyclerView.setAdapter(new NotificationRecyclerAdapter(getActivity(), notificationModels));
                    noRequests.setVisibility(View.GONE);
                    swipeRefreshLayout2.setVisibility(View.GONE);
                    notifRecyclerView.setVisibility(View.VISIBLE);
                    swipeRefreshLayout.setVisibility(View.VISIBLE);
                } else {
                    notifRecyclerView.setVisibility(View.GONE);
                    swipeRefreshLayout.setVisibility(View.GONE);
                    swipeRefreshLayout2.setVisibility(View.VISIBLE);
                    noRequests.setVisibility(View.VISIBLE);
                }
                swipeRefreshLayout2.setRefreshing(false);
            }
        });
        return rootView;
    }


}
