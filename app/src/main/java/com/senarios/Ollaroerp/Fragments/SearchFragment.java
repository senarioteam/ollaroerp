package com.senarios.Ollaroerp.Fragments;


import android.app.Activity;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.kaopiz.kprogresshud.KProgressHUD;
import com.senarios.Ollaroerp.Adapters.SubcatMenuItemAdapter;
import com.senarios.Ollaroerp.MainActivity;
import com.senarios.Ollaroerp.Models.BaseItemModel;
import com.senarios.Ollaroerp.Models.ItemModel;
import com.senarios.Ollaroerp.R;
import com.senarios.Ollaroerp.Utitlities.Retrofit.GetDataService;
import com.senarios.Ollaroerp.Utitlities.Retrofit.RetrofitClientInstance;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;

/**
 * A simple {@link Fragment} subclass.
 */
public class SearchFragment extends Fragment {

    private EditText search;
    private RecyclerView searchRV;
    KProgressHUD pd;
    SubcatMenuItemAdapter subcatMenuItemAdapter;
    private ArrayList<ItemModel> menuItemModels;


    public SearchFragment() {

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        final View rootView = inflater.inflate(R.layout.fragment_search, container, false);
        search = rootView.findViewById(R.id.et_search);
        search.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                    InputMethodManager imm= (InputMethodManager) getActivity().getSystemService(Activity.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(rootView.getWindowToken(),0);
                    search(v.getText().toString());
                }
                return true;
            }
        });
        searchRV=rootView.findViewById(R.id.rv_search);
        return rootView;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        pd = pd.create(getActivity())
                .setStyle(KProgressHUD.Style.SPIN_INDETERMINATE)
                .setLabel("Please Wait")
                .setCancellable(false)
                .setAnimationSpeed(2)
                .setDimAmount(0.5f);
    }

    public void search(String searchable) {
        pd.show();
        menuItemModels=new ArrayList<>();
        GetDataService service = RetrofitClientInstance.getRetrofitInstance().create(GetDataService.class);
        Call<BaseItemModel> call = service.search(searchable);
        call.enqueue(new Callback<BaseItemModel>() {

            @Override
            public void onResponse(Call<BaseItemModel> call, retrofit2.Response<BaseItemModel> response) {
                if (response.body()!=null) {
                    for (int i = 0; i < response.body().getItems().size(); i++) {
                        menuItemModels.add(response.body().getItems().get(i));

                    }
                    subcatMenuItemAdapter=new SubcatMenuItemAdapter(getActivity(),menuItemModels);
                    searchRV.setLayoutManager(new LinearLayoutManager(getActivity()));
                    searchRV.setAdapter(subcatMenuItemAdapter);
                }
                else {
                    Toast.makeText(getActivity(), "Error fetching list! Please try again later", Toast.LENGTH_SHORT).show();
                }
                pd.dismiss();
            }

            @Override
            public void onFailure(Call<BaseItemModel> call, Throwable t) {
                pd.dismiss();
                Toast.makeText(MainActivity.activity, "Something went wrong...Please try later!", Toast.LENGTH_SHORT).show();
            }
        });


    }
}
