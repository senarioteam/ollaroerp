package com.senarios.Ollaroerp.Fragments;


import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.kaopiz.kprogresshud.KProgressHUD;
import com.senarios.Ollaroerp.Adapters.FullOrderAdapter;
import com.senarios.Ollaroerp.Adapters.PendingOrdersAdapter;
import com.senarios.Ollaroerp.Fragments.DialogeFragments.OrderSelectionDialogFragment;
import com.senarios.Ollaroerp.MainActivity;
import com.senarios.Ollaroerp.Models.BaseInProgressOrders;
import com.senarios.Ollaroerp.Models.InProgressOrders;
import com.senarios.Ollaroerp.Models.ItemModel;
import com.senarios.Ollaroerp.Models.OfflineOrders;
import com.senarios.Ollaroerp.Models.OfflinePostOrderItems;
import com.senarios.Ollaroerp.R;
import com.senarios.Ollaroerp.Utitlities.ChangeOrderStatus;
import com.senarios.Ollaroerp.Utitlities.FragmentTags;
import com.senarios.Ollaroerp.Utitlities.PrefUtils;
import com.senarios.Ollaroerp.Utitlities.Retrofit.GetDataService;
import com.senarios.Ollaroerp.Utitlities.Retrofit.RetrofitClientInstance;
import com.senarios.Ollaroerp.Utitlities.TinyDB;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;

public class InprogressOrdersFragment extends Fragment implements ChangeOrderStatus {

    private static RecyclerView allOrdersRecyclerView;
    private static ArrayList<InProgressOrders> orderModels;
    private static ArrayList<InProgressOrders> compOrderModels;
    private ArrayList<ItemModel> itemModels;
    private static  KProgressHUD pd;
    int check=0;
    private LinearLayout switchLayout;
    private TextView title;

    public InprogressOrdersFragment() {

    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View rootView = inflater.inflate(R.layout.fragment_inprogress_orders, container, false);
        initView(rootView);
        return rootView;
    }

    public void initView(View rootView) {
        itemModels=new ArrayList<>();
        title=rootView.findViewById(R.id.tv_title);
        orderModels=new ArrayList<>();
        compOrderModels=new ArrayList<>();
        allOrdersRecyclerView=rootView.findViewById(R.id.rv_inprogress_orders);
        switchLayout=rootView.findViewById(R.id.switch_layout);
        switchLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FragmentTransaction ft = getFragmentManager().beginTransaction();
                Fragment prev = getFragmentManager().findFragmentByTag(FragmentTags.SELECTORDERDIALOGUE);
                if (prev != null) {
                    ft.remove(prev);
                }
                ft.addToBackStack(null);
                DialogFragment dialogFragment = new OrderSelectionDialogFragment(InprogressOrdersFragment.this,check);
                dialogFragment.show(ft, FragmentTags.SELECTORDERDIALOGUE);
                dialogFragment.setCancelable(true);

            }
        });
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        MainActivity.activity.getSupportActionBar().setTitle("Inprogress Orders");
        pd = pd.create(getActivity())
                .setStyle(KProgressHUD.Style.SPIN_INDETERMINATE)
                .setLabel("Just a moment")
                .setCancellable(false)
                .setAnimationSpeed(2)
                .setDimAmount(0.5f);
        pd.show();

        if (isNetworkAvailable()){
            getOrders();
        }
       else{
            List<OfflinePostOrderItems> offlinePostOrderItems=MainActivity.getAllPostOrderItems();
           for (int i=0;i<offlinePostOrderItems.size();i++){
               InProgressOrders inProgressOrders= new InProgressOrders();
               inProgressOrders.setWaiter(offlinePostOrderItems.get(i).getWaiter());
               inProgressOrders.setDateTime(offlinePostOrderItems.get(i).getDate_time());
               inProgressOrders.setId(MainActivity.getAllPostOrderItems().get(i).getPostorderid());
               inProgressOrders.setitem_description(offlinePostOrderItems.get(i).getItem_description());
               inProgressOrders.setRestaurant(offlinePostOrderItems.get(i).getRestaurant());
               inProgressOrders.setStatus(offlinePostOrderItems.get(i).getStatus());
               inProgressOrders.setno_of_guests(offlinePostOrderItems.get(i).getNo_of_guests());
               inProgressOrders.setTableNo(offlinePostOrderItems.get(i).getTable_no());
               inProgressOrders.settotal_amount(offlinePostOrderItems.get(i).getTotal_amount());

               orderModels.add(inProgressOrders);




           }


            allOrdersRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
            allOrdersRecyclerView.setAdapter(new FullOrderAdapter(getActivity(), orderModels, InprogressOrdersFragment.this));
pd.dismiss();
        }

    }


    public  void getOrders() {
        GetDataService service = RetrofitClientInstance.getRetrofitInstance().create(GetDataService.class);
        Call<BaseInProgressOrders> call = service.getInprogressOrders(String.valueOf(PrefUtils.getUserModel(getActivity()).getId()));
        call.enqueue(new Callback<BaseInProgressOrders>() {

            @Override
            public void onResponse(Call<BaseInProgressOrders> call, retrofit2.Response<BaseInProgressOrders> response) {
                if(response.body()!=null) {
                    for (int i = 0; i < response.body().getInProgress().size(); i++) {
                        if (response.body().getInProgress().get(i).getStatus().equalsIgnoreCase("NEW ORDERS") && response.body().getInProgress().get(i).getorder_items().size()!=0) {
                                    orderModels.add(response.body().getInProgress().get(i));
                        } else if (response.body().getInProgress().get(i).getStatus().equalsIgnoreCase("Completed") && response.body().getInProgress().get(i).getorder_items().size() != 0) {
                            compOrderModels.add(response.body().getInProgress().get(i));
                        }



                    }
                    for (int i = 0; i < orderModels.size(); i++) {
                    for (int j=0;j<orderModels.get(i).getorder_items().size();j++) {
                        if (orderModels.get(i).getorder_items().get(j).getStatus()!=null){
                            if (orderModels.get(i).getorder_items().get(j).getStatus().equalsIgnoreCase("Pending")) {
                                orderModels.get(i).getorder_items().remove(j);
                            }
                        }


                    }
                    }

                    allOrdersRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
                    allOrdersRecyclerView.setAdapter(new FullOrderAdapter(getActivity(), orderModels, InprogressOrdersFragment.this));
                    try {
                        pd.dismiss();
                    } catch (Exception e) {

                    }
                }else{

                    pd.dismiss();
                }
            }

            @Override
            public void onFailure(Call<BaseInProgressOrders> call, Throwable t) {
                try {
                    pd.dismiss();
                }catch (Exception e){

                }
                Toast.makeText(MainActivity.activity, "Something went wrong...Please try later!", Toast.LENGTH_SHORT).show();
            }
        });


    }

    @Override
    public void changeStatus(int id) {
        for(int i=0;i<orderModels.size();i++){
            if(orderModels.get(i).getId()==id){

                /*orderModels.get(i).setStatus("Completed");*/
               /* compOrderModels.add(orderModels.get(i));*/
                orderModels.remove(i);
            }
            /*for (int m = 0; m < compOrderModels.size(); m++) {
                for (int j=0;j<compOrderModels.get(m).getorder_items().size();j++) {
s e3v                    if (compOrderModels.get(i).getorder_items().get(j).getStatus()!=null){
                        if (compOrderModels.get(i).getorder_items().get(j).getStatus().equalsIgnoreCase("Pending")) {
                            compOrderModels.get(i).getorder_items().remove(j);
                        }
                    }
                }
            }*/
            allOrdersRecyclerView.setLayoutManager(new LinearLayoutManager(MainActivity.activity));
            allOrdersRecyclerView.setAdapter(new FullOrderAdapter(MainActivity.activity,orderModels,this));
        }
    }

    @Override
    public void listType(int i) {
        if(i==0){
            check=1;
            title.setText("Inprogress Orders");
            allOrdersRecyclerView.setLayoutManager(new LinearLayoutManager(MainActivity.activity));
            allOrdersRecyclerView.setAdapter(new FullOrderAdapter(MainActivity.activity,orderModels,this));
        }
        else if(i==1){
            check=2;
            title.setText("Completed Orders");
            allOrdersRecyclerView.setLayoutManager(new LinearLayoutManager(MainActivity.activity));
            allOrdersRecyclerView.setAdapter(new FullOrderAdapter(MainActivity.activity,compOrderModels,this));
        }
        else if(i==2){
            check=3;
            title.setText("Pending Orders(Offline)");

            TinyDB tinyDB = new TinyDB(getActivity());
            ArrayList<OfflineOrders> allOrders=new ArrayList<>();
            ArrayList<String> offlineOrders = tinyDB.getListString("OfflineOrders");

            for (int j = 0; j < offlineOrders.size(); j++) {
                try {
                    JSONObject obj = new JSONObject(offlineOrders.get(j));
                    OfflineOrders offlineOrder = new OfflineOrders();
                    offlineOrder.setItems((String) obj.get("items"));
                    offlineOrder.setOrder((String) obj.get("order"));
                    allOrders.add(offlineOrder);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                /*OfflineOrders offlineOrder = new Gson().fromJson(offlineOrders.get(j), OfflineOrders.class);*/
            }
            allOrdersRecyclerView.setLayoutManager(new LinearLayoutManager(MainActivity.activity));
            allOrdersRecyclerView.setAdapter(new PendingOrdersAdapter(MainActivity.activity,allOrders));
        }
    }
    private boolean isNetworkAvailable() {
        ConnectivityManager connectivityManager
                = (ConnectivityManager) getActivity().getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }



}
