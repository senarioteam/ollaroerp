package com.senarios.Ollaroerp.Fragments;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.kaopiz.kprogresshud.KProgressHUD;
import com.senarios.Ollaroerp.Adapters.AlcoholicSubGroupAdapter;
import com.senarios.Ollaroerp.MainActivity;
import com.senarios.Ollaroerp.Models.BaseSubAlcoholicGroup;
import com.senarios.Ollaroerp.Models.OfflineSubAlchoholicGroup;
import com.senarios.Ollaroerp.Models.SubAlcoholicGroup;
import com.senarios.Ollaroerp.R;
import com.senarios.Ollaroerp.Utitlities.Retrofit.GetDataService;
import com.senarios.Ollaroerp.Utitlities.Retrofit.RetrofitClientInstance;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;

public class AlcoholicSubGroupFragment extends Fragment {
    private RecyclerView subcatMenuRecyclerView;
    private AlcoholicSubGroupAdapter subcatMenuAdapter;
    private ArrayList<SubAlcoholicGroup> alchoholicMenus;
    KProgressHUD pd;
    String sub_major_id;


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        View rootView=inflater.inflate(R.layout.fragment_subcategory_menu,container,false);
        initView(rootView);
        return rootView;
    }

    private void initView(View rootView){
        subcatMenuRecyclerView=rootView.findViewById(R.id.rv_subcat_menu);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        pd = pd.create(getActivity())
                .setStyle(KProgressHUD.Style.SPIN_INDETERMINATE)
                .setLabel("Please Wait")
                .setCancellable(false)
                .setAnimationSpeed(2)
                .setDimAmount(0.5f);


    }

    @Override
    public void onStart() {
        super.onStart();
        Bundle args = getArguments();
        if (args != null) {
            sub_major_id=args.getString("sub_id");
            alchoholicMenus=new ArrayList<>();
          /*  if(isNetworkAvailable()) {
                pd.show();
                callAlchoholicGroup();
            }else{*/
                if (MainActivity.getAllSubAlcoGroups(sub_major_id).size() != 0) {
                    alchoholicMenus.clear();
                    List<OfflineSubAlchoholicGroup> familyGroups;
                    familyGroups=MainActivity.getAllSubAlcoGroups(sub_major_id);
                    for (int i = 0; i <familyGroups.size(); i++) {
                        SubAlcoholicGroup majorGroup = new SubAlcoholicGroup();
                        majorGroup.setId(familyGroups.get(i).getSub_alco_id());
                        majorGroup.setAImage(familyGroups.get(i).getA_image());
                        majorGroup.setName(familyGroups.get(i).getName());
                        majorGroup.setCreatedAt(familyGroups.get(i).getCreated_at());
                        majorGroup.setUpdatedAt(familyGroups.get(i).getUpdated_at());
                        majorGroup.setFamilyGroupsId(familyGroups.get(i).getFamilyGroupsId());
                        majorGroup.setGlAccounts(familyGroups.get(i).getGl_accounts());
                        majorGroup.setDescription(familyGroups.get(i).getDescription());
                        alchoholicMenus.add(majorGroup);
                    }

                    subcatMenuRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
                    subcatMenuAdapter =new AlcoholicSubGroupAdapter(getActivity(),alchoholicMenus);
                    subcatMenuRecyclerView.setAdapter(subcatMenuAdapter);
                }
                else{
                    if (isNetworkAvailable()) {
                        pd.show();
                        callAlchoholicGroup();
                    } else {
                        Toast.makeText(getActivity(), "No items in the list! Try synchronizing again for updating items", Toast.LENGTH_LONG).show();
                    }                }

        }
    }

    public void callAlchoholicGroup() {
        GetDataService service = RetrofitClientInstance.getRetrofitInstance().create(GetDataService.class);
        Call<BaseSubAlcoholicGroup> call = service.getAlcoholicSubGroups(sub_major_id);
        call.enqueue(new Callback<BaseSubAlcoholicGroup>() {

            @Override
            public void onResponse(Call<BaseSubAlcoholicGroup> call, retrofit2.Response<BaseSubAlcoholicGroup> response) {
                if (response.body()!=null) {
                    for (int i = 0; i < response.body().getSubAlcoholicGroups().size(); i++) {
                        alchoholicMenus.add(response.body().getSubAlcoholicGroups().get(i));
                        OfflineSubAlchoholicGroup offlineAlchoholicGroup=new OfflineSubAlchoholicGroup(response.body().getSubAlcoholicGroups().get(i));
                        if(MainActivity.getSubAlcoGroupRow(response.body().getSubAlcoholicGroups().get(i))==null){
                            offlineAlchoholicGroup.save();
                        }

                    }
                    subcatMenuRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
                    subcatMenuAdapter=new AlcoholicSubGroupAdapter(getActivity(),alchoholicMenus);
                    subcatMenuRecyclerView.setAdapter(subcatMenuAdapter);

                }
                else {
                    Toast.makeText(getActivity(), "Error fetching list! Please try again later", Toast.LENGTH_SHORT).show();
                }
                pd.dismiss();
            }

            @Override
            public void onFailure(Call<BaseSubAlcoholicGroup> call, Throwable t) {
                pd.dismiss();
                Toast.makeText(MainActivity.activity, "Something went wrong...Please try later!", Toast.LENGTH_SHORT).show();
            }
        });


    }
    private boolean isNetworkAvailable() {
        ConnectivityManager connectivityManager
                = (ConnectivityManager) getActivity().getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }
}
