package com.senarios.Ollaroerp.Fragments.DialogeFragments;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.kaopiz.kprogresshud.KProgressHUD;
import com.senarios.Ollaroerp.Fragments.AddToCartFragment;
import com.senarios.Ollaroerp.MainActivity;
import com.senarios.Ollaroerp.Models.BaseTableModel;
import com.senarios.Ollaroerp.Models.OfflineTableModel;
import com.senarios.Ollaroerp.Models.TableModel;
import com.senarios.Ollaroerp.R;
import com.senarios.Ollaroerp.Utitlities.InstantAutoComplete;
import com.senarios.Ollaroerp.Utitlities.PrefUtils;
import com.senarios.Ollaroerp.Utitlities.Retrofit.GetDataService;
import com.senarios.Ollaroerp.Utitlities.Retrofit.RetrofitClientInstance;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;

public class ChooseTableDialogueFragment extends DialogFragment {
    private EditText guests;
    ArrayList<String> tablesList = new ArrayList<String>();
    private InstantAutoComplete table;
    private Button done;
    ArrayList<TableModel> tableModels=new ArrayList<>();
    String tableString, guest;
    private KProgressHUD pd;
    private String tableNo;
    private String section;
    private String tableName;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.dialogue_choose_table, container, false);
        initView(v);

        return v;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        pd = pd.create(getActivity())
                .setStyle(KProgressHUD.Style.SPIN_INDETERMINATE)
                .setLabel("Fetching Tables")
                .setCancellable(false)
                .setAnimationSpeed(2)
                .setDimAmount(0.5f);

            pd.dismiss();
            callTablesGroup();

    }

    private void initView(View v) {
        guests = v.findViewById(R.id.et_guests);
        done = v.findViewById(R.id.btn_done);
        table = v.findViewById(R.id.autoCompleteTextView);

        setClickListeners();
    }

    private void setClickListeners() {
        done.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (table.getText().toString().equalsIgnoreCase("")) {
                    table.setError("Please Select a Table");
                }  else {
                    section=null;
                    for (int i = 0; i < tableModels.size(); i++) {
                        if(tableModels.get(i).getName().equalsIgnoreCase(table.getText().toString())){
                            tableNo= String.valueOf(tableModels.get(i).getId());
                            section=tableModels.get(i).getSection();
                            tableName=tableModels.get(i).getName();
                        }
                    }
                    if(section!=null){
                        AddToCartFragment.bringVlues(tableNo,section,tableName);
                        dismiss();
                    }else{
                        Toast.makeText(getActivity(), "Please select from assigned tables!", Toast.LENGTH_SHORT).show();
                        table.setText("");
                        table.showDropDown();
                    }
                }
            }
        });
    }

    public void onBackPressed() {
        dismiss();
    }

    @Override
    public void onStart() {
        super.onStart();
        Bundle args = getArguments();
        if (args != null) {
            try {
                tableString = args.getString("table");
                guest = args.getString("guests");
                if (tableString != null) {
                    table.setText(tableString);
//                    guests.setText(guest);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }


    public void callTablesGroup() {
        pd.show();
        GetDataService service = RetrofitClientInstance.getRetrofitInstance().create(GetDataService.class);
        Call<BaseTableModel> call = service.getTables(String.valueOf(PrefUtils.getUserModel(getActivity()).getId()));
        call.enqueue(new Callback<BaseTableModel>() {

            @Override
            public void onResponse(Call<BaseTableModel> call, retrofit2.Response<BaseTableModel> response) {
                if (response.body() != null) {
                    for (int i = 0; i < response.body().getTables().size(); i++) {
                        tablesList.add(response.body().getTables().get(i).getName());
                        tableModels.add(response.body().getTables().get(i));
                        OfflineTableModel offlineTableModel=new OfflineTableModel(response.body().getTables().get(i));
                        if(MainActivity.getTableRow(response.body().getTables().get(i))==null){
                            offlineTableModel.save();
                        }
                    }
                    ArrayAdapter<String> adapter = new ArrayAdapter<String>
                            (getActivity(), android.R.layout.select_dialog_item, tablesList);
                    table.setAdapter(adapter);
                    table.setThreshold(1);//will start working from first character
                    table.setTextColor(getActivity().getResources().getColor(R.color.colorPrimary));
                    table.showDropDown();

                } else {
                    Toast.makeText(getActivity(), "Error fetching list! Please try again later", Toast.LENGTH_SHORT).show();
                }
                pd.dismiss();
            }

            @Override
            public void onFailure(Call<BaseTableModel> call, Throwable t) {


                if (MainActivity.getAllTables().size() != 0) {
                    tablesList.clear();
                    List<OfflineTableModel> offlineTableModel=MainActivity.getAllTables();
                    for (int i = 0; i < MainActivity.getAllTables().size(); i++) {
                        TableModel tableModel = new TableModel();
                        tableModel.setId(offlineTableModel.get(i).getTable_id());
                        tableModel.setbooking_status(offlineTableModel.get(i).getBooking_status());
                        tableModel.setcreated_at(offlineTableModel.get(i).getCreated_at());
                        tableModel.setupdated_at(offlineTableModel.get(i).getUpdated_at());
                        tableModel.setCapacity(offlineTableModel.get(i).getCapacity());
                        tableModel.setName(offlineTableModel.get(i).getName());
                        tableModel.setwaiter_id(offlineTableModel.get(i).getWaiter_id());
                        tableModel.setWaiter(offlineTableModel.get(i).getWaiter());
                        tableModel.setSection(offlineTableModel.get(i).getSection());
                        tableModels.add(tableModel);
                        tablesList.add(tableModel.getName());
                    }
                    ArrayAdapter<String> adapter = new ArrayAdapter<String>
                            (getActivity(), android.R.layout.select_dialog_item, tablesList);
                    table.setAdapter(adapter);
                    table.setThreshold(1);//will start working from first character
                    table.setTextColor(getActivity().getResources().getColor(R.color.colorPrimary));
                    table.showDropDown();
                    pd.dismiss();
                }
                else{
                    pd.dismiss();
                    Toast.makeText(getActivity(), "You need to connect to internet to update tables!", Toast.LENGTH_LONG).show();
                }
            }
        });


    }

    private boolean isNetworkAvailable() {
        ConnectivityManager connectivityManager
                = (ConnectivityManager) getActivity().getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }


}
