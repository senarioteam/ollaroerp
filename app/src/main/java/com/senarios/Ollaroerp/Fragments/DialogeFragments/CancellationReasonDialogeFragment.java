package com.senarios.Ollaroerp.Fragments.DialogeFragments;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.gson.JsonObject;
import com.kaopiz.kprogresshud.KProgressHUD;
import com.senarios.Ollaroerp.Adapters.FullOrderAdapter;
import com.senarios.Ollaroerp.Adapters.InprogressOrderAdapter;
import com.senarios.Ollaroerp.Adapters.OrderItemAdapter;
import com.senarios.Ollaroerp.Fragments.InprogressOrdersFragment;
import com.senarios.Ollaroerp.MainActivity;
import com.senarios.Ollaroerp.Models.InProgressOrders;
import com.senarios.Ollaroerp.Models.OrderItem;
import com.senarios.Ollaroerp.R;
import com.senarios.Ollaroerp.Utitlities.FragmentTags;
import com.senarios.Ollaroerp.Utitlities.Retrofit.GetDataService;
import com.senarios.Ollaroerp.Utitlities.Retrofit.RetrofitClientInstance;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;

public class CancellationReasonDialogeFragment extends DialogFragment {
    private EditText table;
    private Button done;
    private KProgressHUD pd;
    private String amount,orderId,waiterId,itemId,itemName,waiterName,general_item_id;
    private Integer quantity;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.dialoge_void_item, container, false);
        initView(v);

        return v;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        pd = pd.create(getActivity())
                .setStyle(KProgressHUD.Style.SPIN_INDETERMINATE)
                .setLabel("Fetching Tables")
                .setCancellable(false)
                .setAnimationSpeed(2)
                .setDimAmount(0.5f);

        pd.dismiss();


    }

    private void initView(View v) {
        done = v.findViewById(R.id.btn_done);
        table = v.findViewById(R.id.et_reason);
        setClickListeners();
    }

    private void setClickListeners() {
        done.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (table.getText().toString().equalsIgnoreCase("")) {
                    table.setError("Please provide reason");
                }  else {
                    pd.show();
                    GetDataService service = RetrofitClientInstance.getRetrofitInstance().create(GetDataService.class);
                    Call<JsonObject> call = service.voidItem(waiterId,itemId,itemName,waiterName,orderId,amount,table.getText().toString(),quantity,general_item_id);
                    call.enqueue(new Callback<JsonObject>() {

                        @Override
                        public void onResponse(Call<JsonObject> call, retrofit2.Response<JsonObject> response) {
                            if (response.code()==201) {
                               List<InProgressOrders> list= FullOrderAdapter.returndata();
                               for (int i=0;i<list.size();i++){
                                   for (int j=0;j<list.get(i).getorder_items().size();j++){
                                       if (itemId.equalsIgnoreCase(String.valueOf(list.get(i).getorder_items().get(j).getId()))){
                                           //list.get(i).getorder_items().get(j).setVoid(true);
                                           FullOrderAdapter.orderItemAdapter.notifyDataSetChanged();

                                       }
                                   }

                               }

                                Toast.makeText(getActivity(), "Item requested for removal", Toast.LENGTH_SHORT).show();
                                dismiss();
                                MainActivity.activity.changeFragment(R.id.fragment_container,new InprogressOrdersFragment(), FragmentTags.ORDERSINPROGRESS);

                            } else {
                                Toast.makeText(getActivity(), "Please try again later", Toast.LENGTH_SHORT).show();
                            }
                            pd.dismiss();
                        }

                        @Override
                        public void onFailure(Call<JsonObject> call, Throwable t) {
                            pd.dismiss();
                            Toast.makeText(MainActivity.activity, "Something went wrong...Please try later!", Toast.LENGTH_SHORT).show();
                        }
                    });
                }
            }
        });
    }

    public void onBackPressed() {
        dismiss();
    }

    @Override
    public void onStart() {
        super.onStart();
        Bundle args = getArguments();
        if (args != null) {
            try {
                itemId = args.getString("item_id");
                itemName = args.getString("item_name");
                waiterId = args.getString("waiter_id");
                orderId = args.getString("order_id");
                amount = args.getString("amount");
                waiterName = args.getString("waiter_name");
                quantity = args.getInt("quantity");
                general_item_id = args.getString("general_item_id");

            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }




}

