package com.senarios.Ollaroerp.Fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

import com.senarios.Ollaroerp.R;


public class CreateVoucherFragment extends Fragment {

    private EditText title,amount;
    private Button generateVoucher;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View rootView=inflater.inflate(R.layout.fragment_create_voucher, container, false);
        initView(rootView);
        return rootView;
    }

    private void initView(View rootView){
        title=rootView.findViewById(R.id.et_voucher_title);
        amount=rootView.findViewById(R.id.et_voucher_amount);
        generateVoucher=rootView.findViewById(R.id.btn_generate_voucher);
        setClickListeners();
    }

    private void setClickListeners(){
        generateVoucher.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(title.getText().toString().equalsIgnoreCase("")){
                    title.setError("Please Enter Title");
                }
                else if(amount.getText().toString().equalsIgnoreCase("")){
                    amount.setError("Please Enter amount");
                }
                else {

                }
            }
        });
    }

}
