package com.senarios.Ollaroerp.Fragments;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.senarios.Ollaroerp.MainActivity;
import com.senarios.Ollaroerp.R;
import com.senarios.Ollaroerp.Utitlities.FragmentTags;

public class OrderAcceptedFragment extends Fragment {
    private TextView orderId;
    private Button done;
    private String myObject;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView= inflater.inflate(R.layout.fragment_order_accepted, container, false);
        initView(rootView);
        return rootView;
    }

    private void initView(View rootView){
        orderId=rootView.findViewById(R.id.tv_order_id);
        done=rootView.findViewById(R.id.btn_done);
        setClickListeners();
    }

    private void setClickListeners(){
        done.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MainActivity.activity.changeFragment(R.id.fragment_container,new HomeFragment(),FragmentTags.HOME);
            }
        });
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
    }

    @Override
    public void onStart() {
        super.onStart();
        Bundle args = getArguments();
        if (args != null) {
            try {
                myObject = args.getString("order_id");
                orderId.setText(myObject);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }
}
