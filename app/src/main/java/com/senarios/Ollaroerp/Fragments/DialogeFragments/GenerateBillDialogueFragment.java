package com.senarios.Ollaroerp.Fragments.DialogeFragments;

import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.senarios.Ollaroerp.R;

public class GenerateBillDialogueFragment extends DialogFragment {
    private ImageView cross;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.dialoge_generate_bill, container, false);
        cross=v.findViewById(R.id.cross);
        cross.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
            }
        });
        return v;
    }
    public void onBackPressed(){
        dismiss();
    }
}
