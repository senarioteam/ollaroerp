package com.senarios.Ollaroerp.Fragments;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.Toast;

import com.activeandroid.query.Delete;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.kaopiz.kprogresshud.KProgressHUD;
import com.senarios.Ollaroerp.MainActivity;
import com.senarios.Ollaroerp.Models.BasePostPaidOrder;
import com.senarios.Ollaroerp.Models.BaseUnpaidBill;
import com.senarios.Ollaroerp.Models.CartItemModel;
import com.senarios.Ollaroerp.Models.OfflineBillModel;
import com.senarios.Ollaroerp.Models.OfflineOrderItems;
import com.senarios.Ollaroerp.Models.OfflinePostOrderItems;
import com.senarios.Ollaroerp.Models.Order;
import com.senarios.Ollaroerp.Models.OrderResponse;
import com.senarios.Ollaroerp.Models.PaidOrder;
import com.senarios.Ollaroerp.R;
import com.senarios.Ollaroerp.Utitlities.FragmentTags;
import com.senarios.Ollaroerp.Utitlities.PrefUtils;
import com.senarios.Ollaroerp.Utitlities.Retrofit.GetDataService;
import com.senarios.Ollaroerp.Utitlities.Retrofit.RetrofitClientInstance;

import org.jetbrains.annotations.NotNull;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;


public class CheckoutFragment extends Fragment {

    private CheckBox prepaid, postpaid;
    private Button next;
    private String myObject;
    private Order order;
    private KProgressHUD pd;
    private String items;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        View rooView = inflater.inflate(R.layout.fragment_checkout, container, false);
        initView(rooView);
        return rooView;
    }

    public void initView(View rootView) {
        prepaid = rootView.findViewById(R.id.check_prepaid);
        postpaid = rootView.findViewById(R.id.check_postpaid);
        next = rootView.findViewById(R.id.btn_continue);
        setClickListeners();
    }

    public void setClickListeners() {
        prepaid.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {

                if (isChecked) {
                    if (postpaid.isChecked()) {
                        postpaid.setChecked(false);
                    }
                }

            }
        });
        postpaid.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    if (prepaid.isChecked()) {
                        prepaid.setChecked(false);
                    }
                }
            }
        });
        next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (prepaid.isChecked()) {
                    List<CartItemModel> models = MainActivity.activity.getAll();
                    JSONArray jsonArray = new JSONArray();
                    for (int i = 0; i < models.size(); i++) {
                        JSONObject myJsonObject = new JSONObject();
                        try {
                            myJsonObject.put("comments",models.get(i).getComments());
                            myJsonObject.put("item_name", models.get(i).getItemName());
                            myJsonObject.put("item_quantity", models.get(i).getQuantity());
                            myJsonObject.put("image", models.get(i).getImage());
                            myJsonObject.put("price", models.get(i).getPrice());
                            myJsonObject.put("print_class_id", models.get(i).getPrintClassId());
                            myJsonObject.put("general_item_id",models.get(i).getGeneral_item_id());
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        jsonArray.put(myJsonObject);
                    }
                    PaymentMethodFragment checkoutFragment = new PaymentMethodFragment();
                    Bundle args = new Bundle();
                    GsonBuilder builder = new GsonBuilder();
                    builder.excludeFieldsWithModifiers();
                    Gson gson = builder.create();
                    args.putString("myObject", gson.toJson(order));
                    args.putString("list", jsonArray.toString());
                    checkoutFragment.setArguments(args);
                    MainActivity.activity.getSupportFragmentManager().beginTransaction().add(R.id.fragment_container, checkoutFragment, FragmentTags.PAYMENTMETHOD).addToBackStack(FragmentTags.PAYMENTMETHOD).commit();
                } else if (postpaid.isChecked()) {
                    pd.show();
                    genOrder();

                } else {
                    Toast.makeText(getActivity(), "Please Select Payment Option", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        pd = pd.create(getActivity())
                .setStyle(KProgressHUD.Style.SPIN_INDETERMINATE)
                .setLabel("Please Wait")
                .setCancellable(false)
                .setAnimationSpeed(2)
                .setDimAmount(0.5f);


    }

    @Override
    public void onStart() {
        super.onStart();
        Bundle args = getArguments();
        if (args != null) {
            try {
                myObject = args.getString("myObject");
                items = args.getString("list");
                order = new Gson().fromJson(myObject, Order.class);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    public void genOrder() {
        GetDataService service = RetrofitClientInstance.getRetrofitInstance().create(GetDataService.class);
        String amount=order.getTotal_amount();
        amount=amount.replace("KSH ","");
        order.setTotal_amount(amount);
        Call<OrderResponse> call = service.generatePostpaidOrder(String.valueOf(PrefUtils.getUserModel(getActivity()).getId()),order.getDate_time(), order.getTable_no(), order.getNo_of_guests(), order.getItem_description(), order.getCondiments(), order.getWaiter(), order.getRestaurant(), order.getTotal_amount(), items,order.getSection());
        call.enqueue(new Callback<OrderResponse>() {

            @Override
            public void onResponse(Call<OrderResponse> call, retrofit2.Response<OrderResponse> response) {
                if (response.body() != null) {
                    new Thread(new Runnable() {
                        @Override
                        public void run() {

                            getUnpaidBills();
                        }
                    }).start();
                    int id = response.body().getOrderId();
                    OrderAcceptedFragment orderAcceptedFragment = new OrderAcceptedFragment();
                    Bundle bundle = new Bundle();
                    bundle.putString("order_id", Integer.toString(id));
                    new Delete().from(CartItemModel.class).execute();
                    MainActivity.activity.updateItemCount();
                    orderAcceptedFragment.setArguments(bundle);
                    MainActivity.activity.changeFragment(R.id.fragment_container, orderAcceptedFragment, FragmentTags.ORDERACCEPTED);


                } else {
                    pd.dismiss();
                    Toast.makeText(getActivity(), "Error generating order! Please try again later", Toast.LENGTH_SHORT).show();
                }

            }

            @Override
            public void onFailure(Call<OrderResponse> call, Throwable t) {
                pd.dismiss();
                Toast.makeText(MainActivity.activity, "Something went wrong...Please try later!", Toast.LENGTH_SHORT).show();
            }
        });


    }

    public void getUnPaidOrders(  ) {
        GetDataService service = RetrofitClientInstance.getRetrofitInstance().create(GetDataService.class);
        Call<BasePostPaidOrder> call = service.getUnPaidOrders(String.valueOf(PrefUtils.getUserModel(getActivity()).getId()));
        call.enqueue(new Callback<BasePostPaidOrder>() {

            @Override
            public void onResponse(Call<BasePostPaidOrder> call, retrofit2.Response<BasePostPaidOrder> response) {
                if (response.body() != null) {
                    if(response.body().getPaidOrders().size()>0) {

                        if (response.body().getPaidOrders().size()!=0) {
                            for (int n = 0; n < response.body().getPaidOrders().size(); n++) {
                                for (int dum=0;dum<response.body().getPaidOrders().get(n).getItems().size();dum++) {
                                    OfflineOrderItems offlineOrderItems = new OfflineOrderItems(response.body().getPaidOrders().get(n).getItems().get(dum));
                                    if (MainActivity.getOrderItemRow(offlineOrderItems) == null) {
                                        offlineOrderItems.save();
                                    }
                                }
                            }
                        }
                        if (response.body().getPaidOrders().size()!=0){
                            for(int m=0;m<response.body().getPaidOrders().size();m++) {
                                PaidOrder paidOrder=new PaidOrder();
                                paidOrder.setTable_no(response.body().getPaidOrders().get(m).gettable_no());
                                paidOrder.setTotal_amount(response.body().getPaidOrders().get(m).gettotal_amount());
                                paidOrder.setDate_time(response.body().getPaidOrders().get(m).getdate_time());
                                paidOrder.setBill_id(response.body().getPaidOrders().get(m).getBillId());
                                paidOrder.setId(response.body().getPaidOrders().get(m).getId());
                                paidOrder.setItem_description(response.body().getPaidOrders().get(m).getitem_description());
                                paidOrder.setNo_of_guests(response.body().getPaidOrders().get(m).getno_of_guests());
                                OfflinePostOrderItems offlinePostOrderItems = new OfflinePostOrderItems(paidOrder);
                                if (MainActivity.getPostOrderRow(offlinePostOrderItems) == null) {
                                    offlinePostOrderItems.save();
                                }
                            }
                        }

                        int dum=MainActivity.getAllPostOrderItems().size();
                        System.out.println(""+dum);


                   }
                    pd.dismiss();
                } else {
                    pd.dismiss();
                    Toast.makeText(getActivity(), "Some Error Occured! Please Try Again", Toast.LENGTH_SHORT).show();
                }

            }

            @Override
            public void onFailure(Call<BasePostPaidOrder> call, Throwable t) {
                pd.dismiss();
                 Toast.makeText(MainActivity.activity, "Something went wrong...Please try later!", Toast.LENGTH_SHORT).show();
            }
        });


    }

    public void getUnpaidBills(  ) {
        GetDataService service = RetrofitClientInstance.getRetrofitInstance().create(GetDataService.class);
        Call<BaseUnpaidBill> call = service.getUnpaidBills(String.valueOf(PrefUtils.getUserModel(getActivity()).getId()));
        call.enqueue(new Callback<BaseUnpaidBill>() {

            @Override
            public void onResponse(Call<BaseUnpaidBill> call, retrofit2.Response<BaseUnpaidBill> response) {
                if (response.body() != null) {
                    new Delete().from(OfflineBillModel.class).execute();
                    for (int i = 0; i < response.body().getBills().size(); i++) {
                        OfflineBillModel offlineBillModel = new OfflineBillModel(response.body().getBills().get(i));
                        if (MainActivity.getBillRow(response.body().getBills().get(i)) == null) {
                            offlineBillModel.save();
                        }
                    }


                    getUnPaidOrders();

                } else {
                    Toast.makeText(getActivity(), "Error generating order! Please try again later", Toast.LENGTH_SHORT).show();
                }


            }

            @Override
            public void onFailure(@NotNull Call<BaseUnpaidBill> call, @NotNull Throwable t) {

                Toast.makeText(MainActivity.activity, "Something went wrong...Please try later!", Toast.LENGTH_SHORT).show();
            }
        });


    }
}
