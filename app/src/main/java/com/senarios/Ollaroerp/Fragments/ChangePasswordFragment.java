package com.senarios.Ollaroerp.Fragments;

import android.os.Bundle;
import android.os.CountDownTimer;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.gson.JsonObject;
import com.kaopiz.kprogresshud.KProgressHUD;
import com.senarios.Ollaroerp.MainActivity;
import com.senarios.Ollaroerp.R;
import com.senarios.Ollaroerp.Utitlities.PrefUtils;
import com.senarios.Ollaroerp.Utitlities.Retrofit.GetDataService;
import com.senarios.Ollaroerp.Utitlities.Retrofit.RetrofitClientInstance;

import retrofit2.Call;
import retrofit2.Callback;

public class ChangePasswordFragment extends Fragment {
    private Button confirmChangePassword;
    private EditText etCurrentPassword, etNewPassword, etConfirmPassword;
    private CountDownTimer timer;
    private KProgressHUD pd;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        View rooView = inflater.inflate(R.layout.fragment_change_password, container, false);
        initView(rooView);
        return rooView;
    }

    public void initView(View rootView) {
        etConfirmPassword = rootView.findViewById(R.id.et_confirm_password);
        etNewPassword = rootView.findViewById(R.id.et_new_password);
        etCurrentPassword = rootView.findViewById(R.id.et_current_password);
        confirmChangePassword = rootView.findViewById(R.id.confirm_change_password);
        pd = pd.create(getActivity())
                .setStyle(KProgressHUD.Style.SPIN_INDETERMINATE)
                .setLabel("Just a moment")
                .setCancellable(false)
                .setAnimationSpeed(2)
                .setDimAmount(0.5f);
        setClickListeners();
    }

    public void setClickListeners() {
        confirmChangePassword.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String currentPassword = etCurrentPassword.getText().toString();
                String newPassword = etNewPassword.getText().toString();
                String confirmPassword = etConfirmPassword.getText().toString();
                if (currentPassword.equalsIgnoreCase("")) {
                    etCurrentPassword.setError("Enter your current password");
                } else if (newPassword.equalsIgnoreCase("")) {
                    etCurrentPassword.setError("Enter new password password");
                } else if (confirmPassword.equalsIgnoreCase("")) {
                    etCurrentPassword.setError("Confirm new password");
                } else {
                    if (!currentPassword.equalsIgnoreCase(PrefUtils.getString(getActivity(), "password"))) {
                        etCurrentPassword.setError("Incorrect Password");
                    } else if (newPassword.length() < 6) {
                        etNewPassword.setError("Password must be 6 characters long");
                    } else if (!newPassword.equalsIgnoreCase(confirmPassword)) {
                        etConfirmPassword.setError("Password does not match");
                    } else if (newPassword.equalsIgnoreCase(PrefUtils.getString(getActivity(), "password"))) {
                        etNewPassword.setError("New Password Matches Current Password");
                    } else {
                        callChangePassword(PrefUtils.getUserModel(getActivity()).getEmail(), newPassword);
                        pd.show();
                    }
                }
            }
        });
    }

    public void callChangePassword(String email, String password) {
        GetDataService service = RetrofitClientInstance.getRetrofitInstance().create(GetDataService.class);
        Call<JsonObject> call = service.callChangePassword(email, password);
        call.enqueue(new Callback<JsonObject>() {

            @Override
            public void onResponse(Call<JsonObject> call, retrofit2.Response<JsonObject> response) {
                JsonObject jsonObject = response.body();
                String code = jsonObject.get("code").getAsString();
                if (code.equalsIgnoreCase("200")) {
                    Toast.makeText(getActivity(), "Password Changesd Successfully", Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(getActivity(), "Please try again later", Toast.LENGTH_SHORT).show();
                }
                pd.dismiss();
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                pd.dismiss();
                Toast.makeText(MainActivity.activity, "Something went wrong...Please try later!", Toast.LENGTH_SHORT).show();
            }
        });


    }
}
