package com.senarios.Ollaroerp.Fragments.DialogeFragments;


import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;

import com.senarios.Ollaroerp.R;
import com.senarios.Ollaroerp.Utitlities.ChangeOrderStatus;


public class OrderSelectionDialogFragment extends DialogFragment {
    ChangeOrderStatus changeOrderStatus;
    RadioButton inprogRadio,compRadio,pendingRadio;
    int check;

    public OrderSelectionDialogFragment(ChangeOrderStatus listSelecter,int check) {
        changeOrderStatus = listSelecter;
        this.check=check;

    }

    public OrderSelectionDialogFragment() {

    }

    ImageView cross;
    LinearLayout inprogress, completed,pending;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.swicth_order_dialogue_fragment, container, false);
        cross = v.findViewById(R.id.cross);
        cross.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
            }
        });
        completed = v.findViewById(R.id.completed_selection);
        inprogress = v.findViewById(R.id.inprogress_selection);
        pending = v.findViewById(R.id.pending_selection);
        inprogRadio=v.findViewById(R.id.inprog_radio);
        pendingRadio = v.findViewById(R.id.pending_radio);
        pendingRadio.setEnabled(false);
        inprogRadio.setEnabled(false);
        if(check==0) {
            inprogRadio.setChecked(true);
        }
        compRadio=v.findViewById(R.id.comp_radio);
        compRadio.setEnabled(false);
        if(check==2){
            compRadio.setChecked(true);
        }else if(check==1){
            inprogRadio.setChecked(true);
        }else if(check==3){
            pendingRadio.setChecked(true);
        }
        completed.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                changeOrderStatus.listType(1);
                compRadio.setChecked(true);
                inprogRadio.setChecked(false);
                dismiss();
            }
        });
        pending.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                changeOrderStatus.listType(2);
                compRadio.setChecked(true);
                inprogRadio.setChecked(false);
                dismiss();
            }
        });
        inprogress.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                changeOrderStatus.listType(0);
                compRadio.setChecked(false);
                inprogRadio.setChecked(true);
                dismiss();
            }
        });
        return v;
    }

    public void onBackPressed() {
        dismiss();
    }


}
