package com.senarios.Ollaroerp.Fragments;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Toast;

import com.google.gson.Gson;
import com.kaopiz.kprogresshud.KProgressHUD;
import com.senarios.Ollaroerp.Adapters.ChooseBillAdapter;
import com.senarios.Ollaroerp.MainActivity;
import com.senarios.Ollaroerp.Models.BaseUnpaidBill;
import com.senarios.Ollaroerp.Models.OfflineBillModel;
import com.senarios.Ollaroerp.Models.Order;
import com.senarios.Ollaroerp.Models.UnpaidBillModel;
import com.senarios.Ollaroerp.R;
import com.senarios.Ollaroerp.Utitlities.PrefUtils;
import com.senarios.Ollaroerp.Utitlities.Retrofit.GetDataService;
import com.senarios.Ollaroerp.Utitlities.Retrofit.RetrofitClientInstance;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;

public class ChooseBillFragment extends Fragment {

    private RecyclerView billsRecyclerView;
    public static ArrayList<UnpaidBillModel> billModels;
    private Button done;
    KProgressHUD pd;
    private String myObject;
    private String items;
    private Order order;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View rootView= inflater.inflate(R.layout.fragment_choose_bill, container, false);
        init(rootView);
        return  rootView;
    }

    private void init(View rootView){
        billModels=new ArrayList<>();
        billsRecyclerView=rootView.findViewById(R.id.rv_bills);
        done=rootView.findViewById(R.id.btn_done);
        setClickListener();
    }

    public void setClickListener(){

    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        try {
            pd = pd.create(getActivity())
                    .setStyle(KProgressHUD.Style.SPIN_INDETERMINATE)
                    .setLabel("Please Wait")
                    .setCancellable(false)
                    .setAnimationSpeed(2)
                    .setDimAmount(0.5f);

            if(isNetworkAvailable()){
                pd.show();
                getUnpaidBills();
            }else{
                if (MainActivity.getAllBills().size() > 0) {
                    ArrayList<UnpaidBillModel> unpaidBillModels = new ArrayList<>();
                    for (int i = 0; i < MainActivity.getAllBills().size(); i++) {
                        OfflineBillModel offlineBillModel = MainActivity.getAllBills().get(i);
                        UnpaidBillModel unpaidBillModel = new UnpaidBillModel();
                        unpaidBillModel.setId(Integer.parseInt(offlineBillModel.getBillId()));
                        unpaidBillModel.settotal_amount(offlineBillModel.getTotalPayment());
                        unpaidBillModel.setno_orders(offlineBillModel.getNoOfOrders());
                        unpaidBillModels.add(unpaidBillModel);
                    }
                    billsRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
                    billsRecyclerView.setAdapter(new ChooseBillAdapter(getActivity(), unpaidBillModels,order,items));
                }
            }
        }catch (Exception e){
            Toast.makeText(getActivity(), e.toString(), Toast.LENGTH_SHORT).show();
        }
    }

    public void getUnpaidBills() {
        GetDataService service = RetrofitClientInstance.getRetrofitInstance().create(GetDataService.class);
        Call<BaseUnpaidBill> call = service.getUnpaidBills(String.valueOf(PrefUtils.getUserModel(getActivity()).getId()));
        call.enqueue(new Callback<BaseUnpaidBill>() {

            @Override
            public void onResponse(Call<BaseUnpaidBill> call, retrofit2.Response<BaseUnpaidBill> response) {
                if (response.body() != null) {
                    billsRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
                    billModels= (ArrayList<UnpaidBillModel>) response.body().getBills();
                    billsRecyclerView.setAdapter(new ChooseBillAdapter(getActivity(), (ArrayList<UnpaidBillModel>) response.body().getBills(),order,items));

                } else {
                    Log.v("RESPONSE",response.toString());
                    Toast.makeText(getActivity(), "Error generating order! Please try again later", Toast.LENGTH_SHORT).show();
                }
                pd.dismiss();
            }

            @Override
            public void onFailure(Call<BaseUnpaidBill> call, Throwable t) {
                pd.dismiss();
                Toast.makeText(MainActivity.activity, "Something went wrong...Please try later!", Toast.LENGTH_SHORT).show();
            }
        });


    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Bundle args = getArguments();
        if (args != null) {
            try {
                myObject = args.getString("myObject");
                items = args.getString("list");
                order = new Gson().fromJson(myObject, Order.class);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    private boolean isNetworkAvailable() {
        ConnectivityManager connectivityManager
                = (ConnectivityManager) getActivity().getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }
}
