package com.senarios.Ollaroerp.Fragments;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.activeandroid.query.Delete;
import com.senarios.Ollaroerp.MainActivity;
import com.senarios.Ollaroerp.Models.CartItemModel;
import com.senarios.Ollaroerp.R;
import com.senarios.Ollaroerp.Utitlities.FragmentTags;
import com.senarios.Ollaroerp.Utitlities.PrefUtils;


public class NavigationFragment extends Fragment {

    private LinearLayout navHome, navLogout, navPlaceOrder, navOrdersInProgress, navCreateVoucher, navChangePassword;
    private DrawerLayout mdrawerLayout;
    private ActionBarDrawerToggle mDrawerToggle;
    public static TextView navName;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        View rootView = inflater.inflate(R.layout.fragment_navigation, container, false);
        initView(rootView);

        return rootView;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        navName.setText(PrefUtils.getUserModel(getActivity()).getName());
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
    }

    public void initView(View rootView) {
        navLogout = rootView.findViewById(R.id.nav_logout);
        navHome = rootView.findViewById(R.id.nav_home);
        navChangePassword = rootView.findViewById(R.id.nav_change_password);
        navCreateVoucher = rootView.findViewById(R.id.nav_create_voucher);
        navOrdersInProgress = rootView.findViewById(R.id.nav_orders_in_progress);
        navPlaceOrder = rootView.findViewById(R.id.nav_place_order);
        navName=rootView.findViewById(R.id.nav_name);
        setClickListeners();
    }

    public void setClickListeners() {
        navLogout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                PrefUtils.clearPreferences(getActivity());
                new Delete().from(CartItemModel.class).execute();
                MainActivity.activity.mDrawerLayout.closeDrawers();
                MainActivity.activity.changeFragment(R.id.fragment_container, new LoginFragment(), FragmentTags.LOGIN);
            }
        });

        navHome.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MainActivity.activity.mDrawerLayout.closeDrawers();
                MainActivity.activity.changeFragment(R.id.fragment_container,new HomeFragment(),FragmentTags.HOME);
            }
        });

        navPlaceOrder.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MainActivity.activity.mDrawerLayout.closeDrawers();
                MainActivity.activity.changeFragment(R.id.fragment_container, new MenuFragment(), FragmentTags.MENU);
            }
        });
        navOrdersInProgress.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MainActivity.activity.mDrawerLayout.closeDrawers();
                MainActivity.activity.changeFragment(R.id.fragment_container, new InprogressOrdersFragment(), FragmentTags.ORDERSINPROGRESS);
            }
        });
        navCreateVoucher.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MainActivity.activity.mDrawerLayout.closeDrawers();
                MainActivity.activity.changeFragment(R.id.fragment_container, new CreateVoucherFragment(), FragmentTags.CREATEVOUCHER);
            }
        });

        navChangePassword.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MainActivity.activity.mDrawerLayout.closeDrawers();
                MainActivity.activity.changeFragment(R.id.fragment_container, new ChangePasswordFragment(), FragmentTags.CHANGEPASSWORD);
            }
        });
    }

    public void setup(DrawerLayout drawerLayout, final Toolbar toolbar) {

        mdrawerLayout = drawerLayout;
        mDrawerToggle = new ActionBarDrawerToggle(getActivity(), drawerLayout, toolbar, R.string.Open, R.string.Close) {
            @Override
            public void onDrawerOpened(View drawerView) {
                super.onDrawerOpened(drawerView);
            }

            @Override
            public void onDrawerClosed(View drawerView) {
                super.onDrawerClosed(drawerView);
            }

            @Override
            public void onDrawerSlide(View drawerView, float slideOffset) {
                if (slideOffset < 0.6) {
                    toolbar.setAlpha(1 - slideOffset);
                }

                // super.onDrawerSlide(drawerView , slideOffset);
            }

        };

        mdrawerLayout.setDrawerListener(mDrawerToggle);

        mdrawerLayout.post(new Runnable() {
            @Override
            public void run() {
                mDrawerToggle.syncState();
            }
        });
    }

}

