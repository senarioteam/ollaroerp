package com.senarios.Ollaroerp.Fragments;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.kaopiz.kprogresshud.KProgressHUD;
import com.senarios.Ollaroerp.Adapters.MenuAdapter;
import com.senarios.Ollaroerp.MainActivity;
import com.senarios.Ollaroerp.Models.MajorGroup;
import com.senarios.Ollaroerp.Models.BaseMajorGroup;
import com.senarios.Ollaroerp.Models.OfflineMajorGroup;
import com.senarios.Ollaroerp.R;
import com.senarios.Ollaroerp.Utitlities.Retrofit.GetDataService;
import com.senarios.Ollaroerp.Utitlities.Retrofit.RetrofitClientInstance;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;

public class MenuFragment extends Fragment {

    private RecyclerView menuRecyclerView;
    private ArrayList<MajorGroup> menuList;
    private MenuAdapter menuAdapter;
    KProgressHUD pd;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        View rootView = inflater.inflate(R.layout.fragment_menu, container, false);
        initView(rootView);
        return rootView;
    }

    private void initView(View rootView) {
        menuRecyclerView = rootView.findViewById(R.id.rv_menu);
        menuList = new ArrayList<>();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        pd = pd.create(getActivity())
                .setStyle(KProgressHUD.Style.SPIN_INDETERMINATE)
                .setLabel("Please Wait")
                .setCancellable(false)
                .setAnimationSpeed(2)
                .setDimAmount(0.5f);


        if (MainActivity.getAllMajorGroups().size() != 0) {
            menuList.clear();
            for (int i = 0; i < MainActivity.getAllMajorGroups().size(); i++) {
                MajorGroup majorGroup = new MajorGroup();
                majorGroup.setId(MainActivity.getAllMajorGroups().get(i).getMajor_id());
                majorGroup.setAImage(MainActivity.getAllMajorGroups().get(i).getA_image());
                majorGroup.setName(MainActivity.getAllMajorGroups().get(i).getName());
                majorGroup.setCreatedAt(MainActivity.getAllMajorGroups().get(i).getCreated_at());
                majorGroup.setUpdatedAt(MainActivity.getAllMajorGroups().get(i).getUpdated_at());
                majorGroup.setDisplayOrder(MainActivity.getAllMajorGroups().get(i).getDisplay_order());

                menuList.add(majorGroup);
            }

            menuAdapter = new MenuAdapter(getActivity(), menuList);
            GridLayoutManager layoutManager = new GridLayoutManager(getActivity(), 4);

            layoutManager.setSpanSizeLookup(new GridLayoutManager.SpanSizeLookup() {
                @Override
                public int getSpanSize(int position) {

                    return 2;
                }
            });
            menuRecyclerView.setLayoutManager(layoutManager);
            menuRecyclerView.setAdapter(menuAdapter);
        } else {
            if (isNetworkAvailable()) {
                pd.show();
                callMajorGroups();
            } else {

                Toast.makeText(getActivity(), "No items in the list! Try synchronizing again for updating items", Toast.LENGTH_LONG).show();
            }
        }


    }

    public void callMajorGroups() {
        GetDataService service = RetrofitClientInstance.getRetrofitInstance().create(GetDataService.class);

        Call<BaseMajorGroup> call = service.getMajorGroups();
        call.enqueue(new Callback<BaseMajorGroup>() {

            @Override
            public void onResponse(Call<BaseMajorGroup> call, retrofit2.Response<BaseMajorGroup> response) {
                if (response.body() != null) {
                    for (int i = 0; i < response.body().getMajorGroup().size(); i++) {
                        menuList.add(response.body().getMajorGroup().get(i));

                        OfflineMajorGroup offlineMajorGroup = new OfflineMajorGroup();
                        offlineMajorGroup.setMajor_id(response.body().getMajorGroup().get(i).getId());
                        offlineMajorGroup.setA_image(response.body().getMajorGroup().get(i).getAImage());
                        offlineMajorGroup.setName(response.body().getMajorGroup().get(i).getName());
                        offlineMajorGroup.setCreated_at(response.body().getMajorGroup().get(i).getCreatedAt());
                        offlineMajorGroup.setUpdated_at(response.body().getMajorGroup().get(i).getUpdatedAt());
                        offlineMajorGroup.setDisplay_order(response.body().getMajorGroup().get(i).getDisplayOrder());
                        if (MainActivity.getMajorGroupRow(response.body().getMajorGroup().get(i)) == null) {
                            offlineMajorGroup.save();
                        }
                    }
                    menuAdapter = new MenuAdapter(getActivity(), menuList);
                    GridLayoutManager layoutManager = new GridLayoutManager(getActivity(), 4);

                    layoutManager.setSpanSizeLookup(new GridLayoutManager.SpanSizeLookup() {
                        @Override
                        public int getSpanSize(int position) {

                            return 2;
                        }
                    });
                    menuRecyclerView.setLayoutManager(layoutManager);
                    menuRecyclerView.setAdapter(menuAdapter);
                } else {
                    Toast.makeText(getActivity(), "Error fetching list! Please try again later", Toast.LENGTH_SHORT).show();
                }
                pd.dismiss();
            }

            @Override
            public void onFailure(Call<BaseMajorGroup> call, Throwable t) {
                pd.dismiss();
                Toast.makeText(MainActivity.activity, "Something went wrong...Please try later!", Toast.LENGTH_SHORT).show();
            }
        });


    }

    private boolean isNetworkAvailable() {
        ConnectivityManager connectivityManager
                = (ConnectivityManager) getActivity().getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }
}
