package com.senarios.Ollaroerp.Fragments;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.activeandroid.query.Delete;
import com.kaopiz.kprogresshud.KProgressHUD;
import com.senarios.Ollaroerp.Adapters.UnpaidBillsAdapter;
import com.senarios.Ollaroerp.MainActivity;
import com.senarios.Ollaroerp.Models.BaseUnpaidBill;
import com.senarios.Ollaroerp.Models.OfflineBillModel;
import com.senarios.Ollaroerp.Models.UnpaidBillModel;
import com.senarios.Ollaroerp.R;
import com.senarios.Ollaroerp.Utitlities.PrefUtils;
import com.senarios.Ollaroerp.Utitlities.Retrofit.GetDataService;
import com.senarios.Ollaroerp.Utitlities.Retrofit.RetrofitClientInstance;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;

public class UnpaidBillsFragment extends Fragment {

    private RecyclerView unpaidBillsRecyclerView;
    KProgressHUD pd;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_unpaid_bills, container, false);
        initView(rootView);
        return rootView;
    }

    private void initView(View rootView) {
        unpaidBillsRecyclerView = rootView.findViewById(R.id.rv_unpaid_bills);

    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        pd = pd.create(getActivity())
                .setStyle(KProgressHUD.Style.SPIN_INDETERMINATE)
                .setLabel("Please Wait")
                .setCancellable(false)
                .setAnimationSpeed(2)
                .setDimAmount(0.5f);

        if (isNetworkAvailable()) {
            pd.show();
            getUnpaidBills();
        } else {
            if (MainActivity.getAllBills().size() > 0) {
                new background().execute();

            }
        }

    }

    public void getUnpaidBills() {
        GetDataService service = RetrofitClientInstance.getRetrofitInstance().create(GetDataService.class);
        Call<BaseUnpaidBill> call = service.getUnpaidBills(String.valueOf(PrefUtils.getUserModel(getActivity()).getId()));
        call.enqueue(new Callback<BaseUnpaidBill>() {

            @Override
            public void onResponse(Call<BaseUnpaidBill> call, retrofit2.Response<BaseUnpaidBill> response) {
                if (response.body() != null) {
                    new Delete().from(OfflineBillModel.class).execute();
                    if (response.body().getBills().size()!=0){
                        for (int k = 0; k < response.body().getBills().size(); k++) {
                            OfflineBillModel offlineBillModel = new OfflineBillModel(response.body().getBills().get(k));
                            if (MainActivity.getBillRow(response.body().getBills().get(k)) == null) {
                                offlineBillModel.save();
                            }}




                    }
                    unpaidBillsRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
                    unpaidBillsRecyclerView.setAdapter(new UnpaidBillsAdapter(getActivity(), (ArrayList<UnpaidBillModel>) response.body().getBills()));

                } else {
                    Toast.makeText(getActivity(), "Error generating order! Please try again later", Toast.LENGTH_SHORT).show();
                }
                pd.dismiss();
            }

            @Override
            public void onFailure(Call<BaseUnpaidBill> call, Throwable t) {
                pd.dismiss();
                Toast.makeText(MainActivity.activity, "Something went wrong...Please try later!", Toast.LENGTH_SHORT).show();
            }
        });


    }

    private boolean isNetworkAvailable() {
        ConnectivityManager connectivityManager
                = (ConnectivityManager) getActivity().getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }

    public class background extends AsyncTask<Void, Void, List> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected void onPostExecute(List list) {
            unpaidBillsRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
            unpaidBillsRecyclerView.setAdapter(new UnpaidBillsAdapter(getActivity(), list));
            super.onPostExecute(list);
        }

        @Override
        protected List doInBackground(Void... voids) {
            ArrayList<UnpaidBillModel> unpaidBillModels = new ArrayList<>();
            for (int i = 0; i < MainActivity.getAllBills().size(); i++) {
                OfflineBillModel offlineBillModel = MainActivity.getAllBills().get(i);
                UnpaidBillModel unpaidBillModel = new UnpaidBillModel();
                unpaidBillModel.setId(Integer.parseInt(offlineBillModel.getBillId()));
                unpaidBillModel.settotal_amount(offlineBillModel.getTotalPayment());
                unpaidBillModel.setno_orders(offlineBillModel.getNoOfOrders());
                unpaidBillModels.add(unpaidBillModel);
            }
            return unpaidBillModels;
        }
    }
}

