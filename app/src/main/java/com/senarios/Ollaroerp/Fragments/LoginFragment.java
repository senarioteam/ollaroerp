package com.senarios.Ollaroerp.Fragments;


import android.os.Bundle;
import android.os.CountDownTimer;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.util.Patterns;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.iid.FirebaseInstanceId;
import com.kaopiz.kprogresshud.KProgressHUD;
import com.senarios.Ollaroerp.BuildConfig;
import com.senarios.Ollaroerp.MainActivity;
import com.senarios.Ollaroerp.Models.BaseAllItemsResponse;
import com.senarios.Ollaroerp.Models.OfflineAlchoholicGroup;
import com.senarios.Ollaroerp.Models.OfflineBillModel;
import com.senarios.Ollaroerp.Models.OfflineFamilyGroup;
import com.senarios.Ollaroerp.Models.OfflineItemModel;
import com.senarios.Ollaroerp.Models.OfflineMajorGroup;
import com.senarios.Ollaroerp.Models.OfflineSubAlchoholicGroup;
import com.senarios.Ollaroerp.Models.OfflineSubMajorGroup;
import com.senarios.Ollaroerp.Models.OfflineTableModel;
import com.senarios.Ollaroerp.Models.UserBaseModel;
import com.senarios.Ollaroerp.R;
import com.senarios.Ollaroerp.Utitlities.FragmentTags;
import com.senarios.Ollaroerp.Utitlities.PrefUtils;
import com.senarios.Ollaroerp.Utitlities.Retrofit.GetDataService;
import com.senarios.Ollaroerp.Utitlities.Retrofit.RetrofitClientInstance;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import retrofit2.Call;
import retrofit2.Callback;


public class LoginFragment extends Fragment {

    private Button submit;
    private TextView forgotPassword;
    private EditText etEmail, etPassword;
    private RadioButton waiter, manager;
    private KProgressHUD pd;
    private LinearLayout linearLayout;
    private CountDownTimer timer;
    private KProgressHUD pd2;
    boolean isFirst=true;

    public LoginFragment() {

    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_login, container, false);
        initView(rootView);
        MainActivity.activity.getSupportActionBar().hide();
        return rootView;
    }

    public void initView(View rootView) {
        linearLayout = rootView.findViewById(R.id.linearLayout);
        submit = rootView.findViewById(R.id.btn_submit);
        waiter = rootView.findViewById(R.id.rdb_waiter);
        manager = rootView.findViewById(R.id.rdb_manager);
        forgotPassword = rootView.findViewById(R.id.tv_forgot_password);
        etEmail = rootView.findViewById(R.id.et_email);
        etPassword = rootView.findViewById(R.id.et_pass);
        waiter.setChecked(true);
        setClickListeners();
    }

    public void setClickListeners() {
        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String userName = etEmail.getText().toString();
                String password = etPassword.getText().toString();
                if (!waiter.isChecked() && !manager.isChecked()) {
                    Toast.makeText(getActivity(), "Please select login type", Toast.LENGTH_SHORT).show();
                } else if (userName.equalsIgnoreCase("")) {
                    etEmail.setError("Please enter email address");
                } else if (password.equalsIgnoreCase("")) {
                    etPassword.setError("Please enter password");
                } else if (!Patterns.EMAIL_ADDRESS.matcher(userName).matches()) {
                    etEmail.setError("Please enter a valid email");
                } else {

                    callLogin(etEmail.getText().toString(), etPassword.getText().toString(), BuildConfig.VERSION_NAME);
                    pd.show();

                }
            }
        });

        waiter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                waiter.setChecked(true);
                manager.setChecked(false);

            }
        });

        manager.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                manager.setChecked(true);
                waiter.setChecked(false);

            }
        });

        forgotPassword.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MainActivity.activity.changeFragment(R.id.fragment_container, new ForgotPasswordFragment(), FragmentTags.FORGOTPASSWORD);
            }
        });

    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        pd = pd.create(getActivity())
                .setStyle(KProgressHUD.Style.SPIN_INDETERMINATE)
                .setLabel("Please Wait")
                .setCancellable(false)
                .setAnimationSpeed(2)
                .setDimAmount(0.5f);
        pd2 = pd.create(getActivity())
                .setStyle(KProgressHUD.Style.SPIN_INDETERMINATE)
                .setLabel("Synchronizing Data")
                .setCancellable(false)
                .setAnimationSpeed(2)
                .setDimAmount(0.5f);
    }

    public void callLogin(String email, final String password,String Versio) {
        final GetDataService service = RetrofitClientInstance.getRetrofitInstance().create(GetDataService.class);

        Call<UserBaseModel> call = service.callLogin(email, password, FirebaseInstanceId.getInstance().getToken(),Versio);
        call.enqueue(new Callback<UserBaseModel>() {

            @Override
            public void onResponse(Call<UserBaseModel> call, retrofit2.Response<UserBaseModel> response) {
                if (response.body() != null) {
                    if (response.body().getVersion().equalsIgnoreCase(BuildConfig.VERSION_NAME)) {
                        if (response.body().getUser().getUserType().equalsIgnoreCase("waiter") && waiter.isChecked()) {
                            PrefUtils.putUserModel(getActivity(), response.body().getUser(), password);
                            PrefUtils.putString(getActivity(), "password", password);
                            PrefUtils.putString(getActivity(), "is_logged_in", "true");
                            NavigationFragment.navName.setText(PrefUtils.getUserModel(getActivity()).getName());
                            pd.dismiss();
                            getEveryThing();
                        } else if (response.body().getUser().getUserType().equalsIgnoreCase("manager") && manager.isChecked()) {
                            PrefUtils.putUserModel(getActivity(), response.body().getUser(), password);
                            PrefUtils.putString(getActivity(), "password", password);
                            PrefUtils.putString(getActivity(), "is_logged_in", "trueManager");
                            MainActivity.activity.changeFragment(R.id.fragment_container, new ManagerHomeFragment(), FragmentTags.MANAGERHOME);
                            NavigationFragment.navName.setText(PrefUtils.getUserModel(getActivity()).getName());
                        } else {
                            Toast.makeText(getActivity(), "Invalid email or password", Toast.LENGTH_SHORT).show();
                        }
                    }
                    else{
                        final Snackbar snackbar = Snackbar
                                .make(linearLayout, "Invalid App Version! Please Install Latest APK", Snackbar.LENGTH_LONG)
                                .setAction("DISMISS", new View.OnClickListener() {
                                    @Override
                                    public void onClick(View view) {
                                        Snackbar snackbar2 = Snackbar
                                                .make(linearLayout, "Invalid App Version! Please Install Latest APK", Snackbar.LENGTH_LONG);
                                        snackbar2.dismiss();
                                    }
                                });

                        snackbar.show();
                    }
                } else {
                    final Snackbar snackbar = Snackbar
                            .make(linearLayout, "Invalid email or password!", Snackbar.LENGTH_LONG)
                            .setAction("DISMISS", new View.OnClickListener() {
                                @Override
                                public void onClick(View view) {
                                    Snackbar snackbar2 = Snackbar
                                            .make(linearLayout, "Invalid email or password!", Snackbar.LENGTH_LONG);
                                    snackbar2.dismiss();
                                }
                            });

                    snackbar.show();
                }
                pd.dismiss();
            }

            @Override
            public void onFailure(Call<UserBaseModel> call, Throwable t) {
                pd.dismiss();
                Toast.makeText(MainActivity.activity, "Something went wrong...Please try later!", Toast.LENGTH_SHORT).show();
            }
        });


    }

    public void getEveryThing() {
        pd2.show();
        GetDataService service = RetrofitClientInstance.getRetrofitInstance().create(GetDataService.class);

        Call<BaseAllItemsResponse> call = service.getEverything(String.valueOf(PrefUtils.getUserModel(getActivity()).getId()));
        call.enqueue(new Callback<BaseAllItemsResponse>() {

            @Override
            public void onResponse(Call<BaseAllItemsResponse> call, final retrofit2.Response<BaseAllItemsResponse> response) {
                if (response.body() != null) {

                    for (int i = 0; i < response.body().getMajorGroup().size(); i++) {
                        OfflineMajorGroup offlineMajorGroup = new OfflineMajorGroup();
                        offlineMajorGroup.setMajor_id(response.body().getMajorGroup().get(i).getId());
                        offlineMajorGroup.setA_image(response.body().getMajorGroup().get(i).getAImage());
                        offlineMajorGroup.setName(response.body().getMajorGroup().get(i).getName());
                        offlineMajorGroup.setCreated_at(response.body().getMajorGroup().get(i).getCreatedAt());
                        offlineMajorGroup.setUpdated_at(response.body().getMajorGroup().get(i).getUpdatedAt());
                        offlineMajorGroup.setDisplay_order(response.body().getMajorGroup().get(i).getDisplayOrder());
                        if (MainActivity.getMajorGroupRow(response.body().getMajorGroup().get(i)) == null) {
                            offlineMajorGroup.save();
                        }
                        for (int a = 0; a < response.body().getSubGroups().size(); a++) {
                            OfflineSubMajorGroup offlineSubMajorGroup = new OfflineSubMajorGroup(response.body().getSubGroups().get(a));
                            offlineSubMajorGroup.setMajor_id(String.valueOf(response.body().getMajorGroup().get(i).getId()));
                            if (MainActivity.getSubMajorGroupRow(response.body().getSubGroups().get(a)) == null) {
                                offlineSubMajorGroup.save();
                            }

                        }
                        for (int b = 0; b < response.body().getAlcoholGroups().size(); b++) {
                            OfflineAlchoholicGroup offlineAlchoholicGroup = new OfflineAlchoholicGroup(response.body().getAlcoholGroups().get(b));
                            if (MainActivity.getAlcoGroupRow(response.body().getAlcoholGroups().get(b)) == null) {
                                offlineAlchoholicGroup.save();
                            }

                        }
                        for (int c = 0; c < response.body().getFamilyGroups().size(); c++) {
                            OfflineFamilyGroup offlineFamilyGroup = new OfflineFamilyGroup(response.body().getFamilyGroups().get(c));
                            if (MainActivity.getFamGroupRow(response.body().getFamilyGroups().get(c)) == null) {
                                offlineFamilyGroup.save();

                            }
                        }
                        for (int d = 0; d < response.body().getSubAlcoholicGroups().size(); d++) {
                            OfflineSubAlchoholicGroup offlineAlchoholicGroup = new OfflineSubAlchoholicGroup(response.body().getSubAlcoholicGroups().get(d));
                            if (MainActivity.getSubAlcoGroupRow(response.body().getSubAlcoholicGroups().get(d)) == null) {
                                offlineAlchoholicGroup.save();
                            }
                        }
                        for (int e = 0; e < response.body().getItems().size(); e++) {
                            OfflineItemModel offlineItemModel = new OfflineItemModel(response.body().getItems().get(e));
                            if (MainActivity.getItemRow(response.body().getItems().get(e)) == null) {
                                offlineItemModel.save();
                            }

                        }
                        for (int f = 0; f < response.body().getTables().size(); f++) {
                            if (response.body().getTables().get(f).getwaiter_id().equalsIgnoreCase(String.valueOf(PrefUtils.getUserModel(MainActivity.activity).getId()))) {
                                OfflineTableModel offlineTableModel = new OfflineTableModel(response.body().getTables().get(f));
                                if (MainActivity.getTableRow(response.body().getTables().get(f)) == null) {
                                    offlineTableModel.save();
                                }
                            }
                        }

                        for (int k = 0; k < response.body().getBillModels().size(); k++) {
                            OfflineBillModel offlineBillModel = new OfflineBillModel(response.body().getBillModels().get(k));
                            if (MainActivity.getBillRow(response.body().getBillModels().get(k)) == null) {
                                offlineBillModel.save();
                            }
                        }
                        Date c = Calendar.getInstance().getTime();
                        System.out.println("Current time => " + c);

                        SimpleDateFormat df = new SimpleDateFormat("dd-MMM-yyyy");
                        String formattedDate = df.format(c);
                        PrefUtils.putString(MainActivity.activity, "date", formattedDate);
                        MainActivity.activity.changeFragment(R.id.fragment_container, new HomeFragment(), FragmentTags.HOME);

                    }


                } else {
                    MainActivity.activity.changeFragment(R.id.fragment_container, new HomeFragment(), FragmentTags.HOME);
                    Toast.makeText(MainActivity.activity, "Error fetching list! Please try again later", Toast.LENGTH_SHORT).show();
                }
                pd2.dismiss();

            }

            @Override
            public void onFailure(Call<BaseAllItemsResponse> call, Throwable t) {
                pd2.dismiss();
                MainActivity.activity.changeFragment(R.id.fragment_container, new HomeFragment(), FragmentTags.HOME);
                if(!(MainActivity.activity.getAllMajorGroups().size()>0)) {
                    Toast.makeText(MainActivity.activity, "Something went wrong...Please try later!", Toast.LENGTH_SHORT).show();
                }
            }
        });

    }
}
