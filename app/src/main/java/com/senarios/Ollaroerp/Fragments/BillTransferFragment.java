package com.senarios.Ollaroerp.Fragments;


import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.kaopiz.kprogresshud.KProgressHUD;
import com.senarios.Ollaroerp.MainActivity;
import com.senarios.Ollaroerp.R;
import com.senarios.Ollaroerp.Utitlities.FragmentTags;
import com.senarios.Ollaroerp.Utitlities.PrefUtils;
import com.senarios.Ollaroerp.Utitlities.Retrofit.GetDataService;
import com.senarios.Ollaroerp.Utitlities.Retrofit.RetrofitClientInstance;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;


public class BillTransferFragment extends Fragment {

    private TextView waiterId, waiterName, billId;
    private Spinner waiterNames;
    private KProgressHUD pd;
    private ArrayList<Waiter> waiters;
    private ArrayList<String> waiterSpinnerList;
    private String billIdFromArgs;
    private String waiterIdToPass;
    private Button transfer;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View rootView = inflater.inflate(R.layout.fragment_bill_transfer, container, false);
        initView(rootView);
        return rootView;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Bundle args = this.getArguments();
        if (args != null) {
            billIdFromArgs = args.getString("bill_id");
        } else {
            Toast.makeText(getActivity(), "Please try again", Toast.LENGTH_SHORT).show();
            getActivity().getSupportFragmentManager().popBackStack();
        }
    }

    private void initView(View rootView) {
        waiterName = rootView.findViewById(R.id.tv_waiter_name);
        waiterId = rootView.findViewById(R.id.tv_waiter_id);
        transfer=rootView.findViewById(R.id.btn_transfer);
        billId = rootView.findViewById(R.id.tv_bill_id);
        waiterNames = rootView.findViewById(R.id.spinner_waiter_name);
        pd = pd.create(getActivity())
                .setStyle(KProgressHUD.Style.SPIN_INDETERMINATE)
                .setLabel("Please Wait")
                .setCancellable(false)
                .setAnimationSpeed(2)
                .setDimAmount(0.5f);
        pd.show();
        getWaiters();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        billId.setText(billIdFromArgs);
        waiterId.setText(String.valueOf(PrefUtils.getUserModel(getActivity()).getId()));
        waiterName.setText(PrefUtils.getUserModel(getActivity()).getName());
        transfer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                transferBill();
            }
        });
    }

    public void getWaiters() {
        GetDataService service = RetrofitClientInstance.getRetrofitInstance().create(GetDataService.class);
        Call<JsonObject> call = service.getWaiters();
        call.enqueue(new Callback<JsonObject>() {

            @Override
            public void onResponse(Call<JsonObject> call, retrofit2.Response<JsonObject> response) {
                if (response.body() != null) {
                    JsonObject jsonObject = response.body();
                    JsonArray jsonArray = jsonObject.get("waiters").getAsJsonArray();
                    waiters = new ArrayList<>();
                    waiterSpinnerList = new ArrayList<>();
                    for (int i = 0; i < jsonArray.size(); i++) {
                        JsonObject waiterObject = jsonArray.get(i).getAsJsonObject();
                        Waiter waiter = new Waiter();
                        waiter.setWaiterId(waiterObject.get("id").getAsString());
                        waiter.setWaiterName(waiterObject.get("name").getAsString());
                        if(!(waiter.getWaiterId().equalsIgnoreCase(String.valueOf(PrefUtils.getUserModel(getActivity()).getId())))) {
                            waiters.add(waiter);
                            waiterSpinnerList.add(waiter.getWaiterName());
                        }
                    }

                    ArrayAdapter<String> spinnerArrayAdapter = new ArrayAdapter<String>
                            (getActivity(), android.R.layout.simple_spinner_item,
                                    waiterSpinnerList); //selected item will look like a spinner set from XML
                    spinnerArrayAdapter.setDropDownViewResource(android.R.layout
                            .simple_spinner_dropdown_item);
                    waiterNames.setAdapter(spinnerArrayAdapter);
                    waiterNames.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                        @Override
                        public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                            waiterIdToPass = waiters.get(position).getWaiterId();
                        }

                        @Override
                        public void onNothingSelected(AdapterView<?> parent) {

                        }
                    });
                } else {
                    Toast.makeText(getActivity(), "Error fetching waiters! Please try again later", Toast.LENGTH_SHORT).show();
                }
                pd.dismiss();
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                pd.dismiss();
                Toast.makeText(MainActivity.activity, "Something went wrong...Please try later!", Toast.LENGTH_SHORT).show();
            }
        });


    }

    public void transferBill() {
        pd.show();
        GetDataService service = RetrofitClientInstance.getRetrofitInstance().create(GetDataService.class);
        Call<JsonObject> call = service.transferBill(billIdFromArgs,waiterIdToPass);
        call.enqueue(new Callback<JsonObject>() {

            @Override
            public void onResponse(Call<JsonObject> call, retrofit2.Response<JsonObject> response) {
                if (response.body() != null) {
                    if(response.code()==200){
                        Toast.makeText(getActivity(), "Bill Transfered Successfully", Toast.LENGTH_SHORT).show();
                        MainActivity.activity.changeFragment(R.id.fragment_container,new GenerateBillFragment(), FragmentTags.GENERATEBILL);
                    }
                }
                pd.dismiss();
            }
            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                pd.dismiss();
                Toast.makeText(MainActivity.activity, "Something went wrong...Please try later!", Toast.LENGTH_SHORT).show();
            }
        });


    }

    public class Waiter {
        String waiterName;
        String waiterId;

        public Waiter() {

        }

        public void setWaiterId(String waiterId) {
            this.waiterId = waiterId;
        }

        public void setWaiterName(String waiterName) {
            this.waiterName = waiterName;
        }

        public String getWaiterId() {
            return waiterId;
        }

        public String getWaiterName() {
            return waiterName;
        }
    }

}
