package com.senarios.Ollaroerp.Fragments;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.widget.DrawerLayout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.kaopiz.kprogresshud.KProgressHUD;
import com.senarios.Ollaroerp.MainActivity;
import com.senarios.Ollaroerp.R;
import com.senarios.Ollaroerp.Utitlities.FragmentTags;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class HomeFragment extends Fragment {

    private LinearLayout placeOrder, orderInProgress, generateBills;
    public int i = 0, a = 0, b = 0, c = 0, d = 0, e = 0, f = 0, g = 0;
    private KProgressHUD pd;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_home, container, false);
        initView(rootView);
        return rootView;
    }

    public void initView(View rootView) {
        placeOrder = rootView.findViewById(R.id.btn_place_order);
        orderInProgress = rootView.findViewById(R.id.btn_orders_inprogress);
        generateBills = rootView.findViewById(R.id.btn_generate_bill);
        MainActivity.activity.showToolbar();

        MainActivity.activity.mDrawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_UNLOCKED);
        MainActivity.activity.getSupportActionBar().setTitle("Home");
        setClickListeners();
    }

    public void setClickListeners() {
        placeOrder.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MainActivity.activity.changeFragment(R.id.fragment_container, new MenuFragment(), FragmentTags.MENU);
            }
        });
        orderInProgress.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MainActivity.activity.changeFragment(R.id.fragment_container, new InprogressOrdersFragment(), FragmentTags.ORDERSINPROGRESS);
            }
        });
        generateBills.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MainActivity.activity.changeFragment(R.id.fragment_container, new GenerateBillFragment(), FragmentTags.GENERATEBILL);
            }
        });
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        if (isNetworkAvailable()) {
            Date c = Calendar.getInstance().getTime();
            System.out.println("Current time => " + c);
            SimpleDateFormat df = new SimpleDateFormat("dd-MMM-yyyy");
            String formattedDate = df.format(c);
          /* if (!PrefUtils.getString(getActivity(), "date","22-10-1996").equalsIgnoreCase(formattedDate)) {
            */
           //}

        }
    }

    private boolean isNetworkAvailable() {
        ConnectivityManager connectivityManager
                = (ConnectivityManager) getActivity().getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }




}
