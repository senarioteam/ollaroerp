package com.senarios.Ollaroerp.Fragments;


import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.kaopiz.kprogresshud.KProgressHUD;
import com.senarios.Ollaroerp.Adapters.SubMajorGroupAdapter;
import com.senarios.Ollaroerp.MainActivity;
import com.senarios.Ollaroerp.Models.BaseSubMajorGroup;
import com.senarios.Ollaroerp.Models.OfflineSubMajorGroup;
import com.senarios.Ollaroerp.Models.SubMajorGroup;
import com.senarios.Ollaroerp.R;
import com.senarios.Ollaroerp.Utitlities.Retrofit.GetDataService;
import com.senarios.Ollaroerp.Utitlities.Retrofit.RetrofitClientInstance;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;

/**
 * A simple {@link Fragment} subclass.
 */
public class SubMajorGroupFragment extends Fragment {

    private RecyclerView menuCategoryRecyclerView;
    private SubMajorGroupAdapter subMajorGroupAdapter;
    private ArrayList<SubMajorGroup> menuCategories;
    private KProgressHUD pd;
    private String major_id;
    public SubMajorGroupFragment() {

    }


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_menu_category, container, false);
        initView(rootView);
        return rootView;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        pd = pd.create(getActivity())
                .setStyle(KProgressHUD.Style.SPIN_INDETERMINATE)
                .setLabel("Please Wait")
                .setCancellable(false)
                .setAnimationSpeed(2)
                .setDimAmount(0.5f);



    }

    private void initView(View rootView) {
        menuCategoryRecyclerView = rootView.findViewById(R.id.rv_menu_category);
    }

    public void callSubMajorGroups() {
        GetDataService service = RetrofitClientInstance.getRetrofitInstance().create(GetDataService.class);
        Call<BaseSubMajorGroup> call = service.getSubMajorGroups(major_id);
        call.enqueue(new Callback<BaseSubMajorGroup>() {

            @Override
            public void onResponse(Call<BaseSubMajorGroup> call, retrofit2.Response<BaseSubMajorGroup> response) {
                if (response.body()!=null) {
                    for (int i = 0; i < response.body().getSubGroups().size(); i++) {
                        menuCategories.add(response.body().getSubGroups().get(i));
                        OfflineSubMajorGroup offlineSubMajorGroup=new OfflineSubMajorGroup(response.body().getSubGroups().get(i));
                        offlineSubMajorGroup.setMajor_id(major_id);
                        if(MainActivity.getSubMajorGroupRow(response.body().getSubGroups().get(i))==null){
                            offlineSubMajorGroup.save();
                        }
                    }

                    subMajorGroupAdapter = new SubMajorGroupAdapter(getActivity(), menuCategories);
                    menuCategoryRecyclerView.setLayoutManager(new GridLayoutManager(getActivity(),2));
                    menuCategoryRecyclerView.setAdapter(subMajorGroupAdapter);
                }
                else {
                    Toast.makeText(getActivity(), "Error fetching list! Please try again later", Toast.LENGTH_SHORT).show();
                }
                pd.dismiss();
            }

            @Override
            public void onFailure(Call<BaseSubMajorGroup> call, Throwable t) {
                pd.dismiss();
                Toast.makeText(MainActivity.activity, "Something went wrong...Please try later!", Toast.LENGTH_SHORT).show();
            }
        });


    }
    @Override
    public void onStart() {
        super.onStart();
        Bundle args = getArguments();
        if (args != null) {
            major_id=args.getString("major_id");
            menuCategories = new ArrayList<>();
           /* if(isNetworkAvailable()) {
                pd.show();
                callSubMajorGroups();
            }else{*/
                if (MainActivity.getAllSubMajorGroups(major_id).size() != 0) {
                    menuCategories.clear();
                    List<OfflineSubMajorGroup> subMajorGroups;
                    subMajorGroups=MainActivity.getAllSubMajorGroups(major_id);
                    for (int i = 0; i <subMajorGroups.size(); i++) {
                        SubMajorGroup majorGroup = new SubMajorGroup();
                        majorGroup.setId(subMajorGroups.get(i).getSub_major_id());
                        majorGroup.setAImage(subMajorGroups.get(i).getA_image());
                        majorGroup.setName(subMajorGroups.get(i).getName());
                        majorGroup.setCreatedAt(subMajorGroups.get(i).getCreated_at());
                        majorGroup.setUpdatedAt(subMajorGroups.get(i).getUpdated_at());
                        majorGroup.setDescription(subMajorGroups.get(i).getDescription());
                        majorGroup.setMenuItems(subMajorGroups.get(i).getMenu_items());
                        majorGroup.setMajorGroupName(subMajorGroups.get(i).getMajor_group_name());
                        menuCategories.add(majorGroup);
                    }

                    subMajorGroupAdapter = new SubMajorGroupAdapter(getActivity(), menuCategories);
                    GridLayoutManager layoutManager = new GridLayoutManager(getActivity(), 2);
                    menuCategoryRecyclerView.setLayoutManager(layoutManager);
                    menuCategoryRecyclerView.setAdapter(subMajorGroupAdapter);
                }
                else{
                    if (isNetworkAvailable()) {
                        pd.show();
                        callSubMajorGroups();
                    } else {
                        Toast.makeText(getActivity(), "No items in the list! Try synchronizing again for updating items", Toast.LENGTH_LONG).show();
                    }
                }

        }
    }

    private boolean isNetworkAvailable() {
        ConnectivityManager connectivityManager
                = (ConnectivityManager) getActivity().getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }

}
