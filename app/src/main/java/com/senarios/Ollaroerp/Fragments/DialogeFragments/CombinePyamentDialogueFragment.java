package com.senarios.Ollaroerp.Fragments.DialogeFragments;

import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.senarios.Ollaroerp.Fragments.CombineBillFragment;
import com.senarios.Ollaroerp.Fragments.PaymentMethodFragment;
import com.senarios.Ollaroerp.MainActivity;
import com.senarios.Ollaroerp.Models.Order;
import com.senarios.Ollaroerp.R;
import com.senarios.Ollaroerp.Utitlities.FragmentTags;

public class CombinePyamentDialogueFragment extends DialogFragment {
    private Button yes, no;
    private ImageView cross;
    private String myObject;
    private Order order;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.combine_payment_dialoge, container, false);
        cross=v.findViewById(R.id.cross);
        cross.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
            }
        });
        initView(v);
        return v;
    }

    private void initView(View v) {
        yes = v.findViewById(R.id.btn_yes);
        no = v.findViewById(R.id.btn_no);
        setClickListeners();
    }

    private void setClickListeners() {
        yes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MainActivity.activity.changeFragment(R.id.fragment_container, new CombineBillFragment(), FragmentTags.COMBINEBILL);
                dismiss();
            }
        });
        no.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                PaymentMethodFragment checkoutFragment = new PaymentMethodFragment();
                Bundle args = new Bundle();
                GsonBuilder builder = new GsonBuilder();
                builder.excludeFieldsWithModifiers();
                Gson gson = builder.create();
                args.putString("myOrder", gson.toJson(order));
                checkoutFragment.setArguments(args);
                MainActivity.activity.getSupportFragmentManager().beginTransaction().add(new PaymentMethodFragment(),FragmentTags.PAYMENTMETHOD).addToBackStack(FragmentTags.PAYMENTMETHOD).commit();
                dismiss();
            }
        });
    }

    @Override
    public void onStart() {
        super.onStart();
        Bundle args = getArguments();
        if (args != null) {
            try {
                myObject = args.getString("myOrder");
                order = new Gson().fromJson(myObject, Order.class);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    public void onBackPressed(){
        dismiss();
    }

}
