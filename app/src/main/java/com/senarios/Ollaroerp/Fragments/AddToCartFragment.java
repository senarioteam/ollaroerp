package com.senarios.Ollaroerp.Fragments;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.activeandroid.query.Delete;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.senarios.Ollaroerp.Adapters.CartItemAdapter;
import com.senarios.Ollaroerp.Fragments.DialogeFragments.ChooseTableDialogueFragment;
import com.senarios.Ollaroerp.MainActivity;
import com.senarios.Ollaroerp.Models.CartItemModel;
import com.senarios.Ollaroerp.Models.OfflineOrders;
import com.senarios.Ollaroerp.Models.Order;
import com.senarios.Ollaroerp.R;
import com.senarios.Ollaroerp.Utitlities.FragmentTags;
import com.senarios.Ollaroerp.Utitlities.PrefUtils;
import com.senarios.Ollaroerp.Utitlities.TinyDB;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.Modifier;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;


public class AddToCartFragment extends Fragment implements CartBalanceCalculator {

    private TextView cartCount, cartSubtotal, cartVat, cateringLevy, cartTotal;
    private RecyclerView cartItems;
    private EditText etComments;
    Button addToBill, checkout, chooseTable;
    private LinearLayout addProduct;
    public static String table = "";
    private static String tableName = "";
    float total, vat, lavy = 0;
    private ArrayList<CartItemModel> cartItemModels;
    public static String section;
    private CartItemAdapter cartItemAdapter;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_add_to_cart, container, false);
        initView(rootView);
        return rootView;
    }

    private void initView(View rootView) {
        cartCount = rootView.findViewById(R.id.tv_cart_items_count);
        cartSubtotal = rootView.findViewById(R.id.tv_cart_subtotal);
        cartVat = rootView.findViewById(R.id.tv_cart_vat);
        cateringLevy = rootView.findViewById(R.id.tv_cart_levy);
        cartTotal = rootView.findViewById(R.id.tv_cart_total);
        cartItems = rootView.findViewById(R.id.rv_cart_items);
        addToBill = rootView.findViewById(R.id.btn_add_bill);
        checkout = rootView.findViewById(R.id.btn_checkout);
        addProduct = rootView.findViewById(R.id.layout_add_product);
        etComments = rootView.findViewById(R.id.et_comments);
        chooseTable = rootView.findViewById(R.id.btn_choose_table);
       /* if (isNetworkAvailable()){
            addToBill.setVisibility(View.VISIBLE);
        }else{
            addToBill.setVisibility(View.GONE);
        }*/
        setClickListeners();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        cartItemModels = (ArrayList<CartItemModel>) MainActivity.activity.getAll();
        if (MainActivity.activity.getAll().size() == 1) {
            cartCount.setText(Integer.toString(MainActivity.activity.getAll().size()) + " Item");
        } else if (MainActivity.activity.getAll().size() > 1) {
            cartCount.setText(Integer.toString(MainActivity.activity.getAll().size()) + " Items");
        } else {
            cartCount.setText("No Item");
        }
        List<CartItemModel> models = MainActivity.activity.getAll();
        for (int i = 0; i < models.size(); i++) {
            total = total + Float.parseFloat(models.get(i).getQuantity()) * Float.parseFloat(models.get(i).getPrice());
        }

        cartVat.setText("KSH " + total * 16 / 100);
        vat = (total * 16 / 100);
        cateringLevy.setText("KSH " + total * 2 / 100);
        lavy = (total * 2 / 100);
        cartSubtotal.setText("KSH " + String.format("%.2f", total - vat - lavy));
        cartTotal.setText("KSH " + Float.toString((total)));
        cartItems.setLayoutManager(new LinearLayoutManager(getActivity()));
        cartItemAdapter = new CartItemAdapter(getActivity(), cartItemModels, this);
        cartItems.setAdapter(cartItemAdapter);
    }

    private void setClickListeners() {
        addProduct.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MainActivity.activity.changeFragment(R.id.fragment_container, new MenuFragment(), FragmentTags.MENU);
            }
        });
        checkout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (MainActivity.activity.getAll().size() == 0) {
                    Toast.makeText(getActivity(), "Nothing in your cart", Toast.LENGTH_SHORT).show();
                } else if (table.equalsIgnoreCase("") ) {
                    Toast.makeText(getActivity(), "Please Select Table", Toast.LENGTH_SHORT).show();
                } else {
                    if (isNetworkAvailable()) {
                        Order order = new Order();
                        order.setCondiments("");
                        order.setItem_description(etComments.getText().toString());
                        order.setNo_of_guests("0");
                        order.setTable_no(tableName);
                        order.setRestaurant(PrefUtils.getUserModel(getActivity()).getRestaurant());
                        order.setWaiter((PrefUtils.getUserModel(getActivity()).getName()));
                        order.setTotal_amount(cartTotal.getText().toString());
                        order.setSection(section);
                        List<CartItemModel> models = MainActivity.activity.getAll();
                        JSONArray jsonArray = new JSONArray();
                        for (int i = 0; i < models.size(); i++) {
                            JSONObject myJsonObject = new JSONObject();
                            try {
                                myJsonObject.put("comments", models.get(i).getComments());
                                myJsonObject.put("item_name", models.get(i).getItemName());
                                myJsonObject.put("item_quantity", models.get(i).getQuantity());
                                myJsonObject.put("image", models.get(i).getImage());
                                myJsonObject.put("price", models.get(i).getPrice());
                                myJsonObject.put("print_class_id", models.get(i).getPrintClassId());
                                myJsonObject.put("general_item_id", models.get(i).getGeneral_item_id());

                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                            jsonArray.put(myJsonObject);
                        }
                        SimpleDateFormat dateFormat = new SimpleDateFormat("KK:mm:ss a");
                        order.setDate_time(dateFormat.format(Calendar.getInstance().getTime()));
                        CheckoutFragment checkoutFragment = new CheckoutFragment();
                        Bundle args = new Bundle();
                        GsonBuilder builder = new GsonBuilder();
                        builder.excludeFieldsWithModifiers();
                        Gson gson = builder.create();
                        args.putString("myObject", gson.toJson(order));
                        args.putString("list", jsonArray.toString());
                        checkoutFragment.setArguments(args);
                        MainActivity.activity.changeFragment(R.id.fragment_container, checkoutFragment, FragmentTags.CHECKOUT);
                    } else {
                        Order order = new Order();
                        order.setCondiments("");
                        order.setItem_description(etComments.getText().toString());
                        order.setNo_of_guests("0");
                        order.setTable_no(tableName);
                        order.setRestaurant(PrefUtils.getUserModel(getActivity()).getRestaurant());
                        order.setWaiter((PrefUtils.getUserModel(getActivity()).getName()));
                        order.setTotal_amount(cartTotal.getText().toString());
                        order.setSection(section);
                        List<CartItemModel> models = MainActivity.activity.getAll();
                        JSONArray jsonArray = new JSONArray();
                        for (int i = 0; i < models.size(); i++) {
                            JSONObject myJsonObject = new JSONObject();
                            try {
                                myJsonObject.put("comments", models.get(i).getComments());
                                myJsonObject.put("item_name", models.get(i).getItemName());
                                myJsonObject.put("item_quantity", models.get(i).getQuantity());
                                myJsonObject.put("image", models.get(i).getImage());
                                myJsonObject.put("price", models.get(i).getPrice());
                                myJsonObject.put("print_class_id", models.get(i).getPrintClassId());
                                myJsonObject.put("general_item_id", models.get(i).getGeneral_item_id());

                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                            jsonArray.put(myJsonObject);
                        }
                        SimpleDateFormat dateFormat = new SimpleDateFormat("KK:mm:ss a");
                        order.setDate_time(dateFormat.format(Calendar.getInstance().getTime()));

                        GsonBuilder builder = new GsonBuilder();
                        builder.excludeFieldsWithModifiers();
                        builder.excludeFieldsWithModifiers(Modifier.FINAL, Modifier.TRANSIENT, Modifier.STATIC);

                        Gson gson = builder.create();

                        OfflineOrders offlineOrders = new OfflineOrders();
                        offlineOrders.setItems(jsonArray.toString());
                        offlineOrders.setOrder(gson.toJson(order));
                        TinyDB tinyDB = new TinyDB(getActivity());
                        try {
                            MainActivity.offlineOrders = tinyDB.getListString("OfflineOrders");
                        }catch (Exception e){
                            e.printStackTrace();
                        }
                        MainActivity.offlineOrders.add(gson.toJson(offlineOrders));
                        PrefUtils.putString(getActivity(),"hasOrders","true");
                        tinyDB.putListString("OfflineOrders", MainActivity.offlineOrders);
                        Toast.makeText(getActivity(), "Order Added To Queue", Toast.LENGTH_SHORT).show();
                        new Delete().from(CartItemModel.class).execute();
                        MainActivity.activity.updateItemCount();
                        MainActivity.activity.changeFragment(R.id.fragment_container, new HomeFragment(), FragmentTags.HOME);
                    }
                }
            }
        });
        chooseTable.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FragmentTransaction ft = getFragmentManager().beginTransaction();
                Fragment prev = getFragmentManager().findFragmentByTag(FragmentTags.CHOOSETABLE);
                if (prev != null) {
                    ft.remove(prev);
                }
                ft.addToBackStack(null);
                DialogFragment dialogFragment;
                if (tableName.equalsIgnoreCase("")) {
                    dialogFragment = new ChooseTableDialogueFragment();
                } else {
                    dialogFragment = new ChooseTableDialogueFragment();
                    Bundle args = new Bundle();
                    args.putString("table", tableName);
                    dialogFragment.setArguments(args);
                }
                dialogFragment.show(ft, FragmentTags.CHOOSETABLE);
                dialogFragment.setCancelable(true);
            }
        });
        addToBill.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (MainActivity.activity.getAll().size() == 0) {
                    Toast.makeText(getActivity(), "Nothing in your cart", Toast.LENGTH_SHORT).show();
                } else if (table.equalsIgnoreCase("") ) {
                    Toast.makeText(getActivity(), "Please Select Table", Toast.LENGTH_SHORT).show();
                } else {
                    Order order = new Order();
                    order.setCondiments("");
                    order.setItem_description(etComments.getText().toString());
                    order.setNo_of_guests("0");
                    order.setTable_no(tableName);
                    order.setRestaurant(PrefUtils.getUserModel(getActivity()).getRestaurant());
                    order.setWaiter((PrefUtils.getUserModel(getActivity()).getName()));
                    order.setTotal_amount(cartTotal.getText().toString());
                    order.setSection(section);
                    List<CartItemModel> models = MainActivity.activity.getAll();
                    JSONArray jsonArray = new JSONArray();
                    for (int i = 0; i < models.size(); i++) {
                        JSONObject myJsonObject = new JSONObject();
                        try {
                            myJsonObject.put("comments",models.get(i).getComments());
                            myJsonObject.put("item_name", models.get(i).getItemName());
                            myJsonObject.put("item_quantity", models.get(i).getQuantity());
                            myJsonObject.put("image", models.get(i).getImage());
                            myJsonObject.put("price", models.get(i).getPrice());
                            myJsonObject.put("print_class_id", models.get(i).getPrintClassId());
                            myJsonObject.put("general_item_id", models.get(i).getGeneral_item_id());
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        jsonArray.put(myJsonObject);
                    }
                    String list = new Gson().toJson(jsonArray);
                    SimpleDateFormat dateFormat = new SimpleDateFormat("KK:mm:ss a");
                    order.setDate_time(dateFormat.format(Calendar.getInstance().getTime()));
                    ChooseBillFragment chooseBillFragment = new ChooseBillFragment();
                    Bundle args = new Bundle();
                    GsonBuilder builder = new GsonBuilder();
                    builder.excludeFieldsWithModifiers();
                    Gson gson = builder.create();
                    args.putString("myObject", gson.toJson(order));
                    args.putString("list", jsonArray.toString());
                    chooseBillFragment.setArguments(args);
                    MainActivity.activity.changeFragment(R.id.fragment_container, chooseBillFragment, FragmentTags.CHOOSEBILL);
                }
            }
        });
    }


    public static void bringVlues(String table1, String section1, String tableName1) {
        table = table1;
        tableName = tableName1;
        section = section1;
    }

    @Override
    public void deductPrice(String price) {
        total = total - Float.parseFloat(price);
        cartSubtotal.setText("KSH " + Float.toString(total));
        cartVat.setText("KSH " + total * 16 / 100);
        vat = (total * 16 / 100);
        cateringLevy.setText("KSH " + total * 2 / 100);
        lavy = (total * 2 / 100);
        cartTotal.setText("KSH " + String.format("%.2f", (total)));
    }

    @Override
    public void inductPrice(String price) {
        total = total + Float.parseFloat(price);
        cartSubtotal.setText("KSH " + Float.toString(total));
        cartVat.setText("KSH " + total * 16 / 100);
        vat = (total * 16 / 100);
        cateringLevy.setText("KSH " + total * 2 / 100);
        lavy = (total * 2 / 100);
        cartTotal.setText("KSH " + String.format("%.2f", (total)));
    }

    @Override
    public void del(int pos, String price) {
        cartItemModels.get(pos).delete();
        cartItemModels.remove(pos);
        cartItemAdapter.notifyDataSetChanged();
        if (MainActivity.activity.getAll().size() == 1) {
            cartCount.setText(Integer.toString(MainActivity.activity.getAll().size()) + " Item");
        } else if (MainActivity.activity.getAll().size() > 1) {
            cartCount.setText(Integer.toString(MainActivity.activity.getAll().size()) + " Items");
        } else {
            cartCount.setText("No Item");
        }
        MainActivity.activity.updateItemCount();
        total = total - Float.parseFloat(price);
        cartSubtotal.setText("KSH " + Float.toString(total));
        cartVat.setText("KSH " + total * 16 / 100);
        vat = (total * 16 / 100);
        cateringLevy.setText("KSH " + total * 2 / 100);
        lavy = (total * 2 / 100);
        cartTotal.setText("KSH " + String.format("%.2f", (total + vat + lavy)));
    }

    @Override
    public void onStop() {
        table = "";

        section = "";
        super.onStop();
    }

    private boolean isNetworkAvailable() {
        ConnectivityManager connectivityManager
                = (ConnectivityManager) getActivity().getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }
}
