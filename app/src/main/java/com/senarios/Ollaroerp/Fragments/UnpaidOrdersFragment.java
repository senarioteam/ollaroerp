package com.senarios.Ollaroerp.Fragments;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.kaopiz.kprogresshud.KProgressHUD;
import com.senarios.Ollaroerp.Adapters.UnpaidOrdersAdapter;
import com.senarios.Ollaroerp.MainActivity;
import com.senarios.Ollaroerp.Models.BasePostPaidOrder;
import com.senarios.Ollaroerp.Models.OfflinePostOrderItems;
import com.senarios.Ollaroerp.Models.PostOrderItems;
import com.senarios.Ollaroerp.R;
import com.senarios.Ollaroerp.Utitlities.PrefUtils;
import com.senarios.Ollaroerp.Utitlities.Retrofit.GetDataService;
import com.senarios.Ollaroerp.Utitlities.Retrofit.RetrofitClientInstance;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;


public class UnpaidOrdersFragment extends Fragment {


    private RecyclerView unpaidOrdersRecyclerView;
    KProgressHUD pd;
    ArrayList<PostOrderItems> dummyList=new ArrayList<>();

    public UnpaidOrdersFragment() {

    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_unpaid_orders, container, false);
        initView(rootView);
        return rootView;
    }

    private void initView(View rootView) {
        unpaidOrdersRecyclerView = rootView.findViewById(R.id.rv_unpaid_orders);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        pd = pd.create(getActivity())
                .setStyle(KProgressHUD.Style.SPIN_INDETERMINATE)
                .setLabel("Please Wait")
                .setCancellable(false)
                .setAnimationSpeed(2)
                .setDimAmount(0.5f);


        if (isNetworkAvailable()){
            getUnPaidOrders();

        }
        else{
            pd.show();
            List<OfflinePostOrderItems> offlinePostOrderItems=new ArrayList<>();
            offlinePostOrderItems=MainActivity.getAllPostOrderItems();
            List<PostOrderItems>postOrderItemslist=new ArrayList<>();
            for (int i=0;i<offlinePostOrderItems.size();i++){
                PostOrderItems postOrderItems=new PostOrderItems();
                postOrderItems.setId(offlinePostOrderItems.get(i).getPostorderid());
                postOrderItems.setId(offlinePostOrderItems.get(i).getBill_id());
                postOrderItems.setdate_time(offlinePostOrderItems.get(i).getDate_time());
                postOrderItems.settotal_amount(offlinePostOrderItems.get(i).getTotal_amount());
                postOrderItems.settable_no(offlinePostOrderItems.get(i).getTable_no());
                postOrderItems.setStatus(offlinePostOrderItems.get(i).getStatus());
                postOrderItems.setno_of_guests(offlinePostOrderItems.get(i).getNo_of_guests());
                postOrderItems.setitem_description(offlinePostOrderItems.get(i).getItem_description());
                postOrderItemslist.add(postOrderItems);
            }
            unpaidOrdersRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
            unpaidOrdersRecyclerView.setAdapter(new UnpaidOrdersAdapter(getActivity(),postOrderItemslist ));
            pd.dismiss();



        }

    }


        public void getUnPaidOrders() {
            pd.show();
            GetDataService service = RetrofitClientInstance.getRetrofitInstance().create(GetDataService.class);
            Call<BasePostPaidOrder> call = service.getUnPaidOrders(String.valueOf(PrefUtils.getUserModel(getActivity()).getId()));
            call.enqueue(new Callback<BasePostPaidOrder>() {

                @Override
                public void onResponse(Call<BasePostPaidOrder> call, retrofit2.Response<BasePostPaidOrder> response) {
                    if (response.body() != null) {
                        if(response.body().getPaidOrders().size()>0) {
                            dummyList.addAll(response.body().getPaidOrders());

                            unpaidOrdersRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
                            unpaidOrdersRecyclerView.setAdapter(new UnpaidOrdersAdapter(getActivity(), response.body().getPaidOrders()));
                        }

                    } else {
                        Toast.makeText(getActivity(), "Some Error Occured! Please Try Again", Toast.LENGTH_SHORT).show();
                    }
                    pd.dismiss();
                }

                @Override
                public void onFailure(Call<BasePostPaidOrder> call, Throwable t) {
                    pd.dismiss();
                   // Toast.makeText(MainActivity.activity, "Something went wrong...Please try later!", Toast.LENGTH_SHORT).show();
                }
            });


        }

    private boolean isNetworkAvailable() {
        ConnectivityManager connectivityManager
                = (ConnectivityManager) getActivity().getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }

}
