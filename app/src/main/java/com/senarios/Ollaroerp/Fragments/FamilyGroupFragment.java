package com.senarios.Ollaroerp.Fragments;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.kaopiz.kprogresshud.KProgressHUD;
import com.senarios.Ollaroerp.Adapters.FamilyGroupAdapter;
import com.senarios.Ollaroerp.MainActivity;
import com.senarios.Ollaroerp.Models.BaseFamilyGroup;
import com.senarios.Ollaroerp.Models.FamilyGroup;
import com.senarios.Ollaroerp.Models.OfflineFamilyGroup;
import com.senarios.Ollaroerp.R;
import com.senarios.Ollaroerp.Utitlities.Retrofit.GetDataService;
import com.senarios.Ollaroerp.Utitlities.Retrofit.RetrofitClientInstance;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;

public class FamilyGroupFragment extends Fragment {
    private RecyclerView subcatMenuRecyclerView;
    private FamilyGroupAdapter familyGroupAdapter;
    private ArrayList<FamilyGroup> familyGroupArrayList;
    KProgressHUD pd;
    String sub_major_id;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        View rootView=inflater.inflate(R.layout.fragment_subcategory_menu,container,false);
        initView(rootView);
        return rootView;
    }

    private void initView(View rootView){
        subcatMenuRecyclerView=rootView.findViewById(R.id.rv_subcat_menu);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        pd = pd.create(getActivity())
                .setStyle(KProgressHUD.Style.SPIN_INDETERMINATE)
                .setLabel("Please Wait")
                .setCancellable(false)
                .setAnimationSpeed(2)
                .setDimAmount(0.5f);


    }

    @Override
    public void onStart() {
        super.onStart();
        Bundle args = getArguments();
        if (args != null) {
            sub_major_id=args.getString("group_id");

            familyGroupArrayList=new ArrayList<>();
           /* if(isNetworkAvailable()) {
                pd.show();
                callFamilyGroup();
            }else{*/
                if (MainActivity.getAllFamGroups(sub_major_id).size() != 0) {
                    familyGroupArrayList.clear();
                    List<OfflineFamilyGroup> familyGroups;
                    familyGroups=MainActivity.getAllFamGroups(sub_major_id);
                    for (int i = 0; i <familyGroups.size(); i++) {
                        FamilyGroup majorGroup = new FamilyGroup();
                        majorGroup.setId(familyGroups.get(i).getFam_group_id());
                        majorGroup.seta_image(familyGroups.get(i).getA_image());
                        majorGroup.setName(familyGroups.get(i).getName());
                        majorGroup.setcreated_at(familyGroups.get(i).getCreated_at());
                        majorGroup.setupdated_a(familyGroups.get(i).getUpdated_at());
                        majorGroup.setmenu_items_group_id(familyGroups.get(i).getMenuItemsGroupId());
                        majorGroup.setgl_account(familyGroups.get(i).getGl_account());
                        familyGroupArrayList.add(majorGroup);
                    }

                    subcatMenuRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
                    familyGroupAdapter =new FamilyGroupAdapter(getActivity(),familyGroupArrayList);
                    subcatMenuRecyclerView.setAdapter(familyGroupAdapter);
                }
                else{
                    if (isNetworkAvailable()) {
                        pd.show();
                        callFamilyGroup();
                    } else {
                        Toast.makeText(getActivity(), "No items in the list! Try synchronizing again for updating items", Toast.LENGTH_LONG).show();
                    }                }

        }
    }

    public void callFamilyGroup() {
        GetDataService service = RetrofitClientInstance.getRetrofitInstance().create(GetDataService.class);
        Call<BaseFamilyGroup> call = service.getFamilyGroups(sub_major_id);
        call.enqueue(new Callback<BaseFamilyGroup>() {

            @Override
            public void onResponse(Call<BaseFamilyGroup> call, retrofit2.Response<BaseFamilyGroup> response) {
                if (response.body()!=null) {
                    for (int i = 0; i < response.body().getFamilyGroups().size(); i++) {
                        familyGroupArrayList.add(response.body().getFamilyGroups().get(i));
                        OfflineFamilyGroup offlineFamilyGroup=new OfflineFamilyGroup(response.body().getFamilyGroups().get(i));
                        if(MainActivity.getFamGroupRow(response.body().getFamilyGroups().get(i))==null){
                            offlineFamilyGroup.save();
                        }
                    }
                    subcatMenuRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
                    familyGroupAdapter =new FamilyGroupAdapter(getActivity(),familyGroupArrayList);
                    subcatMenuRecyclerView.setAdapter(familyGroupAdapter);
                }
                else {
                    Toast.makeText(getActivity(), "Error fetching list! Please try again later", Toast.LENGTH_SHORT).show();
                }
                pd.dismiss();
            }

            @Override
            public void onFailure(Call<BaseFamilyGroup> call, Throwable t) {
                pd.dismiss();
                Toast.makeText(MainActivity.activity, "Something went wrong...Please try later!", Toast.LENGTH_SHORT).show();
            }
        });


    }
    private boolean isNetworkAvailable() {
        ConnectivityManager connectivityManager
                = (ConnectivityManager) getActivity().getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }
}
