package com.senarios.Ollaroerp.Fragments;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.senarios.Ollaroerp.Adapters.TablesAdapter;
import com.senarios.Ollaroerp.MainActivity;
import com.senarios.Ollaroerp.Models.CombineBillItemModel;
import com.senarios.Ollaroerp.Models.CombineBillOrderModel;
import com.senarios.Ollaroerp.Models.CombineBillsTableModel;
import com.senarios.Ollaroerp.R;
import com.senarios.Ollaroerp.Utitlities.FragmentTags;

import java.util.ArrayList;

public class CombineBillFragment extends Fragment {
    private RecyclerView tablesRecyclerView;
    private ArrayList<CombineBillsTableModel> tables;
    private ArrayList<CombineBillOrderModel> orderModels;
    private ArrayList<CombineBillItemModel> itemModels;
    private Button done;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View rootView = inflater.inflate(R.layout.fragment_combine_bills, container, false);
        initView(rootView);
        return rootView;
    }

    private void initView(View rootView) {
        tables = new ArrayList<>();
        itemModels = new ArrayList<>();
        orderModels = new ArrayList<>();
        done = rootView.findViewById(R.id.btn_done);
        tablesRecyclerView = rootView.findViewById(R.id.rv_tables);
        setClickListeners();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        for (int i = 0; i < 2; i++) {
            CombineBillItemModel combineBillItemModel = new CombineBillItemModel();
            combineBillItemModel.setName("Red Bull");
            combineBillItemModel.setPrice("KSH 200");
            combineBillItemModel.setQuantity("2 QNT");
            itemModels.add(combineBillItemModel);

        }

        for (int i = 0; i < 2; i++) {
            CombineBillOrderModel combineBillItemModel = new CombineBillOrderModel();
            combineBillItemModel.setOrderId("24300");
            combineBillItemModel.setTotal("KSH 2000");
            combineBillItemModel.setCombineBillItemModels(itemModels);
            orderModels.add(combineBillItemModel);
        }

        for (int i = 0; i < 4; i++) {
            CombineBillsTableModel combineBillItemModel = new CombineBillsTableModel();
            combineBillItemModel.setTableId(Integer.toString(i) + "1");
            combineBillItemModel.setCombineBillOrderModels(orderModels);
            tables.add(combineBillItemModel);
        }

        tablesRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        tablesRecyclerView.setAdapter(new TablesAdapter(getActivity(), tables));
    }

    private void setClickListeners() {
        done.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MainActivity.activity.changeFragment(R.id.fragment_container,new ConfirmCombineFragment(),FragmentTags.CONFIRMCOMBINE);
            }
        });
    }
}
