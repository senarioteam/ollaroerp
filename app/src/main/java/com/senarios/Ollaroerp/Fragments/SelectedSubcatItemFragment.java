package com.senarios.Ollaroerp.Fragments;


import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.google.gson.Gson;
import com.kaopiz.kprogresshud.KProgressHUD;
import com.senarios.Ollaroerp.MainActivity;
import com.senarios.Ollaroerp.Models.CartItemModel;
import com.senarios.Ollaroerp.Models.ItemModel;
import com.senarios.Ollaroerp.R;
import com.senarios.Ollaroerp.Utitlities.FragmentTags;

/**
 * A simple {@link Fragment} subclass.
 */
public class SelectedSubcatItemFragment extends Fragment {

    private TextView itemName, itemQuantity, itemPrice, increaseQuantity, decreaseQuantity, addTOCart, buy;
    private ImageView itemImage;
    private EditText etComments;
    private String myObject;
    ItemModel itemModel;
    int quantity;
    KProgressHUD pd;

    public SelectedSubcatItemFragment() {

    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        pd = pd.create(getActivity())
                .setStyle(KProgressHUD.Style.SPIN_INDETERMINATE)
                .setLabel("Please Wait")
                .setCancellable(false)
                .setAnimationSpeed(2)
                .setDimAmount(0.5f);
    }

    @Override
    public void onStart() {
        super.onStart();
        Bundle args = getArguments();
        if (args != null) {
            try {
                myObject = args.getString("myObject");
                itemModel = new Gson().fromJson(myObject, ItemModel.class);
                setValues();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View rootView = inflater.inflate(R.layout.fragment_selected_subcat_item, container, false);
        initView(rootView);
        return rootView;
    }

    private void initView(View rootView) {
        itemImage = rootView.findViewById(R.id.item_image);
        itemName = rootView.findViewById(R.id.item_name);
        itemPrice = rootView.findViewById(R.id.item_price);
        itemQuantity = rootView.findViewById(R.id.item_quantity);
        increaseQuantity = rootView.findViewById(R.id.increase_quantity);
        decreaseQuantity = rootView.findViewById(R.id.decrease_quantity);
        addTOCart = rootView.findViewById(R.id.tv_add_to_cart);
        buy = rootView.findViewById(R.id.tv_buy);
        etComments = rootView.findViewById(R.id.et_comments);
        setClickListeners();
    }

    private void setClickListeners() {
        increaseQuantity.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                quantity++;
                itemQuantity.setText(Integer.toString(quantity));
            }
        });
        decreaseQuantity.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!itemQuantity.getText().toString().equalsIgnoreCase("1")) {
                    try {
                        quantity = Integer.parseInt(itemQuantity.getText().toString());
                        quantity--;
                        itemQuantity.setText(Integer.toString(quantity));
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }
        });
        buy.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                CartItemModel cartItemModel=new CartItemModel();
                cartItemModel.setItemName(itemModel.getName());
                cartItemModel.setQuantity(itemQuantity.getText().toString());
                cartItemModel.setDesc(itemModel.getDescription());
                cartItemModel.setComments(etComments.getText().toString().trim());
                cartItemModel.setImage(itemModel.getAImage());
                cartItemModel.setPrintClassId(itemModel.getprint_class_id());
                cartItemModel.setPrice(itemModel.getPrice());
                cartItemModel.setGeneral_item_id(String.valueOf(itemModel.getId()));
                CartItemModel itemModel=MainActivity.activity.getRow(cartItemModel);
                if(itemModel!=null){
                    int quant=Integer.parseInt(itemModel.getQuantity())+Integer.parseInt(itemQuantity.getText().toString());
                    itemModel.setQuantity(Integer.toString(quant));
                    itemModel.save();
                    Toast.makeText(getActivity(), "Item Added to cart", Toast.LENGTH_SHORT).show();
                    MainActivity.activity.getSupportFragmentManager().beginTransaction().add(R.id.fragment_container,new AddToCartFragment(),FragmentTags.CARTFRAGMENT).addToBackStack(FragmentTags.CARTFRAGMENT).commitAllowingStateLoss();
                }
                else{
                    cartItemModel.save();
                    MainActivity.activity.updateItemCount();
                    MainActivity.activity.getSupportFragmentManager().beginTransaction().add(R.id.fragment_container,new AddToCartFragment(),FragmentTags.CARTFRAGMENT).addToBackStack(FragmentTags.CARTFRAGMENT).commitAllowingStateLoss();
                }
            }
        });
        addTOCart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                pd.show();
                CartItemModel cartItemModel=new CartItemModel();
                cartItemModel.setComments(etComments.getText().toString().trim());
                cartItemModel.setItemName(itemModel.getName());
                cartItemModel.setQuantity(itemQuantity.getText().toString());
                cartItemModel.setDesc(itemModel.getDescription());
                cartItemModel.setImage(itemModel.getAImage());
                cartItemModel.setPrice(itemModel.getPrice());
                cartItemModel.setPrintClassId(itemModel.getprint_class_id());
                cartItemModel.setGeneral_item_id(String.valueOf(itemModel.getId()));
                CartItemModel itemModel=MainActivity.activity.getRow(cartItemModel);
                if(itemModel!=null){
                    int quant=Integer.parseInt(itemModel.getQuantity())+Integer.parseInt(itemQuantity.getText().toString());
                    itemModel.setQuantity(Integer.toString(quant));
                    itemModel.save();
                    Toast.makeText(getActivity(), "Item Added to cart", Toast.LENGTH_SHORT).show();
                }
                else{
                    cartItemModel.save();
                    MainActivity.activity.updateItemCount();
                    Toast.makeText(getActivity(), "Item Added to cart", Toast.LENGTH_SHORT).show();
                }
                getActivity().getSupportFragmentManager().popBackStack();
                pd.dismiss();
            }
        });
    }

    public void setValues() {
        itemName.setText(itemModel.getName());
        itemPrice.setText("KSH " + itemModel.getPrice());
        itemQuantity.setText("1");
        Glide.with(getActivity()).load("http://thesmitherp.digitalsystemsafrica.com/public/img/" + itemModel.getAImage()).into(itemImage);
    }

}
