package com.senarios.Ollaroerp.Fragments;

import android.os.Bundle;
import android.os.CountDownTimer;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Patterns;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.kaopiz.kprogresshud.KProgressHUD;
import com.senarios.Ollaroerp.R;

import static com.senarios.Ollaroerp.MainActivity.activity;

public class ForgotPasswordFragment extends Fragment {
    private ImageView ivClose;
    private EditText etEmail;
    private Button submit;
    private CountDownTimer timer;
    private KProgressHUD pd;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        View rootView = inflater.inflate(R.layout.fragment_forgot_password, container, false);
        initViews(rootView);
        return rootView;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
    }

    public void setClickListeners() {
        ivClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                activity.openLoginFragment();
            }
        });
        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String email = etEmail.getText().toString();
                if (email.equalsIgnoreCase("")) {
                    etEmail.setError("Please enter email address");
                } else if (!Patterns.EMAIL_ADDRESS.matcher(email).matches()) {
                    etEmail.setError("Please enter a valid email");
                } else {
                    pd.show();
                    timer = new CountDownTimer(3000, 1000) {
                        @Override
                        public void onTick(long millisUntilFinished) {

                        }

                        @Override
                        public void onFinish() {
                            pd.dismiss();
                            Toast.makeText(getActivity(), "Visit your email to get your new password", Toast.LENGTH_SHORT).show();
                        }
                    }.start();
                }
            }
        });
    }

    public void initViews(View rootView) {
        ivClose = rootView.findViewById(R.id.cross);
        etEmail = rootView.findViewById(R.id.email);
        submit = rootView.findViewById(R.id.submit);
        pd = pd.create(getActivity())
                .setStyle(KProgressHUD.Style.SPIN_INDETERMINATE)
                .setLabel("Just a moment")
                .setCancellable(false)
                .setAnimationSpeed(2)
                .setDimAmount(0.5f);
        setClickListeners();
        setClickListeners();
    }


}
