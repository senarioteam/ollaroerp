package com.senarios.Ollaroerp.Adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;


import com.senarios.Ollaroerp.Models.MenuItem;
import com.senarios.Ollaroerp.Models.SubMajorGroup;
import com.senarios.Ollaroerp.R;

import java.util.ArrayList;

public class SubMajorGroupAdapter extends RecyclerView.Adapter<SubMajorGroupAdapter.Holder> {

    ArrayList<SubMajorGroup> menuCategories;

    Context context;

    public SubMajorGroupAdapter(Context context, ArrayList<SubMajorGroup> menuList){
        menuCategories=menuList;
        this.context=context;
    }

    @NonNull
    @Override
    public Holder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View row = LayoutInflater.from(context).inflate(R.layout.menu_category_item, viewGroup, false);
        Holder holded = new Holder(row);
        return holded;
    }

    @Override
    public void onBindViewHolder(@NonNull Holder holder, int i) {
        SubMajorGroup currentModel=menuCategories.get(i);
        holder.itemName.setText(currentModel.getName());
        /*GridLayoutManager layoutManager = new GridLayoutManager(context, 2);
        layoutManager.setSpanSizeLookup(new GridLayoutManager.SpanSizeLookup() {
            @Override
            public int getSpanSize(int position) {

                return 1;
            }
        });*/
        MenuSubCategoryAdapter menuCategoryAdapter = new MenuSubCategoryAdapter(context, (ArrayList<MenuItem>) currentModel.getMenuItems());
        LinearLayoutManager linearLayoutManager=new LinearLayoutManager(context);
        linearLayoutManager.setOrientation(LinearLayoutManager.HORIZONTAL);
        holder.menuSubcategory.setLayoutManager(linearLayoutManager);
        holder.menuSubcategory.setAdapter(menuCategoryAdapter);
    }

    @Override
    public int getItemCount() {
        return menuCategories.size();
    }

    class Holder extends RecyclerView.ViewHolder {
        TextView itemName;
        RecyclerView menuSubcategory;

        public Holder(View itemView) {
            super(itemView);
            menuSubcategory=itemView.findViewById(R.id.rv_menu_subcat);
            itemName = itemView.findViewById(R.id.tv_menu_cat_name);
        }
    }
}