package com.senarios.Ollaroerp.Adapters;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.kaopiz.kprogresshud.KProgressHUD;
import com.senarios.Ollaroerp.CheckingOrdersInterface;
import com.senarios.Ollaroerp.Fragments.DialogeFragments.CancellationReasonDialogeFragment;
import com.senarios.Ollaroerp.MainActivity;
import com.senarios.Ollaroerp.Models.OrderItem;
import com.senarios.Ollaroerp.R;
import com.senarios.Ollaroerp.Utitlities.FragmentTags;
import com.senarios.Ollaroerp.Utitlities.PrefUtils;

import java.util.ArrayList;
import java.util.List;

public class OrderItemAdapter extends RecyclerView.Adapter<OrderItemAdapter.Holder> {
    private KProgressHUD pd;

    static  ArrayList<OrderItem> itemModels;
    CheckingOrdersInterface checkingOrdersInterface;
    String status;
    Context context;
    int index;

    public OrderItemAdapter(Context context, ArrayList<OrderItem> menuSubcatList, int index, String status) {
        itemModels = menuSubcatList;
        this.context = context;
        this.index = index;
        this.status = status;
        try {
            pd = pd.create(context)
                    .setStyle(KProgressHUD.Style.SPIN_INDETERMINATE)
                    .setLabel("Just a moment")
                    .setCancellable(false)
                    .setAnimationSpeed(2)
                    .setDimAmount(0.5f);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @NonNull
    @Override
    public Holder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View row = LayoutInflater.from(context).inflate(R.layout.inprogress_orders_item_layout, viewGroup, false);
        Holder holded = new Holder(row);
        return holded;
    }

    @Override
    public void onBindViewHolder(@NonNull final Holder holder, final int i) {

            if (itemModels.get(i).isVoid() || (itemModels.get(i).getVoid_request() != null && itemModels.get(i).getVoid_request().equalsIgnoreCase("yes")))
            //  if(itemModels.get(i).isVoid())
            {
               holder.cancelItem.setVisibility(View.INVISIBLE);
            }
            OrderItem currentModel = itemModels.get(i);
            holder.name.setText(currentModel.getName());
            holder.quantity.setText(currentModel.getQuantity() + "" +
                    " QNT");
            holder.check.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                    if (FullOrderAdapter.orders.get(index).getorder_items() != null) {
                        FullOrderAdapter.orders.get(index).getorder_items().get(i).setChecked(isChecked);
                    }

                }
            });
            holder.cancelItem.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    AlertDialog.Builder builder1 = new AlertDialog.Builder(context);
                    builder1.setMessage("Enter the quantity to void ");

                    builder1.setCancelable(true);
                    EditText input = new EditText(context);
                    //  input.setText("HI");
                    builder1.setView(input);
                    builder1.setPositiveButton(
                            "Yes",
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    String regexStr = "^[0-9]*$";
                                    String itemId = String.valueOf(currentModel.getId());
                                    String itemName = currentModel.getName();
                                    String orderId = String.valueOf(FullOrderAdapter.orders.get(index).getId());
                                    String waiterId = String.valueOf(PrefUtils.getUserModel(context).getId());
                                    String waiterName = FullOrderAdapter.orders.get(index).getWaiter();
                                    String amount = currentModel.getPrice();
                                    String quant = input.getText().toString();

                                    if (quant.trim().matches(regexStr) && !quant.isEmpty()) {
                                        int actual_quant = Integer.parseInt(quant);

                                        if (actual_quant <= (currentModel.getQuantity()) && actual_quant != 0) {

                                            int amount2 = Integer.parseInt(amount);
                                            amount2 = amount2 * actual_quant;
                                            amount = String.valueOf(amount2);

                                            //  currentModel.setQuantity((currentModel.getQuantity() - actual_quant));
                                            //      Toast.makeText(context, ""+currentModel.getQuantity(), Toast.LENGTH_SHORT).show();

                                            FragmentTransaction ft = MainActivity.activity.getSupportFragmentManager().beginTransaction();
                                            Fragment prev = MainActivity.activity.getSupportFragmentManager().findFragmentByTag(FragmentTags.CHOOSETABLE);
                                            if (prev != null) {
                                                ft.remove(prev);
                                            }
                                            ft.addToBackStack(null);
                                            DialogFragment dialogFragment;
                                            dialogFragment = new CancellationReasonDialogeFragment();
                                            Bundle args = new Bundle();
                                            args.putString("item_id", itemId);
                                            args.putString("item_name", itemName);
                                            args.putString("order_id", orderId);
                                            args.putString("waiter_id", waiterId);
                                            args.putString("waiter_name", waiterName);
                                            args.putString("amount", amount);
                                            args.putInt("quantity", actual_quant);
                                            args.putString("general_item_id", currentModel.getGeneral_item_id());
                                            dialogFragment.setArguments(args);
                                            dialogFragment.show(ft, FragmentTags.VOIDITEM);
                                            dialogFragment.setCancelable(true);

                                            dialog.cancel();
                                        } else {
                                            Toast.makeText(context, "Enter correct quantity", Toast.LENGTH_SHORT).show();
                                        }
                                    } else {
                                        Toast.makeText(context, "Enter correct quantity", Toast.LENGTH_SHORT).show();
                                    }
                                }
                            });

                    builder1.setNegativeButton(
                            "No",
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    dialog.cancel();
                                }
                            });

                    AlertDialog alert11 = builder1.create();
                    alert11.show();
                }
            });


}

    @Override
    public int getItemCount() {
            return itemModels.size();
    }

class Holder extends RecyclerView.ViewHolder {
    TextView name, quantity, comments;
    ImageView cancelItem;
    CheckBox check;

    public Holder(View itemView) {
        super(itemView);
        name = itemView.findViewById(R.id.item_name);
        quantity = itemView.findViewById(R.id.item_quantity);
        check = itemView.findViewById(R.id.check_completed);
        cancelItem = itemView.findViewById(R.id.iv_void);
    }
}
    private boolean isNetworkAvailable() {
        ConnectivityManager connectivityManager
                = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }

    public static List returndata(){

        return itemModels;
    }

}
