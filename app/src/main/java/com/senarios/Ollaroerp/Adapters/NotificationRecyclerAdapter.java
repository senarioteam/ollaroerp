package com.senarios.Ollaroerp.Adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.senarios.Ollaroerp.Models.NotificationModel;
import com.senarios.Ollaroerp.R;

import java.util.ArrayList;

public class NotificationRecyclerAdapter extends RecyclerView.Adapter<NotificationRecyclerAdapter.Holder> {
    ArrayList<NotificationModel> notificationModels;
    Context context;


    public NotificationRecyclerAdapter(Context context, ArrayList<NotificationModel> notificationModels) {
        this.notificationModels = notificationModels;
        this.context = context;

    }

    @NonNull
    @Override
    public Holder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View row = LayoutInflater.from(context).inflate(R.layout.notification_item_layout, viewGroup, false);
        Holder holded = new Holder(row);
        return holded;
    }

    @Override
    public void onBindViewHolder(@NonNull final Holder holder, final int pos) {
        NotificationModel notificationModel = notificationModels.get(pos);
        holder.itemName.setText(notificationModel.getItemName());
        holder.itemid.setText(notificationModel.getItemId());
        holder.id.setText(notificationModel.getOrderId());
        holder.waiterId.setText(notificationModel.getWaiterId());
        holder.waiterName.setText(notificationModel.getWaiterName());
    }

    @Override
    public int getItemCount() {
        return notificationModels.size();
    }

    class Holder extends RecyclerView.ViewHolder {
        TextView id, itemid, itemName, waiterId, waiterName;

        public Holder(View itemView) {
            super(itemView);
            id = itemView.findViewById(R.id.order_id);
            itemid = itemView.findViewById(R.id.item_id);
            itemName = itemView.findViewById(R.id.item_name);
            waiterId = itemView.findViewById(R.id.waiter_id);
            waiterName = itemView.findViewById(R.id.waiter_name);
        }
    }
}
