package com.senarios.Ollaroerp.Adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.senarios.Ollaroerp.CheckingOrdersInterface;
import com.senarios.Ollaroerp.Models.OrderItem;
import com.senarios.Ollaroerp.R;

import java.util.List;

public class PaidOrderItemAdapter extends RecyclerView.Adapter<PaidOrderItemAdapter.Holder> {
    List<OrderItem> itemModels;
    CheckingOrdersInterface checkingOrdersInterface;
    String status;
    Context context;
    int index;

    public PaidOrderItemAdapter(Context context, List<OrderItem> menuSubcatList) {
        itemModels = menuSubcatList;
        this.context = context;
    }

    @NonNull
    @Override
    public Holder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View row = LayoutInflater.from(context).inflate(R.layout.paid_orders_item_layout, viewGroup, false);
        Holder holded = new Holder(row);
        return holded;
    }

    @Override
    public void onBindViewHolder(@NonNull final Holder holder, final int i) {
        final OrderItem currentModel = itemModels.get(i);
        holder.name.setText(currentModel.getName());
        holder.quantity.setText(currentModel.getQuantity() + "" +
                " QNT");
        holder.price.setText("KSH "+currentModel.getPrice());
        Glide.with(context).load("http://thesmitherp.digitalsystemsafrica.com/public/img/"+currentModel.getImage()).into(holder.image);
    }

    @Override
    public int getItemCount() {
        return itemModels.size();
    }

    class Holder extends RecyclerView.ViewHolder {
        TextView name, quantity,comments,price;
        ImageView image;

        public Holder(View itemView) {
            super(itemView);
            image=itemView.findViewById(R.id.iv_paid_item);
            name = itemView.findViewById(R.id.item_name);
            quantity = itemView.findViewById(R.id.item_quantity);
            price = itemView.findViewById(R.id.paid_item_price);
            comments=itemView.findViewById(R.id.tv_comments);
        }
    }
}