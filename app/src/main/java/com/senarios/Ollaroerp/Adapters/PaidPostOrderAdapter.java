package com.senarios.Ollaroerp.Adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.senarios.Ollaroerp.Models.PostOrderItems;
import com.senarios.Ollaroerp.R;

import java.util.List;

public class PaidPostOrderAdapter extends RecyclerView.Adapter<PaidPostOrderAdapter.Holder> {
    List<PostOrderItems> itemModels;
    Context context;

    public PaidPostOrderAdapter(Context context, List<PostOrderItems> menuSubcatList){
        itemModels=menuSubcatList;
        this.context=context;
    }

    @NonNull
    @Override
    public Holder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View row = LayoutInflater.from(context).inflate(R.layout.paid_bills_layout, viewGroup, false);
        Holder holded = new Holder(row);
        return holded;
    }

    @Override
    public void onBindViewHolder(@NonNull Holder holder, int i) {
        PostOrderItems currentModel=itemModels.get(i);
        holder.orderId.setText(String.valueOf(currentModel.getId()));
        holder.orderTime.setText(currentModel.getdate_time());
        holder.total.setText("KSH "+currentModel.gettotal_amount());
        holder.comments.setText(currentModel.getitem_description());
        holder.itemsRecyclerView.setLayoutManager(new LinearLayoutManager(context));
        holder.itemsRecyclerView.setAdapter(new PaidOrderItemAdapter(context,  currentModel.getItems()));
    }

    @Override
    public int getItemCount() {
        return itemModels.size();
    }

    class Holder extends RecyclerView.ViewHolder {
        TextView total,orderId,orderTime,comments;
        RecyclerView itemsRecyclerView;

        public Holder(View itemView) {
            super(itemView);
            total=itemView.findViewById(R.id.tv_paid_order_total);
            orderId=itemView.findViewById(R.id.tv_order_id);
            orderTime=itemView.findViewById(R.id.tv_paid_order_time);
            comments=itemView.findViewById(R.id.tv_comments);
            itemsRecyclerView=itemView.findViewById(R.id.rv_orders_items);
        }
    }
}

