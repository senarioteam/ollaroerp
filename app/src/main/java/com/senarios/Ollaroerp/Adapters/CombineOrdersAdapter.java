package com.senarios.Ollaroerp.Adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.senarios.Ollaroerp.Models.CombineBillOrderModel;
import com.senarios.Ollaroerp.R;

import java.util.ArrayList;

public class CombineOrdersAdapter extends RecyclerView.Adapter<CombineOrdersAdapter.Holder> {

    ArrayList<CombineBillOrderModel> menu;
    Context context;

    public CombineOrdersAdapter(Context context, ArrayList<CombineBillOrderModel> menuList) {
        menu = menuList;
        this.context = context;
    }

    @NonNull
    @Override
    public Holder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View row = LayoutInflater.from(context).inflate(R.layout.combine_bill_orders_layout, viewGroup, false);

        Holder holded = new Holder(row);
        return holded;
    }

    @Override
    public void onBindViewHolder(@NonNull final Holder holder, int i) {
        CombineBillOrderModel currentModel = menu.get(i);
        holder.total.setText(currentModel.getTotal());
        holder.orderId.setText(currentModel.getOrderId());
        holder.recyclerView.setLayoutManager(new LinearLayoutManager(context));
        holder.recyclerView.setAdapter(new CombineItemsAdapter(context,currentModel.getCombineBillItemModels()));
    }

    @Override
    public int getItemCount() {
        return menu.size();
    }

    class Holder extends RecyclerView.ViewHolder {
        TextView total,orderId;
        RecyclerView recyclerView;

        public Holder(View itemView) {
            super(itemView);
            total = itemView.findViewById(R.id.tv_total_unpaid);
            orderId = itemView.findViewById(R.id.tv_order_id);
            recyclerView=itemView.findViewById(R.id.rv_orders);
        }
    }
}

