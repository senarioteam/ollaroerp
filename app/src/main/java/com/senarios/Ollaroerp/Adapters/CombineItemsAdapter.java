package com.senarios.Ollaroerp.Adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.senarios.Ollaroerp.Models.CombineBillItemModel;
import com.senarios.Ollaroerp.R;

import java.util.ArrayList;

public class CombineItemsAdapter extends  RecyclerView.Adapter<CombineItemsAdapter.Holder> {

    ArrayList<CombineBillItemModel> menu;
    Context context;

    public CombineItemsAdapter(Context context, ArrayList<CombineBillItemModel> menuList) {
        menu = menuList;
        this.context = context;
    }

    @NonNull
    @Override
    public Holder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View row = LayoutInflater.from(context).inflate(R.layout.combine_bills_items_layout, viewGroup, false);
        Holder holded = new Holder(row);
        return holded;
    }

    @Override
    public void onBindViewHolder(@NonNull final Holder holder, int i) {
        CombineBillItemModel currentModel = menu.get(i);
        holder.name.setText(currentModel.getName());
        holder.price.setText(currentModel.getPrice());
        holder.quantity.setText(currentModel.getQuantity());
    }

    @Override
    public int getItemCount() {
        return menu.size();
    }

    class Holder extends RecyclerView.ViewHolder {
        TextView name,quantity,price;

        public Holder(View itemView) {
            super(itemView);
            name = itemView.findViewById(R.id.item_name);
            quantity = itemView.findViewById(R.id.item_quantity);
            price=itemView.findViewById(R.id.item_price);
        }
    }
}

