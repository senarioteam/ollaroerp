package com.senarios.Ollaroerp.Adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.senarios.Ollaroerp.Models.OrderModel;
import com.senarios.Ollaroerp.R;

import java.util.ArrayList;

public class InprogressOrderAdapter extends RecyclerView.Adapter<InprogressOrderAdapter.Holder> {
    ArrayList<OrderModel> orderModels;
    Context context;

    public InprogressOrderAdapter(Context context,ArrayList<OrderModel> menuSubcatList){
        orderModels=menuSubcatList;
        this.context=context;
    }

    @NonNull
    @Override
    public Holder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View row = LayoutInflater.from(context).inflate(R.layout.full_order_item_layout, viewGroup, false);
        Holder holded = new Holder(row);
        return holded;
    }

    @Override
    public void onBindViewHolder(@NonNull Holder holder, int i) {
        OrderModel currentModel=orderModels.get(i);
        holder.tableid.setText(currentModel.getTableId());
        holder.orderId.setText(currentModel.getOrderId());
        holder.time.setText(currentModel.getTime());

    }

    @Override
    public int getItemCount() {
        return orderModels.size();
    }

    class Holder extends RecyclerView.ViewHolder {
        TextView orderId,tableid,time;
        RecyclerView recyclerView;

        public Holder(View itemView) {
            super(itemView);
            orderId = itemView.findViewById(R.id.tv_table_id);
            tableid=itemView.findViewById(R.id.tv_order_id);
            time=itemView.findViewById(R.id.tv_order_time);
            recyclerView=itemView.findViewById(R.id.rv_orders);
        }
    }
}
