package com.senarios.Ollaroerp.Adapters;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.support.annotation.NonNull;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;


import com.google.gson.JsonObject;
import com.kaopiz.kprogresshud.KProgressHUD;
import com.senarios.Ollaroerp.MainActivity;
import com.senarios.Ollaroerp.Models.InProgressOrders;
import com.senarios.Ollaroerp.Models.OfflineOrderItems;
import com.senarios.Ollaroerp.Models.OrderItem;
import com.senarios.Ollaroerp.R;
import com.senarios.Ollaroerp.Utitlities.ChangeOrderStatus;
import com.senarios.Ollaroerp.Utitlities.Retrofit.GetDataService;
import com.senarios.Ollaroerp.Utitlities.Retrofit.RetrofitClientInstance;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;

public class FullOrderAdapter extends RecyclerView.Adapter<FullOrderAdapter.Holder> {
    public static OrderItemAdapter orderItemAdapter;
   static ArrayList<InProgressOrders> orderModels;
    public static String ind;
    public static FullOrderAdapter fullOrderAdapter;
    Context context;
    Boolean checked = false;
    KProgressHUD pd;
    ChangeOrderStatus changeOrderStatus;
    public static ArrayList<InProgressOrders> orders;
   public static ArrayList<OrderItem> orderItems=new ArrayList<>();

    public FullOrderAdapter(Context context, ArrayList<InProgressOrders> menuSubcatList,ChangeOrderStatus changeOrderStatus) {
        orderModels = menuSubcatList;
        orders = menuSubcatList;
        this.changeOrderStatus=changeOrderStatus;
        this.context = context;
        try {
            pd = pd.create(context)
                    .setStyle(KProgressHUD.Style.SPIN_INDETERMINATE)
                    .setLabel("Just a moment")
                    .setCancellable(false)
                    .setAnimationSpeed(2)
                    .setDimAmount(0.5f);
        }catch (Exception e){
            //Toast.makeText(context, e.toString(), Toast.LENGTH_SHORT).show();
        }
    }

    @NonNull
    @Override
    public Holder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View row = LayoutInflater.from(context).inflate(R.layout.full_order_item_layout, viewGroup, false);
        Holder holded = new Holder(row);
        return holded;
    }

    @Override
    public void onBindViewHolder(@NonNull Holder holder, final int i) {

        final InProgressOrders currentModel = orderModels.get(i);
        List<OfflineOrderItems> offlineOrderItems = new ArrayList<>();
        offlineOrderItems = MainActivity.getOrderitemsbyID(String.valueOf(currentModel.getId()));

            orderItems.clear();
            for (int j = 0; j < offlineOrderItems.size(); j++) {

                OrderItem orderItem = new OrderItem();
                orderItem.setQuantity(offlineOrderItems.get(j).getQuantity());
                orderItem.setPrice(offlineOrderItems.get(j).getPrice());
                orderItem.setName(offlineOrderItems.get(j).getName());
                orderItem.setChecked(offlineOrderItems.get(j).getChecked());
                orderItem.setId(offlineOrderItems.get(j).getOrderid());
                orderItem.setImage(offlineOrderItems.get(j).getImage());
                orderItem.setpost_paid_orders_id(offlineOrderItems.get(j).getPost_paid_orders_id());
                if (orderItem.getQuantity() != 0) {
                    orderItems.add(orderItem);
                }

            }
            holder.tableid.setText(currentModel.getTableNo());
            holder.orderId.setText(Integer.toString(currentModel.getId()));
            holder.time.setText(currentModel.getDateTime());
            try {
                if (currentModel.getitem_description() != null) {
                    holder.comments.setText(currentModel.getitem_description());
                }
            } catch (Exception e) {

            }
            if (isNetworkAvailable()) {
                orderItemAdapter = new OrderItemAdapter(context, (ArrayList<OrderItem>) currentModel.getorder_items(), i, "NEW ORDERS");
                holder.recyclerView.setAdapter(orderItemAdapter);
            } else {
                orderItemAdapter = new OrderItemAdapter(context, orderItems, i, "NEW ORDERS");
                holder.recyclerView.setAdapter(orderItemAdapter);
            }
            holder.recyclerView.setLayoutManager(new LinearLayoutManager(context));

            holder.markCOmplete.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (orders.get(i).getorder_items() != null) {
                        for (int k = 0; k < orders.get(i).getorder_items().size(); k++) {
                            if (orders.get(i).getorder_items().get(k).getChecked()) {
                                checked = true;
                            }
                        }
                    }


                    if (checked) {
                        pd.show();
                        GetDataService service = RetrofitClientInstance.getRetrofitInstance().create(GetDataService.class);
                        Call<JsonObject> call = service.updateOrderStatus(Integer.toString(currentModel.getId()));
                        call.enqueue(new Callback<JsonObject>() {

                            @Override
                            public void onResponse(Call<JsonObject> call, retrofit2.Response<JsonObject> response) {
                                JsonObject jsonObject = response.body();
                                String code = jsonObject.get("code").getAsString();
                                if (code.equalsIgnoreCase("200")) {
                                    changeOrderStatus.changeStatus(currentModel.getId());
                                    Toast.makeText(context, "Order Completed", Toast.LENGTH_SHORT).show();
                                } else {
                                    Toast.makeText(context, "Please try again later", Toast.LENGTH_SHORT).show();
                                }
                                pd.dismiss();
                            }

                            @Override
                            public void onFailure(Call<JsonObject> call, Throwable t) {
                                pd.dismiss();
                                if (t.getMessage().contains("Unable to resolve host \"thesmitherp.digitalsystemsafrica.com\": No address associated with hostname")) {
                                    Toast.makeText(context, "Please Enable Wifi or DataConnection", Toast.LENGTH_SHORT).show();
                                } else {
                                    Toast.makeText(MainActivity.activity, "Something went wrong...Please try later!", Toast.LENGTH_SHORT).show();

                                }
                            }
                        });


                    } else {
                        checked = false;
                        if (!isNetworkAvailable()) {
                            Toast.makeText(context, "Please Enable Wifi or data to perform this action", Toast.LENGTH_SHORT).show();
                        } else {
                            Toast.makeText(context, "Please mark all orders complete", Toast.LENGTH_SHORT).show();
                        }

                    }
                }
            });


    }

    @Override
    public int getItemCount() {
        return orderModels.size();
    }


    class Holder extends RecyclerView.ViewHolder {
        TextView orderId, tableid, time,comments;
        RecyclerView recyclerView;
        Button markCOmplete;

        public Holder(View itemView) {
            super(itemView);
            tableid = itemView.findViewById(R.id.tv_table_id);
            orderId = itemView.findViewById(R.id.tv_order_id);
            time = itemView.findViewById(R.id.tv_order_time);
            recyclerView = itemView.findViewById(R.id.rv_orders);
            markCOmplete = itemView.findViewById(R.id.btn_mark_complete);
            comments=itemView.findViewById(R.id.tv_comments);
        }

    }

    private boolean isNetworkAvailable() {
        ConnectivityManager connectivityManager
                = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }
    public static List returndata(){

        return orderModels;
    }


}


