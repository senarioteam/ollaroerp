package com.senarios.Ollaroerp.Adapters;


import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;


import com.bumptech.glide.Glide;
import com.senarios.Ollaroerp.Fragments.AlcoholicSubGroupFragment;
import com.senarios.Ollaroerp.MainActivity;
import com.senarios.Ollaroerp.Models.AlchoholicGroup;
import com.senarios.Ollaroerp.R;
import com.senarios.Ollaroerp.Utitlities.FragmentTags;

import java.util.ArrayList;

public class AlcoholicGroupAdapter extends RecyclerView.Adapter<AlcoholicGroupAdapter.Holder> {

    ArrayList<AlchoholicGroup> menuSubCategories;
    Context context;

    public AlcoholicGroupAdapter(Context context, ArrayList<AlchoholicGroup> menuSubcatList){
        menuSubCategories=menuSubcatList;
        this.context=context;
    }

    @NonNull
    @Override
    public Holder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View row = LayoutInflater.from(context).inflate(R.layout.subcat_menu_itemview, viewGroup, false);
        Holder holded = new Holder(row);
        return holded;
    }

    @Override
    public void onBindViewHolder(@NonNull Holder holder, int i) {
        final AlchoholicGroup currentModel=menuSubCategories.get(i);
        Glide.with(context).load("http://thesmitherp.digitalsystemsafrica.com/public/img/"+currentModel.geta_image()).into(holder.itemImage);
        holder.itemName.setText(currentModel.getName());
        holder.subcatMenuLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlcoholicSubGroupFragment alcoholicSubGroupFragment=new AlcoholicSubGroupFragment();
                Bundle bundle=new Bundle();
                bundle.putString("sub_id",Integer.toString(currentModel.getId()));
                alcoholicSubGroupFragment.setArguments(bundle);
                MainActivity.activity.getSupportFragmentManager().beginTransaction().add(R.id.fragment_container,alcoholicSubGroupFragment,FragmentTags.SUBCATALCOSUBMENU).addToBackStack(FragmentTags.SUBCATALCOSUBMENU).commitAllowingStateLoss();
            }
        });
    }

    @Override
    public int getItemCount() {
        return menuSubCategories.size();
    }

    class Holder extends RecyclerView.ViewHolder {
        TextView itemName;
        ImageView itemImage;
        RelativeLayout subcatMenuLayout;

        public Holder(View itemView) {
            super(itemView);
            itemName = itemView.findViewById(R.id.tv_item_name);
            itemImage=itemView.findViewById(R.id.civ_subcat_menu);
            subcatMenuLayout = itemView.findViewById(R.id.subcat_menu_layout);
        }
    }
}
