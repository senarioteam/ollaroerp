package com.senarios.Ollaroerp.Adapters;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;


import com.bumptech.glide.Glide;
import com.senarios.Ollaroerp.Fragments.SubMajorGroupFragment;
import com.senarios.Ollaroerp.MainActivity;
import com.senarios.Ollaroerp.Models.MajorGroup;
import com.senarios.Ollaroerp.R;
import com.senarios.Ollaroerp.Utitlities.FragmentTags;

import java.util.ArrayList;

public class MenuAdapter extends RecyclerView.Adapter<MenuAdapter.Holder> {

    ArrayList<MajorGroup> menu;
    Context context;

    public MenuAdapter(Context context,ArrayList<MajorGroup> menuList){
        menu=menuList;
        this.context=context;
    }

    @NonNull
    @Override
    public Holder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View row = LayoutInflater.from(context).inflate(R.layout.menu_item_layout, viewGroup, false);
        Holder holded = new Holder(row);
        return holded;
    }

    @Override
    public void onBindViewHolder(@NonNull Holder holder, int i) {
        final MajorGroup currentModel=menu.get(i);
        holder.itemName.setText(currentModel.getName());
        Glide.with(context).load("http://thesmitherp.digitalsystemsafrica.com/public/img/"+currentModel.getAImage()).into(holder.itemImage);
        holder.menuLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SubMajorGroupFragment subMajorGroupFragment =new SubMajorGroupFragment();
                Bundle args=new Bundle();
                args.putString("major_id",Integer.toString(currentModel.getId()));
                subMajorGroupFragment.setArguments(args);
                MainActivity.activity.getSupportFragmentManager().beginTransaction().add(R.id.fragment_container, subMajorGroupFragment,FragmentTags.MENUCAT).addToBackStack(FragmentTags.MENUCAT).commitAllowingStateLoss();
                //MainActivity.activity.changeFragment(R.id.fragment_container,subMajorGroupFragment,(FragmentTags.MENUCAT));
            }
        });
    }

    @Override
    public int getItemCount() {
        return menu.size();
    }

    class Holder extends RecyclerView.ViewHolder {
        TextView itemName;
        ImageView itemImage;
        LinearLayout menuLayout;

        public Holder(View itemView) {
            super(itemView);
            itemImage=itemView.findViewById(R.id.iv_menu_item);
            itemName = itemView.findViewById(R.id.tv_menu_item);
            menuLayout=itemView.findViewById(R.id.menu_layout);
        }
    }
}

