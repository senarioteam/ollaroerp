package com.senarios.Ollaroerp.Adapters;


import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;


import com.bumptech.glide.Glide;
import com.google.gson.Gson;
import com.senarios.Ollaroerp.Fragments.SelectedSubcatItemFragment;
import com.senarios.Ollaroerp.MainActivity;
import com.senarios.Ollaroerp.Models.ItemModel;
import com.senarios.Ollaroerp.R;
import com.senarios.Ollaroerp.Utitlities.FragmentTags;

import java.util.ArrayList;

public class SubcatMenuItemAdapter extends RecyclerView.Adapter<SubcatMenuItemAdapter.Holder> {

    ArrayList<ItemModel> menuSubCategories;
    Context context;

    public SubcatMenuItemAdapter(Context context,ArrayList<ItemModel> menuSubcatList){
        menuSubCategories=menuSubcatList;
         this.context=context;
    }

    @NonNull
    @Override
    public Holder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View row = LayoutInflater.from(context).inflate(R.layout.subcat_menu_subcat_itemview, viewGroup, false);
        Holder holded = new Holder(row);
        return holded;
    }

    @Override
    public void onBindViewHolder(@NonNull Holder holder, int i) {
        final ItemModel currentModel=menuSubCategories.get(i);
        holder.itemName.setText(currentModel.getName());
        holder.itemPrice.setText("KSH " +currentModel.getPrice());
        holder.itemFranchise.setText(currentModel.getDescription());
        Glide.with(context).load("http://thesmitherp.digitalsystemsafrica.com/public/img/"+currentModel.getAImage()).into(holder.itemImage);
        holder.subcatMenuLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Bundle args=new Bundle();
                args.putString("myObject", new Gson().toJson(currentModel));
                SelectedSubcatItemFragment selectedSubcatItemFragment=new SelectedSubcatItemFragment();
                selectedSubcatItemFragment.setArguments(args);
                MainActivity.activity.getSupportFragmentManager().beginTransaction().add(R.id.fragment_container,selectedSubcatItemFragment,FragmentTags.SELECTEDSUBCAT).addToBackStack(FragmentTags.SELECTEDSUBCAT).commitAllowingStateLoss();
            }
        });
    }

    @Override
    public int getItemCount() {
        return menuSubCategories.size();
    }

    class Holder extends RecyclerView.ViewHolder {
        TextView itemName,itemPrice,itemFranchise;
        ImageView itemImage;
        RelativeLayout subcatMenuLayout;

        public Holder(View itemView) {
            super(itemView);
            itemName = itemView.findViewById(R.id.tv_product_title);
            itemFranchise=itemView.findViewById(R.id.tv_product_franchise);
            itemPrice=itemView.findViewById(R.id.tv_product_price);
            itemImage=itemView.findViewById(R.id.iv_product_image);
            subcatMenuLayout = itemView.findViewById(R.id.sub_menu_item_layout);
        }
    }
}

