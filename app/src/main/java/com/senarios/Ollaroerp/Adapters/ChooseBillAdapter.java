package com.senarios.Ollaroerp.Adapters;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RadioButton;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.activeandroid.query.Delete;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonObject;
import com.kaopiz.kprogresshud.KProgressHUD;
import com.senarios.Ollaroerp.Fragments.HomeFragment;
import com.senarios.Ollaroerp.MainActivity;
import com.senarios.Ollaroerp.Models.CartItemModel;
import com.senarios.Ollaroerp.Models.OfflineAddToBills;
import com.senarios.Ollaroerp.Models.Order;
import com.senarios.Ollaroerp.Models.UnpaidBillModel;
import com.senarios.Ollaroerp.R;
import com.senarios.Ollaroerp.Utitlities.FragmentTags;
import com.senarios.Ollaroerp.Utitlities.PrefUtils;
import com.senarios.Ollaroerp.Utitlities.Retrofit.GetDataService;
import com.senarios.Ollaroerp.Utitlities.Retrofit.RetrofitClientInstance;
import com.senarios.Ollaroerp.Utitlities.TinyDB;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;

public class ChooseBillAdapter extends RecyclerView.Adapter<ChooseBillAdapter.Holder> {
    ArrayList<UnpaidBillModel> menu;
    Context context;
    Order order;
    String items;
    KProgressHUD pd;

    public ChooseBillAdapter(Context context, ArrayList<UnpaidBillModel> menuList, Order order, String items) {
        menu = menuList;
        this.order = order;
        this.items = items;
        this.context = context;
        pd = pd.create(context)
                .setStyle(KProgressHUD.Style.SPIN_INDETERMINATE)
                .setLabel("Please Wait")
                .setCancellable(false)
                .setAnimationSpeed(2)
                .setDimAmount(0.5f);
    }

    @NonNull
    @Override
    public Holder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View row = LayoutInflater.from(context).inflate(R.layout.bill_item_layout, viewGroup, false);
        Holder holded = new Holder(row);
        return holded;
    }

    @Override
    public void onBindViewHolder(@NonNull final Holder holder, final int pos) {
        final UnpaidBillModel currentModel = menu.get(pos);
        holder.id.setText(Integer.toString(currentModel.getId()));
        holder.price.setText("KSH " + currentModel.gettotal_amount());
        holder.comment.setText(currentModel.getComments());
        holder.check.setEnabled(false);
        holder.linearLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                holder.check.setChecked(true);
                AlertDialog.Builder builder;
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                    builder = new AlertDialog.Builder(context, android.R.style.Theme_Material_Dialog_Alert);
                } else {
                    builder = new AlertDialog.Builder(context);
                }
                builder.setTitle("Confirm Add")
                        .setMessage("Are you sure you want to add to this bill?")
                        .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                String amount = order.getTotal_amount();
                                amount = amount.replace("KSH ", "");
                                order.setTotal_amount(amount);
                                if (isNetworkAvailable()) {
                                    pd.show();
                                    GetDataService service = RetrofitClientInstance.getRetrofitInstance().create(GetDataService.class);
                                    Call<JsonObject> call = service.addToBill(String.valueOf(PrefUtils.getUserModel(context).getId()), String.valueOf(currentModel.getId()), order.getDate_time(), order.getTable_no(), order.getNo_of_guests(), order.getItem_description(), order.getCondiments(), order.getWaiter(), order.getRestaurant(), order.getTotal_amount(), items, order.getSection());
                                    call.enqueue(new Callback<JsonObject>() {

                                        @Override
                                        public void onResponse(Call<JsonObject> call, retrofit2.Response<JsonObject> response) {
                                            try {
                                                JsonObject jsonObject = response.body();
                                                String code = jsonObject.get("code").getAsString();
                                                if (code.equalsIgnoreCase("200")) {
                                                    Toast.makeText(context, "Order added to bill successfully", Toast.LENGTH_SHORT).show();
                                                    new Delete().from(CartItemModel.class).execute();
                                                    MainActivity.activity.updateItemCount();
                                                    MainActivity.activity.changeFragment(R.id.fragment_container, new HomeFragment(), FragmentTags.HOME);
                                                } else {
                                                    Toast.makeText(context, "Please try again later", Toast.LENGTH_SHORT).show();
                                                }
                                            } catch (Exception e) {
                                                Toast.makeText(context, "Please try again later", Toast.LENGTH_SHORT).show();
                                            }

                                            pd.dismiss();
                                        }

                                        @Override
                                        public void onFailure(Call<JsonObject> call, Throwable t) {
                                            pd.dismiss();
                                            Toast.makeText(MainActivity.activity, "Something went wrong...Please try later!", Toast.LENGTH_SHORT).show();
                                        }
                                    });

                                }else {
                                    GsonBuilder builder = new GsonBuilder();
                                    builder.excludeFieldsWithModifiers();
                                    Gson gson = builder.create();

                                    OfflineAddToBills offlineAddToBills = new OfflineAddToBills();
                                    offlineAddToBills.setItems(items);
                                    offlineAddToBills.setOrder(gson.toJson(order));
                                    offlineAddToBills.setBilld(String.valueOf(currentModel.getId()));

                                    TinyDB tinyDB = new TinyDB(context);
                                    try {
                                        MainActivity.offlineAddToBills = tinyDB.getListString("OfflineAddToBills");
                                    }catch (Exception e){
                                        e.printStackTrace();
                                    }
                                    MainActivity.offlineAddToBills.add(gson.toJson(offlineAddToBills));

                                    PrefUtils.putString(context,"hasBills","true");
                                    tinyDB.putListString("OfflineAddToBills", MainActivity.offlineAddToBills);
                                    Toast.makeText(context, "Request Added To Queue", Toast.LENGTH_SHORT).show();
                                    new Delete().from(CartItemModel.class).execute();
                                    MainActivity.activity.updateItemCount();
                                    MainActivity.activity.changeFragment(R.id.fragment_container, new HomeFragment(), FragmentTags.HOME);
                                }
                            }
                        })
                        .setNegativeButton(android.R.string.no, new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                holder.check.setChecked(false);
                                dialog.dismiss();
                                pd.dismiss();
                            }
                        })
                        .setIcon(android.R.drawable.ic_dialog_alert)
                        .show();


            }
        });

    }

    @Override
    public int getItemCount() {
        return menu.size();
    }

    class Holder extends RecyclerView.ViewHolder {
        TextView id, price, comment;
        RadioButton check;
        RelativeLayout linearLayout;

        public Holder(View itemView) {
            super(itemView);
            id = itemView.findViewById(R.id.tv_bill_id);
            comment = itemView.findViewById(R.id.tv_bill_comment);
            price = itemView.findViewById(R.id.tv_bill_price);
            check = itemView.findViewById(R.id.check_bill);
            linearLayout = itemView.findViewById(R.id.linear_layout);
        }
    }

    private boolean isNetworkAvailable() {
        ConnectivityManager connectivityManager
                = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }
}
