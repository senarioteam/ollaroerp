package com.senarios.Ollaroerp.Adapters;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.support.annotation.NonNull;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.JsonObject;
import com.kaopiz.kprogresshud.KProgressHUD;
import com.senarios.Ollaroerp.Fragments.GenerateBillFragment;
import com.senarios.Ollaroerp.MainActivity;
import com.senarios.Ollaroerp.Models.OfflineOrderItems;
import com.senarios.Ollaroerp.Models.OrderItem;
import com.senarios.Ollaroerp.Models.PostOrderItems;
import com.senarios.Ollaroerp.R;
import com.senarios.Ollaroerp.Utitlities.FragmentTags;
import com.senarios.Ollaroerp.Utitlities.Retrofit.GetDataService;
import com.senarios.Ollaroerp.Utitlities.Retrofit.RetrofitClientInstance;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;

public class UnpaidOrdersAdapter extends RecyclerView.Adapter<UnpaidOrdersAdapter.Holder> {
    List<PostOrderItems> itemModels;
    Context context;
    private KProgressHUD pd;

    public UnpaidOrdersAdapter(Context context, List<PostOrderItems> menuSubcatList) {
        itemModels = menuSubcatList;
        this.context = context;
    }

    @NonNull
    @Override
    public Holder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View row = LayoutInflater.from(context).inflate(R.layout.updaid_orders_layout, viewGroup, false);
        Holder holded = new Holder(row);
        return holded;
    }

    @Override
    public void onBindViewHolder(@NonNull Holder holder, int i) {
        final PostOrderItems currentModel = itemModels.get(i);
        holder.totalUnpaid.setText(currentModel.gettotal_amount());
        holder.orderId.setText(Integer.toString(currentModel.getId()));
        holder.orderTime.setText(currentModel.getdate_time());
        holder.tableId.setText(currentModel.gettable_no());
        if (currentModel.getBillId().equalsIgnoreCase("0")) {
            holder.payOrder.setVisibility(View.VISIBLE);
        }  if (isNetworkAvailable()){

            holder.unpaidItemRecyclerView.setLayoutManager(new LinearLayoutManager(context));
            holder.unpaidItemRecyclerView.setAdapter(new UnpaidItemsAdapter(context, currentModel.getItems()));
        }
        else{
            List<OfflineOrderItems> offlineOrderItems=MainActivity.getOrderitemsbyID(String.valueOf(currentModel.getId()));
            ArrayList<OrderItem> orderItems=new ArrayList<>();
            for (int k=0;k<offlineOrderItems.size();k++){
                OrderItem orderItem=new OrderItem();
                orderItem.setName(offlineOrderItems.get(k).getName());
                orderItem.setPrice(offlineOrderItems.get(k).getPrice());
                orderItem.setQuantity(offlineOrderItems.get(k).getQuantity());
                orderItems.add(orderItem);
            }
            holder.unpaidItemRecyclerView.setLayoutManager(new LinearLayoutManager(context));
            holder.unpaidItemRecyclerView.setAdapter(new UnpaidItemsAdapter(context, orderItems));
        }




        holder.payOrder.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               /* FragmentTransaction ft = MainActivity.activity.getSupportFragmentManager().beginTransaction();
                Fragment prev = MainActivity.activity.getSupportFragmentManager().findFragmentByTag(FragmentTags.COMBINEDIALOGUE);
                if (prev != null) {
                    ft.remove(prev);
                }
                CombinePyamentDialogueFragment combinePyamentDialogueFragment = new CombinePyamentDialogueFragment();
                Bundle args = new Bundle();
                GsonBuilder builder = new GsonBuilder();
                builder.excludeFieldsWithModifiers();
                Gson gson = builder.create();
                args.putString("myOrder", gson.toJson(currentModel));
                combinePyamentDialogueFragment.setArguments(args);
                ft.addToBackStack(null);
                DialogFragment dialogFragment = combinePyamentDialogueFragment;
                dialogFragment.show(ft, FragmentTags.COMBINEDIALOGUE);
                dialogFragment.setCancelable(true);*/
                pd = pd.create(context)
                        .setStyle(KProgressHUD.Style.SPIN_INDETERMINATE)
                        .setLabel("Please Wait")
                        .setCancellable(false)
                        .setAnimationSpeed(2)
                        .setDimAmount(0.5f);

                pd.show();

                GetDataService service = RetrofitClientInstance.getRetrofitInstance().create(GetDataService.class);
                Call<JsonObject> call = service.genBill(String.valueOf(currentModel.getId()));
                call.enqueue(new Callback<JsonObject>() {

                    @Override
                    public void onResponse(Call<JsonObject> call, retrofit2.Response<JsonObject> response) {
                        if (response.body() != null) {
                            JsonObject jsonObject=response.body();
                            if(jsonObject.get("code").getAsString().equalsIgnoreCase("200")){
                                Toast.makeText(context, "Order generated successfully", Toast.LENGTH_SHORT).show();
                                MainActivity.activity.changeFragment(R.id.fragment_container,new GenerateBillFragment(),FragmentTags.GENERATEBILL);
                            }else {
                                Toast.makeText(context, "Some Error Occured! Please Try Again", Toast.LENGTH_SHORT).show();
                            }


                        } else {
                            Toast.makeText(context, "Some Error Occured! Please Try Again", Toast.LENGTH_SHORT).show();
                        }
                        pd.dismiss();
                    }

                    @Override
                    public void onFailure(Call<JsonObject> call, Throwable t) {
                        pd.dismiss();
                        Toast.makeText(MainActivity.activity, "Something went wrong...Please try later!", Toast.LENGTH_SHORT).show();
                    }
                });
            }
        });
    }

    @Override
    public int getItemCount() {
        return itemModels.size();
    }

    class Holder extends RecyclerView.ViewHolder {
        private TextView totalUnpaid, orderId, tableId, orderTime;
        private RecyclerView unpaidItemRecyclerView;
        private Button payOrder;

        public Holder(View itemView) {
            super(itemView);
            orderTime = itemView.findViewById(R.id.tv_order_time);
            totalUnpaid = itemView.findViewById(R.id.tv_total_unpaid);
            orderId = itemView.findViewById(R.id.tv_order_id);
            tableId = itemView.findViewById(R.id.tv_table_id);
            unpaidItemRecyclerView = itemView.findViewById(R.id.rv_orders);
            payOrder = itemView.findViewById(R.id.btn_generate_bill);
        }
    }

    private boolean isNetworkAvailable() {
        ConnectivityManager connectivityManager
                = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }
}