package com.senarios.Ollaroerp.Adapters;


import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;


import com.bumptech.glide.Glide;
import com.senarios.Ollaroerp.Fragments.AlcoholicGroupFragment;
import com.senarios.Ollaroerp.Fragments.FamilyGroupFragment;
import com.senarios.Ollaroerp.MainActivity;
import com.senarios.Ollaroerp.Models.MenuItem;
import com.senarios.Ollaroerp.R;
import com.senarios.Ollaroerp.Utitlities.FragmentTags;

import java.util.ArrayList;

public class MenuSubCategoryAdapter extends RecyclerView.Adapter<MenuSubCategoryAdapter.Holder> {

    ArrayList<MenuItem> menuSubCategories;
    Context context;

    public MenuSubCategoryAdapter(Context context,ArrayList<MenuItem> menuSubcatList){
        menuSubCategories=menuSubcatList;
        this.context=context;
    }

    @NonNull
    @Override
    public Holder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View row = LayoutInflater.from(context).inflate(R.layout.menu_subcat_itemview, viewGroup, false);
        Holder holded = new Holder(row);
        return holded;
    }

    @Override
    public void onBindViewHolder(@NonNull Holder holder, int i) {
        final MenuItem currentModel=menuSubCategories.get(i);
        holder.itemName.setText(currentModel.getName());
        Glide.with(context).load("http://thesmitherp.digitalsystemsafrica.com/public/img/"+currentModel.geta_image()).into(holder.itemImage);
        holder.time.setText(currentModel.getTimefrom()+" - "+currentModel.getTimeto());
        holder.subcatLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(currentModel.getIs_checked().equalsIgnoreCase("on")) {
                    AlcoholicGroupFragment alcoholicGroupFragment = new AlcoholicGroupFragment();
                    Bundle args = new Bundle();
                    args.putString("group_id", Integer.toString(currentModel.getId()));
                    alcoholicGroupFragment.setArguments(args);
                    MainActivity.activity.getSupportFragmentManager().beginTransaction().add(R.id.fragment_container, alcoholicGroupFragment, FragmentTags.SUBCATALCOSUBMENU).addToBackStack(FragmentTags.SUBCATMENU).commit();
                }
                else if(currentModel.getIs_checked().equalsIgnoreCase("off")){
                    FamilyGroupFragment familyGroupFragment = new FamilyGroupFragment();
                    Bundle args = new Bundle();
                    args.putString("group_id", Integer.toString(currentModel.getId()));
                    familyGroupFragment.setArguments(args);
                    MainActivity.activity.getSupportFragmentManager().beginTransaction().add(R.id.fragment_container, familyGroupFragment, FragmentTags.FAMILYGROUPFRAGMENT).addToBackStack(FragmentTags.FAMILYGROUPFRAGMENT).commit();
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        if(menuSubCategories!=null) {
            return menuSubCategories.size();
        }
        else{
            return 0;
        }
    }

    class Holder extends RecyclerView.ViewHolder {
        TextView itemName;
        TextView time;
        LinearLayout subcatLayout;
        ImageView itemImage;

        public Holder(View itemView) {
            super(itemView);
            time=itemView.findViewById(R.id.tv_time);
            itemName = itemView.findViewById(R.id.tv_subcat_name);
            subcatLayout=itemView.findViewById(R.id.subcat_layout);
            itemImage=itemView.findViewById(R.id.iv_item);
        }
    }
}
