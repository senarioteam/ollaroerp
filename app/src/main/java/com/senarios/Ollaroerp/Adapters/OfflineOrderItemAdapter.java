package com.senarios.Ollaroerp.Adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.TextView;

import com.senarios.Ollaroerp.CheckingOrdersInterface;
import com.senarios.Ollaroerp.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class OfflineOrderItemAdapter extends RecyclerView.Adapter<OfflineOrderItemAdapter.Holder> {
    JSONArray itemModels;
    CheckingOrdersInterface checkingOrdersInterface;
    Context context;

    public OfflineOrderItemAdapter(Context context, JSONArray menuSubcatList) {
        itemModels = menuSubcatList;
        this.context = context;
    }

    @NonNull
    @Override
    public Holder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View row = LayoutInflater.from(context).inflate(R.layout.inprogress_orders_item_layout, viewGroup, false);
        Holder holded = new Holder(row);
        return holded;
    }

    @Override
    public void onBindViewHolder(@NonNull final Holder holder, final int i) {
        final JSONObject currentModel;
        try {
            currentModel = itemModels.getJSONObject(i);
            holder.name.setText(currentModel.getString("item_name"));
            holder.quantity.setText(currentModel.getString("item_quantity") + "" +
                    " QNT");

        } catch (JSONException e) {
            e.printStackTrace();
        }
        holder.check.setVisibility(View.GONE);
    }

    @Override
    public int getItemCount() {
        return itemModels.length();
    }

    class Holder extends RecyclerView.ViewHolder {
        TextView name, quantity, comments;
        CheckBox check;

        public Holder(View itemView) {
            super(itemView);
            name = itemView.findViewById(R.id.item_name);
            quantity = itemView.findViewById(R.id.item_quantity);
            check = itemView.findViewById(R.id.check_completed);
        }
    }
}

