package com.senarios.Ollaroerp.Adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.senarios.Ollaroerp.Models.OrderItem;
import com.senarios.Ollaroerp.R;

import java.util.List;

public class UnpaidItemsAdapter extends RecyclerView.Adapter<UnpaidItemsAdapter.Holder>{
   List<OrderItem> itemModels;
    Context context;

    public UnpaidItemsAdapter(Context context,List<OrderItem> menuSubcatList){
        itemModels=menuSubcatList;
        this.context=context;
    }

    @NonNull
    @Override
    public Holder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View row = LayoutInflater.from(context).inflate(R.layout.unpaid_orders_item_layout, viewGroup, false);
        Holder holded = new Holder(row);
        return holded;
    }

    @Override
    public void onBindViewHolder(@NonNull Holder holder, int i) {
        OrderItem currentModel=itemModels.get(i);
        holder.name.setText(currentModel.getName());
        holder.quantity.setText(currentModel.getQuantity()+" QNT");
        holder.price.setText("KSH "+currentModel.getPrice());
    }

    @Override
    public int getItemCount() {
        return itemModels.size();
    }

    class Holder extends RecyclerView.ViewHolder {
        TextView name,quantity,price;

        public Holder(View itemView) {
            super(itemView);
            name = itemView.findViewById(R.id.unpaid_item_name);
            quantity=itemView.findViewById(R.id.unpaid_item_quantity);
            price=itemView.findViewById(R.id.tv_unpaid_item_price);
        }
    }
}
