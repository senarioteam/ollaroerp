package com.senarios.Ollaroerp.Adapters;


import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;


import com.bumptech.glide.Glide;
import com.senarios.Ollaroerp.Fragments.CartBalanceCalculator;
import com.senarios.Ollaroerp.Models.CartItemModel;
import com.senarios.Ollaroerp.R;

import java.util.ArrayList;

public class CartItemAdapter extends RecyclerView.Adapter<CartItemAdapter.Holder> {

    ArrayList<CartItemModel> menuSubCategories;
    Context context;
    CartBalanceCalculator cartBalanceCalculator;

    public CartItemAdapter(Context context, ArrayList<CartItemModel> menuSubcatList,CartBalanceCalculator cartBalanceCalculator){
        menuSubCategories=menuSubcatList;
        this.cartBalanceCalculator=cartBalanceCalculator;
        this.context=context;
    }

    @NonNull
    @Override
    public Holder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View row = LayoutInflater.from(context).inflate(R.layout.cart_item_layout, viewGroup, false);
        Holder holded = new Holder(row);
        return holded;
    }

    @Override
    public void onBindViewHolder(@NonNull final Holder holder,final int i) {
        final CartItemModel currentModel=menuSubCategories.get(i);
        Glide.with(context).load("http://thesmitherp.digitalsystemsafrica.com/public/img/"+currentModel.getImage()).into(holder.itemImage);
        holder.itemName.setText(currentModel.getItemName());
        holder.itemPrice.setText("KSH "+currentModel.getPrice());
        holder.itemQuantity.setText(currentModel.getQuantity());
        holder.inc.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int quantity=Integer.parseInt(holder.itemQuantity.getText().toString());
                quantity++;
                currentModel.setQuantity(Integer.toString(quantity));
                currentModel.save();
                holder.itemQuantity.setText(Integer.toString(quantity));
                cartBalanceCalculator.inductPrice(currentModel.getPrice());
            }
        });
        holder.dec.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!holder.itemQuantity.getText().toString().equalsIgnoreCase("1")) {
                    try {
                        int quantity = Integer.parseInt(holder.itemQuantity.getText().toString());
                        quantity--;
                        currentModel.setQuantity(Integer.toString(quantity));
                        currentModel.save();
                        holder.itemQuantity.setText(Integer.toString(quantity));
                        cartBalanceCalculator.deductPrice(currentModel.getPrice());
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }
        });
        holder.cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                float price=Float.parseFloat(holder.itemQuantity.getText().toString())*Float.parseFloat(currentModel.getPrice());
                cartBalanceCalculator.del(i,Float.toString(price));
            }
        });
    }

    @Override
    public int getItemCount() {
        return menuSubCategories.size();
    }

    class Holder extends RecyclerView.ViewHolder {
        TextView itemName,itemPrice,itemQuantity,inc,dec;
        ImageView itemImage,cancel;

        public Holder(View itemView) {
            super(itemView);
            itemName = itemView.findViewById(R.id.tv_cart_item_name);
            itemImage=itemView.findViewById(R.id.iv_cart_item);
            itemPrice = itemView.findViewById(R.id.tv_cart_item_price);
            itemQuantity=itemView.findViewById(R.id.tv_cart_quantity);
            inc=itemView.findViewById(R.id.inc);
            dec=itemView.findViewById(R.id.dec);
            cancel=itemView.findViewById(R.id.iv_cancel);
        }
    }
}
