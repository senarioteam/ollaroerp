package com.senarios.Ollaroerp.Adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.google.gson.Gson;
import com.kaopiz.kprogresshud.KProgressHUD;
import com.senarios.Ollaroerp.Models.OfflineOrders;
import com.senarios.Ollaroerp.Models.Order;
import com.senarios.Ollaroerp.R;
import com.senarios.Ollaroerp.Utitlities.ChangeOrderStatus;

import org.json.JSONArray;
import org.json.JSONException;

import java.util.ArrayList;

public class PendingOrdersAdapter extends RecyclerView.Adapter<PendingOrdersAdapter.Holder> {

    ArrayList<OfflineOrders> orderModels;
    Context context;
    KProgressHUD pd;
    ChangeOrderStatus changeOrderStatus;

    public PendingOrdersAdapter(Context context, ArrayList<OfflineOrders> menuSubcatList) {
        orderModels = menuSubcatList;
        this.changeOrderStatus = changeOrderStatus;
        this.context = context;
        try {
            pd = pd.create(context)
                    .setStyle(KProgressHUD.Style.SPIN_INDETERMINATE)
                    .setLabel("Just a moment")
                    .setCancellable(false)
                    .setAnimationSpeed(2)
                    .setDimAmount(0.5f);
        } catch (Exception e) {
            //Toast.makeText(context, e.toString(), Toast.LENGTH_SHORT).show();
        }
    }

    @NonNull
    @Override
    public Holder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View row = LayoutInflater.from(context).inflate(R.layout.full_order_item_layout, viewGroup, false);
        Holder holded = new Holder(row);
        return holded;
    }

    @Override
    public void onBindViewHolder(@NonNull Holder holder, final int i) {
        final OfflineOrders currentModel = orderModels.get(i);
        Order order = new Gson().fromJson(currentModel.getOrder(), Order.class);
        String items = currentModel.getItems();
        try {
            JSONArray jsonArray = new JSONArray(items);
            holder.tableid.setText(order.getTable_no());
            holder.time.setText(order.getDate_time());
            if (!order.getItem_description().equalsIgnoreCase("")) {
                holder.comments.setText(order.getItem_description());
            }
            holder.recyclerView.setLayoutManager(new LinearLayoutManager(context));
            holder.recyclerView.setAdapter(new OfflineOrderItemAdapter(context, jsonArray));

        } catch (JSONException e) {
            e.printStackTrace();
        }


    }

    @Override
    public int getItemCount() {
        return orderModels.size();
    }


    class Holder extends RecyclerView.ViewHolder {
        TextView orderId, tableid, time, comments;
        RecyclerView recyclerView;
        Button markCOmplete;

        public Holder(View itemView) {
            super(itemView);
            tableid = itemView.findViewById(R.id.tv_table_id);
            orderId = itemView.findViewById(R.id.tv_order_id);
            time = itemView.findViewById(R.id.tv_order_time);
            recyclerView = itemView.findViewById(R.id.rv_orders);
            markCOmplete = itemView.findViewById(R.id.btn_mark_complete);
            comments = itemView.findViewById(R.id.tv_comments);
        }
    }
}



