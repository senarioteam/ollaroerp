package com.senarios.Ollaroerp.Adapters;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.senarios.Ollaroerp.Fragments.BillTransferFragment;
import com.senarios.Ollaroerp.Fragments.PaymentMethodFragment;
import com.senarios.Ollaroerp.MainActivity;
import com.senarios.Ollaroerp.Models.UnpaidBillModel;
import com.senarios.Ollaroerp.R;
import com.senarios.Ollaroerp.Utitlities.FragmentTags;

import java.util.List;

public class UnpaidBillsAdapter extends RecyclerView.Adapter<UnpaidBillsAdapter.Holder> {
    List<UnpaidBillModel> itemModels;
    Context context;

    public UnpaidBillsAdapter(Context context, List<UnpaidBillModel> menuSubcatList) {
        itemModels = menuSubcatList;
        this.context = context;
    }

    @NonNull
    @Override
    public Holder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View row = LayoutInflater.from(context).inflate(R.layout.unpaid_bills_item_layout, viewGroup, false);
        Holder holded = new Holder(row);
        return holded;
    }

    @Override
    public void onBindViewHolder(@NonNull Holder holder, int i) {
        final UnpaidBillModel currentModel = itemModels.get(i);
        holder.totalBill.setText("KSH "+currentModel.gettotal_amount());
        holder.billId.setText(Integer.toString(currentModel.getId()));
        holder.totalOrders.setText(currentModel.getno_orders());
        holder.makePayment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                PaymentMethodFragment paymentMethodFragment = new PaymentMethodFragment();
                Bundle args = new Bundle();
                GsonBuilder builder = new GsonBuilder();
                builder.excludeFieldsWithModifiers();
                Gson gson = builder.create();
                args.putString("myBill", gson.toJson(currentModel));
                paymentMethodFragment.setArguments(args);
                MainActivity.activity.getSupportFragmentManager().beginTransaction().add(R.id.fragment_container,paymentMethodFragment,FragmentTags.PAYMENTMETHOD).addToBackStack(FragmentTags.PAYMENTMETHOD).commit();
            }
        });
        holder.transferBill.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                BillTransferFragment billTransferFragment=new BillTransferFragment();
                Bundle args=new Bundle();
                args.putString("bill_id",String.valueOf(currentModel.getId()));
                billTransferFragment.setArguments(args);
                MainActivity.activity.getSupportFragmentManager().beginTransaction().add(R.id.fragment_container,billTransferFragment,FragmentTags.BILLTRANSFER).addToBackStack(FragmentTags.BILLTRANSFER).commit();
            }
        });
        holder.comment.setText(currentModel.getComments());
        holder.tableId.setText(currentModel.getTable_no());
    }

    @Override
    public int getItemCount() {
        return itemModels.size();
    }

    class Holder extends RecyclerView.ViewHolder {
        private TextView billId, totalBill, totalOrders, comment,tableId;
        private Button makePayment,transferBill;

        public Holder(View itemView) {
            super(itemView);
            totalBill = itemView.findViewById(R.id.tv_total);
            comment=itemView.findViewById(R.id.tv_comments);
            billId = itemView.findViewById(R.id.tv_bill_id);
            totalOrders = itemView.findViewById(R.id.tv_order_total);
            makePayment=itemView.findViewById(R.id.btn_make_payment);
            transferBill=itemView.findViewById(R.id.btn_transfer_bill);//tv_table_id
            tableId=itemView.findViewById(R.id.tv_table_id);
        }
    }

}
