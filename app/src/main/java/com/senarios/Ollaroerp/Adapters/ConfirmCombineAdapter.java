package com.senarios.Ollaroerp.Adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.senarios.Ollaroerp.Models.BillModel;
import com.senarios.Ollaroerp.R;

import java.util.ArrayList;

public class ConfirmCombineAdapter extends RecyclerView.Adapter<ConfirmCombineAdapter.Holder> {
    ArrayList<BillModel> menu;
    Context context;

    public ConfirmCombineAdapter(Context context) {

        this.context = context;
    }

    @NonNull
    @Override
    public Holder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View row = LayoutInflater.from(context).inflate(R.layout.confirm_combile_orders_itemview, viewGroup, false);
        Holder holded = new Holder(row);
        return holded;
    }

    @Override
    public void onBindViewHolder(@NonNull final Holder holder, int i) {

    }

    @Override
    public int getItemCount() {
        return 3;
    }

    class Holder extends RecyclerView.ViewHolder {
        TextView id, price, comment;

        public Holder(View itemView) {
            super(itemView);

        }
    }
}
