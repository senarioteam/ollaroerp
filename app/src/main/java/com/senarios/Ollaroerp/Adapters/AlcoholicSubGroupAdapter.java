package com.senarios.Ollaroerp.Adapters;


import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;


import com.bumptech.glide.Glide;
import com.senarios.Ollaroerp.Fragments.SubcatMenuItemFragment;
import com.senarios.Ollaroerp.MainActivity;
import com.senarios.Ollaroerp.Models.SubAlcoholicGroup;
import com.senarios.Ollaroerp.R;
import com.senarios.Ollaroerp.Utitlities.FragmentTags;

import java.util.ArrayList;

public class AlcoholicSubGroupAdapter extends RecyclerView.Adapter<AlcoholicSubGroupAdapter.Holder> {

    ArrayList<SubAlcoholicGroup> menuSubCategories;
    Context context;

    public AlcoholicSubGroupAdapter(Context context, ArrayList<SubAlcoholicGroup> menuSubcatList){
        menuSubCategories=menuSubcatList;
        this.context=context; }

    @NonNull
    @Override
    public Holder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View row = LayoutInflater.from(context).inflate(R.layout.subcat_menu_itemview, viewGroup, false);
        Holder holded = new Holder(row);
        return holded;
    }

    @Override
    public void onBindViewHolder(@NonNull Holder holder, int i) {
        final SubAlcoholicGroup currentModel=menuSubCategories.get(i);
        Glide.with(context).load("http://thesmitherp.digitalsystemsafrica.com/public/img/"+currentModel.getAImage()).into(holder.itemImage);
        holder.itemName.setText(currentModel.getName());
        holder.subcatMenuLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SubcatMenuItemFragment subcatMenuItemFragment=new SubcatMenuItemFragment();
                Bundle bundle=new Bundle();
                bundle.putString("sub_alco_id",Integer.toString(currentModel.getId()));
                subcatMenuItemFragment.setArguments(bundle);
                MainActivity.activity.getSupportFragmentManager().beginTransaction().add(R.id.fragment_container,subcatMenuItemFragment,FragmentTags.SUBCATMENUITEM).addToBackStack(FragmentTags.SUBCATMENUITEM).commitAllowingStateLoss();
            }
        });
    }

    @Override
    public int getItemCount() {
        return menuSubCategories.size();
    }

    class Holder extends RecyclerView.ViewHolder {
        TextView itemName;
        ImageView itemImage;
        RelativeLayout subcatMenuLayout;

        public Holder(View itemView) {
            super(itemView);
            itemName = itemView.findViewById(R.id.tv_item_name);
            itemImage=itemView.findViewById(R.id.civ_subcat_menu);
            subcatMenuLayout = itemView.findViewById(R.id.subcat_menu_layout);
        }
    }
}
