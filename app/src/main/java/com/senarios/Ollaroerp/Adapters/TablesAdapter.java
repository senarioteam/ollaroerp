package com.senarios.Ollaroerp.Adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.senarios.Ollaroerp.Models.CombineBillsTableModel;
import com.senarios.Ollaroerp.R;

import java.util.ArrayList;

public class TablesAdapter extends RecyclerView.Adapter<TablesAdapter.Holder> {

    ArrayList<CombineBillsTableModel> menu;
    Context context;

    public TablesAdapter(Context context, ArrayList<CombineBillsTableModel> menuList) {
        menu = menuList;
        this.context = context;
    }

    @NonNull
    @Override
    public Holder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View row = LayoutInflater.from(context).inflate(R.layout.tables_recycler_list_item, viewGroup, false);
        Holder holded = new Holder(row);
        return holded;
    }

    @Override
    public void onBindViewHolder(@NonNull final Holder holder, int i) {
        CombineBillsTableModel currentModel = menu.get(i);
        holder.itemName.setText(currentModel.getTableId());
        holder.recyclerView.setLayoutManager(new LinearLayoutManager(context));
        holder.recyclerView.setAdapter(new CombineOrdersAdapter(context,currentModel.getCombineBillOrderModels()));
        holder.itemImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(holder.recyclerView.getVisibility()==View.GONE) {
                    holder.recyclerView.setVisibility(View.VISIBLE);
                    holder.itemImage.setImageDrawable(context.getResources().getDrawable(R.drawable.minus_icon));
                }
                else {
                    holder.itemImage.setImageDrawable(context.getResources().getDrawable(R.drawable.plus_icon));
                    holder.recyclerView.setVisibility(View.GONE);
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return menu.size();
    }

    class Holder extends RecyclerView.ViewHolder {
        TextView itemName;
        ImageView itemImage;
        RecyclerView recyclerView;

        public Holder(View itemView) {
            super(itemView);
            itemImage = itemView.findViewById(R.id.iv_hide_show);
            itemName = itemView.findViewById(R.id.table_id);
            recyclerView=itemView.findViewById(R.id.rv_orders);
        }
    }
}

